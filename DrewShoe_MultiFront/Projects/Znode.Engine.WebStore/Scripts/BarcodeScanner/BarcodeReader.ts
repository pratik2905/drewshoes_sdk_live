﻿class BarcodeReader extends ZnodeBase {
    constructor() {
        super();
    }
    _iptIndex: number = 0;
    _scanner = null;
    
    public InitiateBarcodeScanner(licenseKey: string, barcodeFormats: string[], UIElement: string, callbackOnLoadMethod, callbackResultMethod) {
        var dbr = window['dbr'];
        //let barcodeFormats: string[] = ["ONED", "QR_CODE", "CODABAR"];
        if (dbr != null) {
            if (this._scanner != null) {
                this._scanner.onUnduplicatedRead = undefined;
                BarcodeReader.prototype.StopScanner();
            }
            dbr.licenseKey = licenseKey;
            dbr.BarcodeScanner.createInstance().then(s => {
                this._scanner = s;
                this._iptIndex = 0;
                this._scanner.bAddSearchRegionCanvasToResult = true;
                let runtimeSettings = this._scanner.getRuntimeSettings();

                barcodeFormats.forEach((item, index) => {
                    if (index == 0) {
                        runtimeSettings.BarcodeFormatIds = BarcodeReader.prototype.GetBarcodeFormatCode(item, dbr);
                    }
                    else {
                        runtimeSettings.BarcodeFormatIds += BarcodeReader.prototype.GetBarcodeFormatCode(item, dbr);
                    }
                });
                
                // Only decode OneD and QR
                ////runtimeSettings.BarcodeFormatIds = dbr.EnumBarcodeFormat.OneD | dbr.EnumBarcodeFormat.QR_CODE | dbr.EnumBarcodeFormat.CODABAR;
                //runtimeSettings.BarcodeFormatIds += dbr.EnumBarcodeFormat.OneD;
                //runtimeSettings.BarcodeFormatIds += dbr.EnumBarcodeFormat.QR_CODE;
                //runtimeSettings.BarcodeFormatIds += dbr.EnumBarcodeFormat.CODABAR;

                this._scanner.updateRuntimeSettings(runtimeSettings);
                if (dbr.BarcodeReader.isLoaded()) {
                    callbackOnLoadMethod(this._scanner);
                    console.log('Is the loading completed? ' + dbr.BarcodeReader.isLoaded());
                    console.log('indes? ' + this._iptIndex);
                    this._scanner.UIElement = document.getElementById(UIElement);
                    this._scanner.onFrameRead = results => { };
                    this._scanner.onUnduplicatedRead = (txt, result) => {
                        console.log('result? ' + result);
                        callbackResultMethod(txt, result);
                        if (3 == ++this._iptIndex) {
                            this._scanner.onUnduplicatedRead = undefined;
                            BarcodeReader.prototype.StopScanner();
                        }
                    };
                }               
            });
        }
    }
    public StartScanner(callbackSuccess, callbackFailed) {
        if (this._scanner != null) {            
            this._scanner.show().then(_ => { callbackSuccess() }).catch(error => {                
               callbackFailed(error);
            });
        }
    }
    public StartScannerOnElement(UIElement: string, callbackSuccess, callbackFailed) {
        if (this._scanner != null) {            
            this._scanner.UIElement = document.getElementById(UIElement);
            BarcodeReader.prototype.StartScanner(callbackSuccess, callbackFailed);
        }
    }
    public StopScanner() {
        if (this._scanner != null) {
            this._scanner.stop();
            this._scanner.hide();
        }
    }
    public PauseScanner() {
        if (this._scanner != null) {
            this._scanner.pause();
        }
    }
    public GetBarcodeFormatCode(Code, dbr): any {
        var barcodeFormatCode;
        switch (Code) {
            case "ONED": {
                barcodeFormatCode = dbr.EnumBarcodeFormat.OneD;
                break;
            }
            case "QR_CODE": {
                barcodeFormatCode = dbr.EnumBarcodeFormat.QR_CODE;
                break;
            }
            case "CODABAR": {
                barcodeFormatCode = dbr.EnumBarcodeFormat.CODABAR;
                break;
            }
            default: {
                barcodeFormatCode = dbr.EnumBarcodeFormat.QR_CODE;
                break;
            }
        }
        return barcodeFormatCode;
    }
}