﻿export interface KlaviyoTrackProfileActivityInterface {
    email: string  //eg: sohan.dawande@nivisolutions.com
    first_name: string  //eg: Sohan
    last_name: string  //Dawande
    phone_number: string; //eg: "+918149958899"
    city: string //eg: Nagpur
    region: string; //state, or other region
    country: string //eg: India
    zip: string  //eg: 440001
    image: string; //url to a photo of a person
    //$consent: list of strings; //eg: ['sms', 'email', 'web', 'directmail', 'mobile']
}

export interface KlaviyoIdentityProfileInterface {
    email: string
    first_name: string
    last_name: string
    phone_number: string;
    city: string
    region: string; 
    country: string
    zip: string
    image: string;
//$consent: list of strings; eg: ['sms', 'email', 'web', 'directmail', 'mobile']
}
