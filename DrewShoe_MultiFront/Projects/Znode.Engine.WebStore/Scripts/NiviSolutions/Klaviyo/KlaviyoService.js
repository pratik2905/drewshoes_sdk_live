"use strict";
/// <reference path="klaviyourls.ts" />
/// <reference path="klaviyointerface.ts" />
Object.defineProperty(exports, "__esModule", { value: true });
var klaviyourls_1 = require("./klaviyourls");
var TrackUrl = klaviyourls_1.KlaviyoUrls._trackURL;
var IdentityUrl = klaviyourls_1.KlaviyoUrls._trackURL;
function KlaviyoPromotionSignUpEvent() {
    var options = {
        method: 'POST',
        headers: { Accept: 'text/html', 'Content-Type': 'application/x-www-form-urlencoded' },
        body: new URLSearchParams({
            data: "{\"token\": " + KlaviyoCommonEnums.DrewTokenPublicKey + ", \n                  \"event\": \"Promotion Sign Up\",\n                  \"properties\": {\"$email\": \"abraham.lincoln@klaviyo.com\",\"$first_name\": \"Abraham\",\"$last_name\": \"Lincoln\"}}"
        })
    };
    fetch(IdentityUrl, options)
        .then(function (response) { return response.json(); })
        .then(function (response) { return console.log(response); })
        .catch(function (err) { return console.error(err); });
}
;
function KlaviyoBrowsingEvent() {
    var options = {
        method: 'POST',
        headers: { Accept: 'text/html', 'Content-Type': 'application/x-www-form-urlencoded' },
        body: new URLSearchParams({
            data: "{\"token\": " + KlaviyoCommonEnums.DrewTokenPublicKey + ", \n                  \"event\": \"Customer Browsing Data\",\n                  \"customer_properties\": {\"$email\": \"abraham.lincoln@klaviyo.com\"},\n                  \"properties\": {\"productName\": \"productName\",\"productUrl\": \"productUrl\",\"price\": \"price\",\"imageUrl\": \"imageUrl\"}}"
        })
    };
    fetch(TrackUrl, options)
        .then(function (response) { return response.json(); })
        .then(function (response) { return console.log(response); })
        .catch(function (err) { return console.error(err); });
}
;
function KlaviyoCheckoutEvent() {
    var options = {
        method: 'POST',
        headers: { Accept: 'text/html', 'Content-Type': 'application/x-www-form-urlencoded' },
        body: new URLSearchParams({
            data: "{\"token\": " + KlaviyoCommonEnums.DrewTokenPublicKey + ", \n                  \"event\": \"Customer Checkout Data\",\n                  \"customer_properties\": {\"$email\": \"abraham.lincoln@klaviyo.com\"},\n                  \"properties\": {\"SKU\": \"Sku\",\"ProductName\": \"ProductName\",\"itemPrice\": \"itemPrice\",\"productUrl\": \"productUrl\",\"imageUrl\": \"imageUrl\"}}"
        })
    };
    fetch(TrackUrl, options)
        .then(function (response) { return response.json(); })
        .then(function (response) { return console.log(response); })
        .catch(function (err) { return console.error(err); });
}
;
function KlaviyoPlacedOrderEvent() {
    var options = {
        method: 'POST',
        headers: { Accept: 'text/html', 'Content-Type': 'application/x-www-form-urlencoded' },
        body: new URLSearchParams({
            data: "{\"token\": " + KlaviyoCommonEnums.DrewTokenPublicKey + ", \n                  \"event\": \"Customer Placed Order Data\",\n                  \"customer_properties\": {\"$email\": \"abraham.lincoln@klaviyo.com\"},\n                  \"properties\": {\n                  \"BillingAddress\":{\"firstName\":\"firstName\",\"lastName\":\"lastName\",\"companyName\":\"null\",\"Address1\":\"address1\",\"Address2\":\"address2\",\"City\":\"city\",\"Region\":\"region\",\n                                    \"RegionCode\":\"regionCode\",\"Country\":\"country\",\"countryCode\":\"countrycode\",\"Zip\":\"zip\",\"Phone\":\"phone\"},\n                  \"Items\":{\"ProductName\":\"productName\",\"SKU\":\"sku\",\"ItemPrice\":\"itemPrice\",\"Quantity\":\"quantity\",\"RowTotal\":\"rowTotal\",\"ProductURL\":\"producturl\",\n                           \"Description\":{\"Color\":\"color\",\"ImageURL\":\"imageURL\"}},\n                  \"ShippingAddress\":{\"FirstName\":\"firstName1\",\"LastName1\":\"lastName1\",\"CompanyName1\":\"null\",\"Address1\":\"address1\",\"Address2\":\"address2\",\"City\":\"city\",\"Region\":\"region\",\n                                     \"RegionCode\":\"regionCode\",\"Country\":\"country\",\"countryCode\":\"countrycode\",\"Zip\":\"zip\",\"Phone\":\"phone\" },\n                  \"Tax\":\"tax\"}}"
        })
    };
    fetch(TrackUrl, options)
        .then(function (response) { return response.json(); })
        .then(function (response) { return console.log(response); })
        .catch(function (err) { return console.error(err); });
}
;
function KlaviyoTrackProfileActivity() {
    var options = {
        method: 'POST',
        headers: { Accept: 'text/html', 'Content-Type': 'application/x-www-form-urlencoded' },
        body: new URLSearchParams({
            data: "\n                 {\"token\": " + KlaviyoCommonEnums.DrewTokenPublicKey + ", \n                  \"event\": \"Ordered Product\", \n                  \"customer_properties\": {\"$email\": \"abraham.lincoln@klaviyo.com\"}, \n                  \"properties\": {\"item_name\": \"Boots\",\"$value\": 100}\n                 }"
        })
    };
    fetch(TrackUrl, options)
        .then(function (response) { return response.json(); })
        .then(function (response) { return console.log(response); })
        .catch(function (err) { return console.error(err); });
}
;
function KlaviyoIdentityProfile() {
    var options = {
        method: 'POST',
        headers: { Accept: 'text/html', 'Content-Type': 'application/x-www-form-urlencoded' },
        body: new URLSearchParams({
            data: '{"token": "PUBLIC_KEY","properties": {"$email":"ben.franklin@klaviyo.com"}}'
        })
    };
    fetch('https://a.klaviyo.com/api/identify', options)
        .then(function (response) { return response.json(); })
        .then(function (response) { return console.log(response); })
        .catch(function (err) { return console.error(err); });
}
//# sourceMappingURL=KlaviyoService.js.map