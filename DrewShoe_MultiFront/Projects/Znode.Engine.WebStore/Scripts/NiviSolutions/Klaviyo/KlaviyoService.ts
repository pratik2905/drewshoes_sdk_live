﻿/// <reference path="klaviyourls.ts" />
/// <reference path="klaviyointerface.ts" />

import { KlaviyoTrackProfileActivityInterface } from "./klaviyointerface";
import { KlaviyoUrls } from "./klaviyourls";

let TrackUrl = KlaviyoUrls._trackURL;
let IdentityUrl = KlaviyoUrls._trackURL;


function KlaviyoPromotionSignUpEvent() {
    const options = {
        method: 'POST',
        headers: { Accept: 'text/html', 'Content-Type': 'application/x-www-form-urlencoded' },
        body: new URLSearchParams({
            data: `{"token": ${KlaviyoCommonEnums.DrewTokenPublicKey}, 
                  "event": "Promotion Sign Up",
                  "properties": {"$email": "abraham.lincoln@klaviyo.com","$first_name": "Abraham","$last_name": "Lincoln"}}`
        })
    };

    fetch(IdentityUrl, options)
        .then(response => response.json())
        .then(response => console.log(response))
        .catch(err => console.error(err));
};

function KlaviyoBrowsingEvent() {
    const options = {
        method: 'POST',
        headers: { Accept: 'text/html', 'Content-Type': 'application/x-www-form-urlencoded' },
        body: new URLSearchParams({
            data: `{"token": ${KlaviyoCommonEnums.DrewTokenPublicKey}, 
                  "event": "Customer Browsing Data",
                  "customer_properties": {"$email": "abraham.lincoln@klaviyo.com"},
                  "properties": {"productName": "productName","productUrl": "productUrl","price": "price","imageUrl": "imageUrl"}}`
        })
    };

    fetch(TrackUrl, options)
        .then(response => response.json())
        .then(response => console.log(response))
        .catch(err => console.error(err));
};

function KlaviyoCheckoutEvent() {
    const options = {
        method: 'POST',
        headers: { Accept: 'text/html', 'Content-Type': 'application/x-www-form-urlencoded' },
        body: new URLSearchParams({
            data: `{"token": ${KlaviyoCommonEnums.DrewTokenPublicKey}, 
                  "event": "Customer Checkout Data",
                  "customer_properties": {"$email": "abraham.lincoln@klaviyo.com"},
                  "properties": {"SKU": "Sku","ProductName": "ProductName","itemPrice": "itemPrice","productUrl": "productUrl","imageUrl": "imageUrl"}}`
        })
    };

    fetch(TrackUrl, options)
        .then(response => response.json())
        .then(response => console.log(response))
        .catch(err => console.error(err));
};

function KlaviyoPlacedOrderEvent() {
    const options = {
        method: 'POST',
        headers: { Accept: 'text/html', 'Content-Type': 'application/x-www-form-urlencoded' },
        body: new URLSearchParams({
            data: `{"token": ${KlaviyoCommonEnums.DrewTokenPublicKey}, 
                  "event": "Customer Placed Order Data",
                  "customer_properties": {"$email": "abraham.lincoln@klaviyo.com"},
                  "properties": {
                  "BillingAddress":{"firstName":"firstName","lastName":"lastName","companyName":"null","Address1":"address1","Address2":"address2","City":"city","Region":"region",
                                    "RegionCode":"regionCode","Country":"country","countryCode":"countrycode","Zip":"zip","Phone":"phone"},
                  "Items":{"ProductName":"productName","SKU":"sku","ItemPrice":"itemPrice","Quantity":"quantity","RowTotal":"rowTotal","ProductURL":"producturl",
                           "Description":{"Color":"color","ImageURL":"imageURL"}},
                  "ShippingAddress":{"FirstName":"firstName1","LastName1":"lastName1","CompanyName1":"null","Address1":"address1","Address2":"address2","City":"city","Region":"region",
                                     "RegionCode":"regionCode","Country":"country","countryCode":"countrycode","Zip":"zip","Phone":"phone" },
                  "Tax":"tax"}}`
        })
    };

    fetch(TrackUrl, options)
        .then(response => response.json())
        .then(response => console.log(response))
        .catch(err => console.error(err));
};

function KlaviyoTrackProfileActivity(){
    const options = {
        method: 'POST',
        headers: { Accept: 'text/html', 'Content-Type': 'application/x-www-form-urlencoded' },
        body: new URLSearchParams({
            data: `
                 {"token": ${KlaviyoCommonEnums.DrewTokenPublicKey}, 
                  "event": "Ordered Product", 
                  "customer_properties": {"$email": "abraham.lincoln@klaviyo.com"}, 
                  "properties": {"item_name": "Boots","$value": 100}
                 }`           
        })
    };

    fetch(TrackUrl, options)
        .then(response => response.json())
        .then(response => console.log(response))
        .catch(err => console.error(err));
};

function KlaviyoIdentityProfile() {
    const options = {
        method: 'POST',
        headers: { Accept: 'text/html', 'Content-Type': 'application/x-www-form-urlencoded' },
        body: new URLSearchParams({
            data: '{"token": "PUBLIC_KEY","properties": {"$email":"ben.franklin@klaviyo.com"}}'
        })
    };

    fetch('https://a.klaviyo.com/api/identify', options)
        .then(response => response.json())
        .then(response => console.log(response))
        .catch(err => console.error(err));
}
