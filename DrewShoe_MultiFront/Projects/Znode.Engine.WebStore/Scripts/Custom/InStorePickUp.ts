﻿ //<reference path="../../views/themes/Drewshoe/scripts/lib/googlemap.js" />
//<reference path="../typings/google.maps.d.ts" />
var xhr = new XMLHttpRequest();
var calledfrom = '';
var storeId = '';
var productId = '';

class InStorePickUp extends ZnodeBase {
    constructor() {
        super();
    }

    /*Called From Store Locator,PDP and Cart store location pop up Submit button click*/
    GetLatLng(calledfrom) {
        try {
           
            $(".dynamic").html("");
            $("#responseerror").text("");
            var addr = $("#txtzipcitystate").val();
            //nivi code to add query string start
            if (addr !== "") {
                var originalUrl = window.location.href;
                var a = originalUrl.indexOf("?");
                var b = originalUrl.substring(a);
                var c = originalUrl.replace(b, "");
                var cleanurl = c;
                var valu = $("#txtzipcitystate").val();
                var query = new URLSearchParams();
                query.append("searchterm", valu);
                //var myURL = document.location;
                var newUrl = cleanurl + "?" + query.toString();
                window.history.pushState('obj', 'newtitle', newUrl);
            }
            //nivi code to add query string end
            if (addr == '') {
                $("#error").text("*This is a required field.")
                return false;
            }
            else {
                $("#error").text("");
            }
            var lat = '';
            var lng = '';
            var address = addr;
            //$("#loaderId").html("<div class='loader-inner'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
            $(".dynamic").append("<div class='info'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");                       
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    lat = String(results[0].geometry.location.lat());
                    lng = String(results[0].geometry.location.lng());

                    var _userCord = new google.maps.LatLng(Number(lat), Number(lng));
                    var sku = $("#dynamic-configurableproductskus").val();
                    if (calledfrom == 'storelocator') {
                        InStorePickUp.prototype.GetStoresForStoreLocator('ALL', '0', _userCord);
                    }
                    else if (calledfrom == 'pdp') {
                       // InStorePickUp.prototype.GetStores('ALL', sku, _userCord);
                    }
                    else {
                        sku = $("#btnSubmit").attr('data-sku');
                       // InStorePickUp.prototype.GetStoresForCart('ALL', sku, _userCord);
                    }
                    // initMap();
                }
                else {
                    console.log("Geocode was not successful for the following reason: " + status);
                    $(".dynamic").html("");
                    $("#storedetails").html("");
                    $("#storedetails").append("Geocode was not successful for the following reason: " + status);
                    $("#storedetails").show();
                }
            });
           // $("#loaderId").html("");

        }
        catch (err) {
            console.log(err.message);
        }
    }

    /*Called from processRequest(e) and GetLatLang()*/
    GetStoresForStoreLocator(state, sku, _userCord) {

        try {
                     
            var imagePath = $("#ImagePathURL").val();
            var webstoreUrl = $("#WebStoreUrl").val();
            var currentStore = $(".StoreLocatorLink span").html();
            RSZnodeEndpoint.prototype.GetStoresForStoreLocatorSuccess(state, sku, _userCord, function (data) {
                $("#storedetails").html('');
                $.each(data, function (i, item) {

                    if (i == "StoreLocations") {
                        var json = JSON.parse(JSON.stringify(data[i]));
                        $.each(json, function (j, val) {
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var _storeCord = new google.maps.LatLng(address['Latitude'], address['Longitude']);

                            var distanceinmeters = google.maps.geometry.spherical.computeDistanceBetween(_userCord, _storeCord);
                            var distanceinmiles = distanceinmeters / 1609.344;
                            address['distance'] = distanceinmiles;
                            json[j].distance = distanceinmiles;

                        });

                        json.sort(function (a, b) {
                            return Math.floor(a.distance) - Math.floor(b.distance)
                        });
                        var cnt = 0;
                        $.each(json, function (j, val) {
                            if (cnt != 10)
                            { 
                            cnt = cnt + 1;
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var addr = address['StoreAddress'];
                            var storedetailshref = "";
                            if (address['seourl'] == null) {
                                storedetailshref = "#";
                            }
                            else {
                                storedetailshref = webstoreUrl + '/' + address['seourl'];
                            }
                           // alert("storedetailshref"+storedetailshref);
                            try {
                                var addressdetail = addr.replace(/^,/, '');
                                addressdetail = addressdetail + ",Phone:" + address['PhoneNumber'];
                                var arr = address['StoreTiming'].split(',');
                            }
                            catch (err) {
                                console.log(err.message);
                            }
                            var storenamedetail = address['StoreName'].replace(/'/g, '&#39;');
                            var HtmlTags = $("#storedetails").html();
                            HtmlTags += "<div class='col-lg-12 col-md-12 nopadding storeLocationCoordinate' style='margin-bottom:20px !important' id=" + storenamedetail + " data-distance='0'  data-lng=" + address['Longitude'] + "  data-lat=" + address['Latitude'] + "  data-address=" + addr.replace(/^,/, '') + "  data-title=" + address['StoreName'] + ">"
                            //HtmlTags += "<div class='col-lg-4 col-md-4 col-sm-4 col-xs-12 nopadding-mobile'><img alt='" + storenamedetail + "' title='" + storenamedetail + "' src='" + imagePath + address['ImageName'] + "'/></div>"
                            HtmlTags += "<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding' style='margin-top:15px;margin-bottom:15px'>" + "<div class='col-lg-4 col-md-4 col-sm-4 col-xs-12 nopadding-mobile nopadding'>" + "<div class='form-group'><h4 style='margin: 0px;'>" + storenamedetail + "</h4></div>"

                            HtmlTags += "<div style='margin-bottom: 15px !important;'><h4 style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>"
                            $.each(arr, function (j, val) {
                                //HtmlTags += "<div class='form-group' style='line-height: 10px;'><p class='store-hours'>" + arr[j] + "</p></div>"
                            });
                            HtmlTags += "</div>"
                            HtmlTags += "<div class='col-lg-4 col-md-4 col-sm-4 nopadding col-xs-12'><div class='form-group'>" + addr.replace(/^,/, '') + "</div>"
                            HtmlTags += "<div class='form-group'><p class='recent-order-reorder'><a href=" + address['DisplayName'] + " target='_blank' style='text-decoration: underline;' title='Map and Directions'>Map and Directions</a></p></div></div>"
                            HtmlTags += "<div class='col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center margin-bottom'><h1><span class='miles'>" + Math.floor(address['distance']) + "</span>" + "<br/> miles away" + "</h1></div>"
                            
                            //HtmlTags += "<div class='col-lg-12 col-xs-12 col-md-12 margin-top'><div class='form-inlineclass'><div>"

                           // HtmlTags += "<a style='color: #fff !important;font-weight: normal;' class='btn red' href=" + storedetailshref + " target='_blank' title='Store Details'>Store Details</a></div>"
                           
                            if (currentStore == storenamedetail) {
                                
                               // HtmlTags += "<div><button id=" + address['WarehouseId'] + " onclick=' return InStorePickUp.prototype.ShowPopUpSL(this,\"" + storenamedetail + "\",\"" + addressdetail + "\");' class='btn btn-primary-1' data-dismiss='modal' aria-hidden='true' value='Make this My Store' title='Make this My Store' style='margin-left: 15px;'>My Store</button></div></div></div>" + "</div>";
                               // alert("HtmlTags store address: - " + HtmlTags);
                            }
                            else {
                               // HtmlTags += "<div><button id=" + address['WarehouseId'] + " onclick=' return InStorePickUp.prototype.ShowPopUpSL(this,\"" + storenamedetail + "\",\"" + addressdetail + "\");' class='btn btn-primary' data-dismiss='modal' aria-hidden='true' value='Make this My Store' title='Make this My Store' style='margin-left: 15px;'>Make this My Store</button></div></div></div>" + "</div>";
                                //alert("HtmlTags store address2: - " + JSON.stringify(HtmlTags));
                            }
                            HtmlTags += "</div></div><br/>";

                            $("#storedetails").html(HtmlTags);
                           // alert("HtmlTags store address2 store details: - " + JSON.stringify($("#storedetails").html(HtmlTags)));
                            $("#storedetails").show();
                            $(".dynamic").html("");
                            }

                        });
                        if (cnt == 0) {
                            $("#storedetails").append("Sorry No Stores Found for Your State");
                            $("#storedetails").show();
                        }
                    }
                });
            });
           
        }
        catch (err) {
            console.log(err.message);
        }
    }

    /*Called from PDP,CART and STORELocator Use My LOCation Button CLick*/
    GetLocation(c) {
        try {
            var ipaddress = $("#hdnIPAddress").val();
            console.log("IPaddress: " + ipaddress);
            calledfrom = c;
            xhr.onreadystatechange = this.processRequest;
            //xhr.open('GET', "//ipinfo.io/json", true);
            xhr.open('GET', ipaddress, true);
            xhr.send();
        }
        catch (err) {
            console.log(err.message);
        }
    }

    /*Called from GetLocation()*/
    processRequest(e) {
        $(".dynamic").html("");
        $("#responseerror").text("");
        if (xhr.readyState === 4 && xhr.status === 200) {
            var response = JSON.parse(xhr.responseText);
            var lat = '';
            var lng = '';
            $(".dynamic").append("<div class='info'><img src= '../Content/Images/throbber.gif' alt= 'Loading' class='dashboard-loader' /></div>");
            var array = response.loc.split(",");
            lat = array[0];
            lng = array[1];
            var _userCord = new google.maps.LatLng(Number(lat), Number(lng));
            var response = JSON.parse(xhr.responseText);
            if (calledfrom === 'storelocator') {
                InStorePickUp.prototype.GetStoresForStoreLocator(response.region, "0", _userCord);
            }
        }
        else {
            console.log("Geocode was not successful for the following reason: " + status);
            $(".dynamic").html("");
            $("#storedetails").html("");
            $("#storedetails").append("Geocode was not successful for the following reason: " + status);
            $("#storedetails").show();
        }
    }
    /*Called from processRequest(e) for both PDP and Store Locator*/
    GetStores(state, sku, _userCord) {
        if (sku != '0') {
            sku = $("#dynamic-configurableproductskus").val();
        }
        try {
            RSZnodeEndpoint.prototype.GetStoresSuccess(state, sku, _userCord, function (data) {

                $("#loaderId").html("");
                $("#storedetails").html('');
                $.each(data, function (i, item) {

                    if (i == "StoreLocations") {
                        var json = JSON.parse(JSON.stringify(data[i]));
                        $.each(json, function (j, val) {

                            //if (json[j].Quantity > 0) {
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var _storeCord = new google.maps.LatLng(address['Latitude'], address['Longitude']);

                            var distanceinmeters = google.maps.geometry.spherical.computeDistanceBetween(_userCord, _storeCord);
                            var distanceinmiles = distanceinmeters / 1609.344;
                            address['distance'] = distanceinmiles;
                            json[j].distance = distanceinmiles;
                            // }

                        });

                        json.sort(function (a, b) {
                            return Math.floor(a.distance) - Math.floor(b.distance)
                        });
                        var cnt = 0;
                        $.each(json, function (j, val) {
                            cnt = cnt + 1;
                            //if (json[j].Quantity > 0) {
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var addr = address['StoreAddress'];
                            try {
                                var addressdetail = addr.replace(/^,/, '');
                                addressdetail = addressdetail + ",Phone:" + address['PhoneNumber'];

                            }
                            catch (err) {
                                console.log(err.message);
                            }
                            var storenamedetail = address['StoreName'].replace(/'/g, '&#39;');
                            $("#storedetails").append("<div class='col-lg-12 col-md-12 nopadding' id=" + storenamedetail + ">");
                            if (sku == '0') {
                                /*STORE LOCATOR CASE*/
                                $("#storedetails").append("<div class='col-lg-8 col-xs-8 col-md-8 nopadding'>" + "<div class='form-group'><h4>" + storenamedetail + "</h4></div>" + "<div class='form-group'> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div class='form-group'>" + addr.replace(/^,/, '') + "</div>" + "<div class='form-group'><button id=" + address['WarehouseId'] + " onclick=' return InStorePickUp.prototype.ShowPopUpSL(this,\"" + storenamedetail + "\",\"" + addressdetail + "\");' class='btn btn-primary' data-dismiss='modal' aria-hidden='true' value='Make this My Store'>Make this My Store</button></div>" + "</div>");
                            }
                            else {
                                /*PDP CASE*/
                                //var status = "AVAILABLE(" + address['Quantity']+")";
                                var status = "<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + address['Quantity'] + " left</span>";
                                if (address['Quantity'] == 0) {
                                    status = "Out of stock";
                                    //$("#storedetails").append("<div class='col-lg-8 col-xs-8 col-md-8 nopadding'>" + "<div class='form-group'><h4>" + address['StoreName'] + "</h4><label style='color:darkred;font-weight:bold;'>" + status + "</label></div>" + "<div class='form-group'> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div class='form-group'>" + addr.replace(/^,/, '') + "</div>" + "<div class='form-group'><label id=" + address['WarehouseId'] + " data-qty=" + address['Quantity'] + " class='btn btn-danger' data-dismiss='modal' aria-hidden='true'>Not Available</label></div>" + "</div>");
                                    $("#storedetails").append("<div class='col-lg-6 col-xs-9 col-md-9 nopadding'>" + "<div><h4>" + storenamedetail + "</h4></div>" + "<div> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div>" + addr.replace(/^,/, '') + "</div>" + " <span class= 'error-msg'> " + status + "</span>" + "</div>");
                                }
                                else {
                                    //var storenamedetail = address['StoreName'].replace(/'/g, '&#39;');
                                    $("#storedetails").append("<div class='col-lg-6 col-xs-9 col-md-9 nopadding'>" + "<div><h4>" + storenamedetail + "</h4></div>" + "<div> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div>" + addr.replace(/^,/, '') + "</div>" + "<span class='form-group'>" + status + "</span>" + "<div><button id=" + address['WarehouseId'] + " data-qty=" + address['Quantity'] + " onclick='InStorePickUp.prototype.Click(this,\"" + storenamedetail + "\",\"" + addressdetail + "\",\"" + address['Quantity'] + "\");' class='btn btn-primary popup-btnpickup' data-dismiss='modal' aria-hidden='true' style='text-transform: none;'>Pick Up here</button></div>" + "</div>");
                                }
                            }
                            $("#storedetails").append("<div class='col-lg-6 col-xs-3 col-md-3 nopadding text-center'>" + "<br/><span class='miles'>" + Math.floor(address['distance']) + "</span>" + "<br/> Miles Away" + "</div>");
                            $("#storedetails").append("</div><hr>");
                            $("#storedetails").show();
                            // }

                        });
                        if (cnt == 0) {
                            $("#storedetails").append("Sorry No Stores Found for Your State");
                            $("#storedetails").show();
                        }
                    }
                });
            });
        }
        catch (err) {
            console.log(err.message);
        }
    }

  
    /*CART- called from processRequest(e) and GetLatLang()*/
    GetStoresForCart(state, sku, _userCord) {
        try {
            RSZnodeEndpoint.prototype.GetStoresForCartSuccess(state, sku, _userCord, function (data) {
                $("#storedetails").html('');
                $.each(data, function (i, item) {

                    if (i == "StoreLocations") {
                        var json = JSON.parse(JSON.stringify(data[i]));
                        $.each(json, function (j, val) {
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var _storeCord = new google.maps.LatLng(address['Latitude'], address['Longitude']);

                            var distanceinmeters = google.maps.geometry.spherical.computeDistanceBetween(_userCord, _storeCord);
                            var distanceinmiles = distanceinmeters / 1609.344;
                            address['distance'] = distanceinmiles;
                            json[j].distance = distanceinmiles;

                        });

                        json.sort(function (a, b) {
                            return Math.floor(a.distance) - Math.floor(b.distance)
                        });
                        var cnt = 0;
                        $("#storedetails").html('');
                        $.each(json, function (j, val) {
                            cnt = cnt + 1;
                            var address1 = JSON.stringify(json[j]);
                            var address = JSON.parse(address1);
                            var addr = address['StoreAddress'];
                            try {
                                var addressdetail = addr.replace(/^,/, '');
                                addressdetail = addressdetail + ",Phone:" + address['PhoneNumber'];
                            }
                            catch (err) {
                                console.log(err.message);
                            }
                            var storenamedetail = address['StoreName'].replace(/'/g, '&#39;');
                            $("#storedetails").append("<div class='col-lg-12 col-md-12 nopadding' id=" + storenamedetail + ">");

                            var status = "<b style='color:#000'>In Stock</b> - <span class='text-danger' style='font-weight: 400;'>only " + address['Quantity'] + " left</span>";
                            if (address['Quantity'] == 0) {
                                status = "Out of stock";
                                $("#storedetails").append("<div class='col-lg-8 col-xs-9 col-md-4 nopadding'>" + "<div><h4>" + storenamedetail + "</h4></div>" + "<div> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div>" + addr.replace(/^,/, '') + "</div>" + " <span class= 'error-msg'> " + status + "</span>" + "</div>");
                            }
                            else {
                                $("#storedetails").append("<div class='col-lg-8 col-xs-9 col-md-4 nopadding'>" + "<div><h4>" + storenamedetail + "</h4></div>" + "<div> <h4  style='margin: 0px;'><b>" + address['PhoneNumber'] + "</b></h4></div>" + "<div>" + addr.replace(/^,/, '') + "</div>" + "<span class='form-group'>" + status + "</span>" + "<div><button id='" + address['WarehouseId'] + "' data-qty=" + address['Quantity'] + " onclick='InStorePickUp.prototype.ClickCart(this,\"" + address['StoreName'] + "\",\"" + addressdetail + "\",\"" + sku + "\",\"" + address['Quantity'] + "\");' class='btn btn-primary popup-btnpickup' data-dismiss='modal' aria-hidden='true' style='text-transform: none;'>Pick Up here</button></div>" + "</div>");
                            }

                            $("#storedetails").append("<div class='col-lg-4 col-xs-3 col-md-4 text-center'>" + "<br/><span class='miles'>" + Math.floor(address['distance']) + "</span>" + "<br/> miles away" + "</div>");
                            $("#storedetails").append("</div><hr/>");
                            $("#storedetails").show();

                        });
                        if (cnt == 0) {
                            $("#storedetails").append("Sorry No Stores Found for Your State");
                            $("#storedetails").show();
                        }
                    }
                });

            });

        }
        catch (err) {
            console.log(err.message);
        }
    }


    /*Called from Add To cart button */
    RedirectToCart() {
        var webstoreURL = $("#WebstoreURL").val();
        window.location.href = webstoreURL + "/cart";
    }
    
}