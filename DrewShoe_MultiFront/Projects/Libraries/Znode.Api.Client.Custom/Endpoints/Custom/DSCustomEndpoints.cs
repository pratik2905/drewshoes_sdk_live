﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Api.Client.Custom.Endpoints.Custom
{
    public class DSCustomEndpoints : BaseEndpoint
    {
        public static string GetInventoyGrid(int PublishProductId, string Color) => $"{ApiRoot}/dscustom/getinventoygrid/{PublishProductId}/{Color}";
    }
}
