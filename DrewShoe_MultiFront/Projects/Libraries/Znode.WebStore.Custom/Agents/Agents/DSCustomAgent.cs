﻿using System.Linq;
using Znode.Api.Client.Custom.Clients.Clients;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Engine.WebStore;
using Znode.Sample.Api.Model;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class DSCustomAgent : BaseAgent, IDSCustomAgent
    {
        private readonly IDSCustomClient _DSCustomClient;

        public DSCustomAgent()
        {
            _DSCustomClient = new DSCustomClient();
        }

        public virtual DSCustomListViewModel GetInventoyGrid(int PublishProductId, string Color)
        {
            DSCustomListModel taxClassListModel = _DSCustomClient.GetInventoyGrid(PublishProductId, Color);
            DSCustomListViewModel list = new DSCustomListViewModel { InventoryList = taxClassListModel?.InventoryList?.ToViewModel<DSCustomViewModel>().ToList() };
            return list;


        }
    }
}
