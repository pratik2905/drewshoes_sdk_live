﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Core.ViewModels;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
namespace Znode.WebStore.Custom.Agents.Agents
{
    public class DSCartAgent :  CartAgent, ICartAgent
    {

        #region protected member
        protected readonly IShoppingCartClient _shoppingCartsClient;
        protected readonly IPublishProductClient _publishProductClient;
        protected readonly IAccountQuoteClient _accountQuoteClient;
        protected readonly IUserClient _userClient;
        protected List<UpdatedProductQuantityModel> updatedProducts = new List<UpdatedProductQuantityModel>();
        #endregion
        #region Private member       
        #endregion
        public DSCartAgent(IShoppingCartClient shoppingCartsClient, IPublishProductClient publishProductClient, IAccountQuoteClient accountQuoteClient, IUserClient userClient) : base(shoppingCartsClient, publishProductClient, accountQuoteClient, userClient)
        {
            _shoppingCartsClient = GetClient<IShoppingCartClient>(shoppingCartsClient);
            _publishProductClient = GetClient<IPublishProductClient>(publishProductClient);
            _accountQuoteClient = GetClient<IAccountQuoteClient>(accountQuoteClient);
            _userClient = GetClient<IUserClient>(userClient);
        }
        public override void GetSelectedGroupedProductsForAddToCart(AddToCartViewModel cartItem)
        {
            //Get sku's and quantity of associated group products.

            string[] groupProducts = string.IsNullOrEmpty(cartItem.GroupProductSKUs) ? cartItem.GroupProducts?.Select(x => x.Sku)?.ToArray() : cartItem.GroupProductSKUs?.Split(',');

            string[] groupProductsQuantity = string.IsNullOrEmpty(cartItem.GroupProductsQuantity) ? cartItem.GroupProducts?.Select(x => Convert.ToString(x.Quantity))?.ToArray() : cartItem.GroupProductsQuantity?.Split('_');

            //groupProducts[0] = "";
            cartItem.SKU = cartItem.SKU;
            cartItem.AddOnProductSKUs = cartItem.AddOnProductSKUs;
            cartItem.AutoAddonSKUs = cartItem.AutoAddonSKUs;

            for (int index = 0; index < groupProducts?.Length; index++)
            {
                bool isNewExtIdRequired = !Equals(index, 0);

                //cartItem.ExternalId = isNewExtIdRequired ? Guid.NewGuid().ToString() : cartItem.ExternalId;
                //cartItem.GroupProducts = new List<AssociatedProductModel> { new AssociatedProductModel { Sku = groupProducts[index], Quantity = decimal.Parse(groupProductsQuantity[index]) } };

                ShoppingCartItemModel cartItemModel = BindConfigurableProducts(groupProducts[index], groupProductsQuantity[index], cartItem, isNewExtIdRequired);

                if (IsNotNull(cartItemModel))
                {
                    cartItem.ShoppingCartItems.Add(cartItemModel);
                }
            }
        }

        private ShoppingCartItemModel BindConfigurableProducts(string configurableSKU, string quantity, AddToCartViewModel cartItem, bool isNewExtIdRequired)
        {
            return new ShoppingCartItemModel
            {
                ExternalId = isNewExtIdRequired ? Guid.NewGuid().ToString() : cartItem.ExternalId,
                SKU = cartItem.SKU,
                ConfigurableProductSKUs = configurableSKU,
                AddOnProductSKUs = cartItem.AddOnProductSKUs,
                AutoAddonSKUs = cartItem.AutoAddonSKUs,
                Quantity = Convert.ToDecimal(quantity),
                PersonaliseValuesList = cartItem.PersonaliseValuesList,
                GroupProducts = new List<AssociatedProductModel>(),
                //IsProductEdit = cartItem.IsProductEdit
            };
        }

        public override CartViewModel GetCart(bool isCalculateTaxAndShipping = true, bool isCalculateCart = true, bool isCalculatePromotionAndCoupon = true)
        {
            try
            {
                ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                                     GetCartFromCookie();

                if (IsNull(shoppingCartModel))
                {
                    return new CartViewModel()
                    {
                        HasError = true,
                        ErrorMessage = WebStore_Resources.OutofStockMessage
                    };
                }

                if (IsNotNull(shoppingCartModel) && (IsNull(shoppingCartModel?.ShoppingCartItems) || shoppingCartModel?.ShoppingCartItems?.Count == 0))
                {
                    var shoppingartItems = GetCartFromCookie()?.ShoppingCartItems;
                    shoppingCartModel.ShoppingCartItems = (shoppingartItems?.Count > 0) ? shoppingartItems : new List<ShoppingCartItemModel>();
                }

                if (shoppingCartModel.ShoppingCartItems?.Count == 0)
                {
                    return new CartViewModel();
                }

                //Remove Shipping and Tax calculation From Cart.
                RemoveShippingTaxFromCart(shoppingCartModel);

                //To remove the tax getting calculated when we come back from checkout page to cart page
                shoppingCartModel.ShippingAddress = new AddressModel();
                shoppingCartModel.ShippingAddress.CountryName = "us";
                shoppingCartModel.ShippingAddress.PostalCode = GetFromSession<String>(ZnodeConstant.ShippingEstimatedZipCode);
                shoppingCartModel.CultureCode = PortalAgent.CurrentPortal.CultureCode;
                shoppingCartModel.CurrencyCode = PortalAgent.CurrentPortal.CurrencyCode;
                shoppingCartModel.IsCalculatePromotionAndCoupon = isCalculatePromotionAndCoupon;
                shoppingCartModel.IsCalculateVoucher = false;
                //Calculate cart
                shoppingCartModel = CalculateCart(shoppingCartModel, isCalculateTaxAndShipping, isCalculateCart);

                SaveInSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey, shoppingCartModel);
                //Set currency details.
                return SetCartCurrency(shoppingCartModel);
            }
            catch (Exception ex)
            {

                return new CartViewModel();
            }
        }
        public override ShoppingCartModel GetCartFromCookie()
        {
            ShoppingCartModel shoppingCartModel = null;
            if (GetCartCount() <= 0)
                return shoppingCartModel;
            try
            {
                string cookieValue = GetFromCookie(WebStoreConstants.CartCookieKey);
                _shoppingCartsClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
                shoppingCartModel = _shoppingCartsClient.GetShoppingCart(new CartParameterModel
                {
                    CookieMappingId = cookieValue,
                    LocaleId = PortalAgent.LocaleId,
                    PortalId = PortalAgent.CurrentPortal.PortalId,
                    PublishedCatalogId = GetCatalogId().GetValueOrDefault(),
                    UserId = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.UserId
                });
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }

            return shoppingCartModel;
        }
        public override CartViewModel CreateCart(CartItemViewModel cartItem)
        {
            if (IsNotNull(cartItem))
            {
                //Get shopping cart data from session.

                //Bind Portal related data to Shopping cart model.
                ShoppingCartModel shoppingCartModel = IsNull(GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)) && GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey)?.ShoppingCartItems?.Count > 0 ? GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) : new ShoppingCartModel();
                //shoppingCartModel.Coupons = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey)?.Coupons?.Count > 0 ? GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey)?.Coupons : new List<CouponModel>();
                shoppingCartModel = GetShoppingCart(cartItem, shoppingCartModel);
                //Create new cart.
                _shoppingCartsClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
                ShoppingCartModel shoppingCart = _shoppingCartsClient.CreateCart(shoppingCartModel);

                //Bind CartItemViewModel properties to ShoppingCartModel.
                BindCartItemData(cartItem, shoppingCart);

                //assign values which are required for the estimated shipping calculation.
                shoppingCart = GetEstimatedShippingDetails(shoppingCart);

                SaveInCookie(WebStoreConstants.CartCookieKey, shoppingCart.CookieMappingId.ToString(), ZnodeConstant.MinutesInAYear);

                shoppingCart?.ShoppingCartItems.Where(x => x.SKU == cartItem.SKU).Select(x => { x.ProductType = cartItem.ProductType; return x; })?.ToList();

                SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCart);
                //To clear the session for CartCount session key.
                ClearCartCountFromSession();

                return shoppingCart?.ToViewModel<CartViewModel>();
            }
            return null;
        }

    }
}
