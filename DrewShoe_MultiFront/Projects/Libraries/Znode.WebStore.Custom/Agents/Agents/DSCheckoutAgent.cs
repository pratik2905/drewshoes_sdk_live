﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
namespace Znode.WebStore.Custom.Agents.Agents
{
    public class DSCheckoutAgent : CheckoutAgent
    {
        #region protected Variables
        protected readonly IShippingClient _shippingsClient;
        protected readonly IPaymentClient _paymentClient;
        protected readonly IPortalProfileClient _profileClient;
        protected readonly ICustomerClient _customerClient;
        protected readonly IUserClient _userClient;
        protected readonly IOrderClient _orderClient;
        protected readonly ICartAgent _cartAgent;
        protected readonly IUserAgent _userAgent;
        protected readonly IPaymentAgent _paymentAgent;
        protected readonly IAccountClient _accountClient;
        protected readonly IWebStoreUserClient _webStoreAccountClient;
        protected readonly IPortalClient _portalClient;
        protected readonly IShoppingCartClient _shoppingCartClient;
        protected readonly IAddressAgent _addressAgent;
        #endregion

        public DSCheckoutAgent(IShippingClient shippingsClient, IPaymentClient paymentClient, IPortalProfileClient profileClient, ICustomerClient customerClient, IUserClient userClient, IOrderClient orderClient, IAccountClient accountClient, IWebStoreUserClient webStoreAccountClient, IPortalClient portalClient, IShoppingCartClient shoppingCartClient, IAddressClient addressClient)
         : base(shippingsClient, paymentClient, profileClient, customerClient, userClient, orderClient, accountClient, webStoreAccountClient, portalClient, shoppingCartClient, addressClient)
        {
            _shippingsClient = GetClient<IShippingClient>(shippingsClient);
            _paymentClient = GetClient<IPaymentClient>(paymentClient);
            _profileClient = GetClient<IPortalProfileClient>(profileClient);
            _customerClient = GetClient<ICustomerClient>(customerClient);
            _userClient = GetClient<IUserClient>(userClient);
            _orderClient = GetClient<IOrderClient>(orderClient);
            _accountClient = GetClient<IAccountClient>(accountClient);
            _webStoreAccountClient = GetClient<IWebStoreUserClient>(webStoreAccountClient);
            _portalClient = GetClient<IPortalClient>(portalClient);
            _shoppingCartClient = GetClient<IShoppingCartClient>(shoppingCartClient);
            _userAgent = GetService<IUserAgent>();
            _cartAgent = GetService<ICartAgent>();
            _paymentAgent = GetService<IPaymentAgent>();
            _addressAgent = GetService<IAddressAgent>();
        }
        public override OrdersViewModel SubmitOrder(SubmitOrderViewModel submitOrderViewModel)
        {
            ZnodeLogging.LogMessage("SubmitOrder Agent Method Started=:" + submitOrderViewModel.OrderNumber, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);

            OrdersViewModel ovm = base.SubmitOrder(submitOrderViewModel);

            ZnodeLogging.LogMessage("SubmitOrder Agent Method Ended=:" + submitOrderViewModel.OrderNumber, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            return ovm;
        }
    }
}
