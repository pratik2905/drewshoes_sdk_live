﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class DSUserAgent : UserAgent, IUserAgent
    {        
        public DSUserAgent(ICountryClient countryClient, IWebStoreUserClient webStoreAccountClient, IWishListClient wishListClient, IUserClient userClient, IPublishProductClient productClient, ICustomerReviewClient customerReviewClient, IOrderClient orderClient,
          IGiftCardClient giftCardClient, IAccountClient accountClient, IAccountQuoteClient accountQuoteClient, IOrderStateClient orderStateClient, IPortalCountryClient portalCountryClient, IShippingClient shippingClient, IPaymentClient paymentClient, ICustomerClient customerClient, IStateClient stateClient, IPortalProfileClient portalProfileClient) :
            base(countryClient, webStoreAccountClient, wishListClient, userClient, productClient, customerReviewClient, orderClient, giftCardClient, accountClient, accountQuoteClient,
                orderStateClient, portalCountryClient, shippingClient, paymentClient, customerClient, stateClient, portalProfileClient)
        {
           
        }
        public override OrdersViewModel GetOrderDetails(int orderId, int portalId = 0)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(ZnodePortalEnum.PortalId.ToString(), FilterOperators.Equals, PortalAgent.CurrentPortal.PortalId.ToString());

            UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

            if (HelperUtility.IsNotNull(userViewModel) && orderId > 0)
            {
                OrderModel orderModel = portalId > 0 ? GetOrderBasedOnPortalId(orderId, portalId) : _orderClient.GetOrderById(orderId, SetExpandsForReceipt(), filters);

                if (orderModel.OmsOrderId > 0 || userViewModel.UserId == orderModel.UserId || ((orderModel.IsQuoteOrder || userViewModel?.AccountId.GetValueOrDefault() > 0)))
                {
                    List<OrderLineItemModel> orderLineItemListModel = new List<OrderLineItemModel>();

                    //Create new order line item model.
                    CreateSingleOrderLineItem(orderModel, orderLineItemListModel);

                    orderModel.OrderLineItems = orderLineItemListModel;
                    /*Here*/
                    orderModel.TrackingNumber = SetTrackingURL(orderModel);
                    if (orderModel?.OrderLineItems?.Count > 0)
                    {
                        OrdersViewModel orderDetails = orderModel?.ToViewModel<OrdersViewModel>();
                        orderDetails.CurrencyCode = PortalAgent.CurrentPortal?.CurrencyCode;
                        orderDetails.CouponCode = orderDetails.CouponCode?.Replace("<br/>", ", ");
                        orderDetails?.OrderLineItems?.ForEach(x => x.UOM = orderModel?.ShoppingCartModel?.ShoppingCartItems?.Where(y => y.SKU == x.Sku).Select(y => y.UOM).FirstOrDefault());
                        /*Here*/
                        orderDetails?.OrderLineItems?.ForEach(x => x.TrackingNumber = SetTrackingUrl(x.TrackingNumber, orderModel.ShippingId));
                        return orderDetails;
                    }
                }
                return null;
            }
            return new OrdersViewModel();
        }
        private string SetTrackingUrl(string trackingNo, int shippingId)
        {
            string trackingUrl = GetTrackingUrlByShippingId(shippingId);
            return string.IsNullOrEmpty(trackingUrl) ? trackingNo : "<a target=_blank href=" + GetTrackingUrlByShippingId(shippingId) + trackingNo + ">" + trackingNo + "</a>";
        }
        private string GetTrackingUrlByShippingId(int shippingId)
            => _shippingClient.GetShipping(shippingId)?.TrackingUrl;

        /*nivi code start*/
        private string SetTrackingURL(OrderModel ordermodel)
        {
            string TrackingNumbers = "";
            try
            {
                //Get shipping type name based on provided shipping id
                string SPEEDY = Convert.ToString(ConfigurationManager.AppSettings["CustomSpeedeeTrackingUrl"]);
                string UPS = Convert.ToString(ConfigurationManager.AppSettings["CustomUpsTrackingUrl"]);
                string FEDEX = Convert.ToString(ConfigurationManager.AppSettings["CustomFedexTrackingUrl"]);
                string USPS = Convert.ToString(ConfigurationManager.AppSettings["CustomUspsTrackingUrl"]);
                string[] shipping = ordermodel.Custom1?.Split(',');
                string[] TrackNumbers = ordermodel.TrackingNumber?.Split(',');
                //string shippingType = GetShippingType(ordermodel.ShippingId);
                int icount = 0;
                if (TrackNumbers?.Count() > 0)
                {
                    foreach (string shippingname in shipping)
                    {
                        if (ZnodeConstant.UPS == shippingname.ToLower())
                            TrackingNumbers = TrackingNumbers + $"<a target='_blank' href='{UPS + TrackNumbers[icount]}'>{TrackNumbers[icount]} </a>" + ",";
                        else if (ZnodeConstant.FedEx == shippingname.ToLower())
                            TrackingNumbers = TrackingNumbers + $"<a target='_blank' href='{FEDEX + TrackNumbers[icount]}'>{TrackNumbers[icount]} </a>" + ",";
                        else if (ZnodeConstant.USPS == shippingname.ToLower())
                            TrackingNumbers = TrackingNumbers + $"<a target='_blank' href='{USPS + TrackNumbers[icount]}'>{TrackNumbers[icount]} </a>" + ",";
                        else if ("SPEEDEE" == shippingname)
                            TrackingNumbers = TrackingNumbers + $"<a target='_blank' href='{ SPEEDY + TrackNumbers[icount]}'>{TrackNumbers[icount]} </a>" + ",";
                        icount = icount + 1;
                    }
                    if (TrackingNumbers != "")
                    {
                        TrackingNumbers = TrackingNumbers.Remove(TrackingNumbers.Length - 1, 1);
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("Shipping=" + Convert.ToString(ordermodel.Custom1) + " TrackingNo=" + ordermodel.TrackingNumber + "error =" + ex.Message + " stacktrace =" + ex.StackTrace, string.Empty, TraceLevel.Error);
            }
            return TrackingNumbers;
        }
        //The parameters isShippingBillingDifferent and isCreateAccountForGuestUser are not in use currently but for compatibility we have added these paramter, In future release we will remove these parameter
      
        //Check whether address is valid or not.
        protected override BooleanModel IsValidAddress(AddressViewModel addressViewModel)
        {
            if (PortalAgent.CurrentPortal.EnableAddressValidation)
            {
                AddressModel addressModel = addressViewModel.ToModel<AddressModel>();
                addressModel.PublishStateId = (byte)PortalAgent.CurrentPortal.PublishState;
                var issuccess= _shippingClient.IsShippingAddressValid(addressModel);
                return new BooleanModel { IsSuccess = true };
            }
            return new BooleanModel { IsSuccess = true };
        }



    }
}
