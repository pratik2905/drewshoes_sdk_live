﻿using System.Collections.Generic;
using Znode.Engine.WebStore;
namespace Znode.WebStore.Custom.ViewModel
{
    public class DSCustomListViewModel : BaseViewModel
    {
        public List<DSCustomViewModel> InventoryList { get; set; }
    }

    public class DSCustomUpdatePatientNameViewModel : BaseViewModel
    {
        public string PatientName { get; set; }
        public string EMessage { get; set; }
    }

    public class DSCustomGetPatientNameViewModel : BaseViewModel
    {
        public string Sku { get; set; }
        public string PatientName { get; set; }
        public string EMessage { get; set; }
    }
}
