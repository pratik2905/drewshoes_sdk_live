﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.WebStore.Custom.ViewModel
{
    public class DSCustomPatientNameCart
    {
        public string Sku { get; set; }
        public string PatientName { get; set; }
    }
}
