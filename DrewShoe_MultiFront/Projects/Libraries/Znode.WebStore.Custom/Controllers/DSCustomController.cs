﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Web.Mvc;
using Znode.Engine.WebStore.Controllers;
using Znode.Libraries.Framework.Business;
using Znode.WebStore.Custom.Agents.Agents;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Controllers
{
    public class DSCustomController : BaseController
    {
        private readonly IDSCustomAgent _DSCustomAgent;

        public DSCustomController()
        {
            _DSCustomAgent = new DSCustomAgent();
        }

        [HttpGet]
        public virtual ActionResult GetInventoyGrid(int PublishProductId, string Color)
        {
            DSCustomListViewModel list = _DSCustomAgent.GetInventoyGrid(PublishProductId, Color);
            ZnodeLogging.LogMessage("Webstore controller-", TraceLevel.Error.ToString());
            return PartialView("../Customized/_DSPdpGrid", list);

        }
        [HttpPost]
        public ActionResult LogMessagesForOrderLoss(string data)
        {
            if (data.Contains("KlaviyoGetPersonId"))
            {
                try
                {
                    var val = data.Split('|');
                    var Value = val[0].Replace('"', '|').Replace("|", "");
                    string personEmail = "";
                    string personId = GetKlaviyoPersonId(Value);
                    if (personId != "")
                    {
                        personEmail = GetKlaviyoPersonDetails(personId);
                    }
                    ZnodeLogging.LogMessage("Klaviyo-Person-Log:- " + personEmail, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                    return Json(new
                    {
                        isSuccess = true,
                        message = personEmail,
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (System.Exception)
                {
                    return Json(new
                    {
                        isSuccess = false,
                        message = "KlaviyoGetPersonId Error",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                try
                {
                    ZnodeLogging.LogMessage("PAYMENT-TS-LOG:- " + data, ZnodeLogging.Components.Payment.ToString(), TraceLevel.Error);
                    return Json(new
                    {
                        isSuccess = true,
                        message = "Payment ts log Success",
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (System.Exception ex)
                {
                    ZnodeLogging.LogMessage("PAYMENT-TS-ERRORS ex:- " + ex, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                    return Json(new
                    {
                        isSuccess = false,
                        message = "Payment ts log Error",
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        [HttpPost]
        public ActionResult UpdatePatientName(string patientName, string sku, string email, string userId)
        {
            try
            {
                var dts = "";
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ZnodeECommerceDB"].ConnectionString))
                {
                    try
                    {
                        SqlCommand command = new SqlCommand("Nivi_UpdatePatientNameInCart", connection);
                        connection.Open();
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@PatientName", patientName);
                        command.Parameters.AddWithValue("@Sku", sku);
                        command.Parameters.AddWithValue("@Email", email);
                        command.Parameters.AddWithValue("@UserId", Convert.ToInt32(userId));
                        SqlDataReader reader = command.ExecuteReader();

                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        dts = ex.Message;
                    }
                }
                dts = patientName + " " + sku + " " + email + " " + userId;
                return Json(new
                {
                    isSuccess = true,
                    message = dts,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception Ex)
            {
                return Json(new
                {
                    isSuccess = false,
                    message = "error",
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetPatientName(string userId)
        {
            try
            {
                dynamic responseDatas;
                List<DSCustomPatientNameCart> dSCustomPatientNameCartList = new List<DSCustomPatientNameCart>();
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ZnodeECommerceDB"].ConnectionString))
                {
                    try
                    {
                        SqlCommand command = new SqlCommand("Nivi_GetPatientNameInCart", connection);
                        connection.Open();
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@UserId", Convert.ToInt32(userId));
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            DSCustomPatientNameCart _DSCustomPatientNameCart = new DSCustomPatientNameCart()
                            {
                                Sku = reader["Sku"].ToString(),
                                PatientName = reader["PatientName"].ToString()
                            };
                            dSCustomPatientNameCartList.Add(_DSCustomPatientNameCart);
                        }
                        connection.Close();
                        responseDatas = dSCustomPatientNameCartList;
                    }
                    catch (Exception ex)
                    {
                        responseDatas = ex.Message;
                    }
                }
                return Json(new
                {
                    isSuccess = true,
                    message = dSCustomPatientNameCartList,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception Ex)
            {
                return Json(new
                {
                    isSuccess = false,
                    message = Ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        private string GetKlaviyoPersonId(string exchangeId)
        {
            string URL = "https://a.klaviyo.com/api/v2/people/exchange";
            string urlParameters = "?api_key=pk_15a256266172f3366b079c6f9126c66f0d";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var jsonBody = @"{""exchange_id"":""" + exchangeId + "\"}";
            var postRequest = new HttpRequestMessage(HttpMethod.Post, urlParameters)
            {
                Content = new StringContent(jsonBody, Encoding.UTF8, "application/json")
            };
            HttpResponseMessage response = client.SendAsync(postRequest).Result;
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(response);
            string KlaviyoPersonId = "";
            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                JObject jobject = JObject.Parse(content);
                KlaviyoPersonId = jobject.Value<string>("id");
            }
            else
            {
                var l = (int)response.StatusCode;
                var p = response.ReasonPhrase;
            }
            return KlaviyoPersonId;

        }
        private string GetKlaviyoPersonDetails(string personId)
        {
            string URL = "https://a.klaviyo.com/api/v1/person/" + personId;
            string urlParameters = "?api_key=pk_15a256266172f3366b079c6f9126c66f0d";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(response);
            string personEmail = "";
            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                JObject jobject = JObject.Parse(content);
                personEmail = jobject.Value<string>("email");
            }
            else
            {
                var l = (int)response.StatusCode;
                var p = response.ReasonPhrase;
            }
            return personEmail;
        }

        //For Roshommerson Klaviyo Start
        [HttpPost]
        public ActionResult KlaviyoEmailToLandForRoshommerson(string data)
        {
            try
            {
                var Value = data.Replace('"', '|').Replace("|", "");
                string personEmail = "";
                string personId = GetKlaviyoPersonIdForRoshommerson(Value);
                if (personId != "")
                {
                    personEmail = GetKlaviyoPersonDetailsForRoshommerson(personId);
                }
                ZnodeLogging.LogMessage("KlaviyoEmailToLandForRoshommerson:- " + personEmail, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                return Json(new
                {
                    isSuccess = true,
                    message = personEmail,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Exception)
            {
                return Json(new
                {
                    isSuccess = false,
                    message = "KlaviyoGetPersonId Error",
                }, JsonRequestBehavior.AllowGet);
            }
        }
        private string GetKlaviyoPersonIdForRoshommerson(string exchangeId)
        {
            string URL = "https://a.klaviyo.com/api/v2/people/exchange";
            string urlParameters = "?api_key=pk_758742895f65513022e9cf3e62bb87ce15";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var jsonBody = @"{""exchange_id"":""" + exchangeId + "\"}";
            var postRequest = new HttpRequestMessage(HttpMethod.Post, urlParameters)
            {
                Content = new StringContent(jsonBody, Encoding.UTF8, "application/json")
            };
            HttpResponseMessage response = client.SendAsync(postRequest).Result;
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(response);
            string KlaviyoPersonId = "";
            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                JObject jobject = JObject.Parse(content);
                KlaviyoPersonId = jobject.Value<string>("id");
            }
            else
            {
                var l = (int)response.StatusCode;
                var p = response.ReasonPhrase;
            }
            return KlaviyoPersonId;

        }
        private string GetKlaviyoPersonDetailsForRoshommerson(string personId)
        {
            string URL = "https://a.klaviyo.com/api/v1/person/" + personId;
            string urlParameters = "?api_key=pk_758742895f65513022e9cf3e62bb87ce15";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync(urlParameters).Result;
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(response);
            string personEmail = "";
            if (response.IsSuccessStatusCode)
            {
                string content = response.Content.ReadAsStringAsync().Result;
                JObject jobject = JObject.Parse(content);
                personEmail = jobject.Value<string>("email");
            }
            else
            {
                var l = (int)response.StatusCode;
                var p = response.ReasonPhrase;
            }
            return personEmail;
        }
        //For Roshommerson Klaviyo Start
    }
}
