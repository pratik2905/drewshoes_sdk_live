﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Sample.Api.Model.Responses
{
    public class DSCustomResponse : BaseListResponse
    {
        public List<DSCustomModel> InventoryList { get; set; }
        //public List<DSCustomGetCatalogIdModel> CatalogIdList { get; set; }

    }
}
