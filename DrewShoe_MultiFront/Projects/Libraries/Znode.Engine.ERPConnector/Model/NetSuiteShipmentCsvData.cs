﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.ERPConnector.Model
{
    public class NetSuiteShipmentCsvData
    {
        public string WebOrderNumber { get; set; }
        public string NetsuiteOrderNumber { get; set; }
        public string ShipDate { get; set; }
        public string OrderDate { get; set; }
        public string OrderStatus { get; set; }
        public string SKU { get; set; }
        public string MaximumofQuantityShipped { get; set; }
        public string CarrierCode { get; set; }
        public string TrackingNumber { get; set; }
    }
}
