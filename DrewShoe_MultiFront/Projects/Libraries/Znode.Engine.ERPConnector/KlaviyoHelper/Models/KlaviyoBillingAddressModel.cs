﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.ERPConnector.KlaviyoHelper.Models
{
    [DataContract]
    public class KlaviyoBillingAddressModel
    {
        [DataMember(Name = "FirstName")]
        public string FirstName { get; set; }
        [DataMember(Name = "LastName")]
        public string LastName { get; set; }
        [DataMember(Name = "CompanyName")]
        public string CompanyName { get; set; }
        [DataMember(Name = "Address1")]
        public string Address1 { get; set; }
        [DataMember(Name = "Address2")]
        public string Address2 { get; set; }
        [DataMember(Name = "City")]
        public string City { get; set; }
        [DataMember(Name = "Region")]
        public string Region { get; set; }
        [DataMember(Name = "RegionCode")]
        public string RegionCode { get; set; }
        [DataMember(Name = "Country")]
        public string Country { get; set; }
        [DataMember(Name = "CountryCode")]
        public string CountryCode { get; set; }
        [DataMember(Name = "Zip")]
        public string Zip { get; set; }
        [DataMember(Name = "Phone")]
        public string Phone { get; set; }
    }
}
