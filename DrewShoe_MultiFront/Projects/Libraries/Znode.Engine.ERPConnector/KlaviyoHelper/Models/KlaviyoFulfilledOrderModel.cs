﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.ERPConnector.KlaviyoHelper.Models
{
    [DataContract]
    public class KlaviyoFulfilledOrderModel
    {
        [DataMember(Name = "token")]
        public string Token { get; set; } = "VKyiDH";
        [DataMember(Name = "event")]
        public string Event { get; set; } = "Fulfilled Order";
        [DataMember(Name = "customer_properties")]
        public KlaviyoCustomerPropertiesModel CustomerProperties { get; set; }
        [DataMember(Name = "properties")]
        public KlaviyoProperties Properties { get; set; }
    }
}
