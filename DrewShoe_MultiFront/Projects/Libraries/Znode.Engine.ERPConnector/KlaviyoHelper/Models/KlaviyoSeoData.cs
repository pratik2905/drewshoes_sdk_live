﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.ERPConnector.KlaviyoHelper.Models
{
    public class KlaviyoSeoData
    {
        public string SEOUrl { get; set; }
        public string CanonicalURL { get; set; }
    }
}
