﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.ERPConnector.KlaviyoHelper.Models
{
    [DataContract]
    public class KlaviyoOrderLineItemsModel
    {
        [DataMember(Name = "ProductName")]
        public string ProductName { get; set; }
        [DataMember(Name = "SKU")]
        public string SKU { get; set; }
        [DataMember(Name = "ProductUrl")]
        public string ProductUrl { get; set; }
        [DataMember(Name = "ImageUrl")]
        public string ImageUrl { get; set; }
        [DataMember(Name = "ItemDescription")]
        public string ItemDescription { get; set; }
        [DataMember(Name = "ItemQuantity")]
        public string ItemQuantity { get; set; }
        [DataMember(Name = "ItemPrice")]
        public string ItemPrice { get; set; }
        [DataMember(Name = "ItemRowTotal")]
        public string ItemRowTotal { get; set; }
    }
}
