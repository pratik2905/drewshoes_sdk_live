﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.ERPConnector.KlaviyoHelper.Models
{
    [DataContract]
    public class KlaviyoCustomerPropertiesModel
    {
        [DataMember(Name = "$email")]
        public string Email { get; set; }
    }
}
