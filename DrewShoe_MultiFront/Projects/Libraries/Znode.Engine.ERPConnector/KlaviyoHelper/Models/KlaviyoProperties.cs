﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.ERPConnector.KlaviyoHelper.Models
{
    [DataContract]
    public class KlaviyoProperties
    {
        [DataMember(Name = "ShippingAddress")]
        public KlaviyoShippingAddressModel ShippingAddress { get; set; }
        [DataMember(Name = "Items")]
        public List<KlaviyoOrderLineItemsModel> Items { get; set; } = new List<KlaviyoOrderLineItemsModel>();
        [DataMember(Name = "BillingAddress")]
        public KlaviyoBillingAddressModel BillingAddress { get; set; }
        [DataMember(Name = "$value")]
        public decimal Value { get; set; }
    }
}
