﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.ERPConnector.KlaviyoHelper.Models;
using Znode.Libraries.Data;

namespace Znode.Engine.ERPConnector.KlaviyoHelper.Services.Service
{
    public class KlaviyoFulfilledOrderGetSeoData
    {
        public List<KlaviyoSeoData> GetSEOData(ShoppingCartItemModel _ShoppingCartItemModel, int PortalId)
        {
            List<KlaviyoSeoData> klaviyoSeoDatasList = new List<KlaviyoSeoData>();
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ZnodeECommerceDB"].ConnectionString))
            {
                try
                {
                    DataTable dt = new DataTable();
                    SqlCommand command = new SqlCommand("Nivi_GetPublishSeoData", connection);
                    connection.Open();
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@SeoCodes", _ShoppingCartItemModel.SKU);
                    command.Parameters.AddWithValue("@PortalIds", Convert.ToString(PortalId));
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        KlaviyoSeoData _KlaviyoSeoData = new KlaviyoSeoData()
                        {
                            SEOUrl = reader["SEOUrl"].ToString(),
                            CanonicalURL = reader["CanonicalURL"].ToString()
                        };
                        klaviyoSeoDatasList.Add(_KlaviyoSeoData);
                        break;
                    }
                    connection.Close();
                    return klaviyoSeoDatasList;
                }
                catch (Exception ex)
                {
                    return new List<KlaviyoSeoData>();
                }
            }
        }
    }
}
