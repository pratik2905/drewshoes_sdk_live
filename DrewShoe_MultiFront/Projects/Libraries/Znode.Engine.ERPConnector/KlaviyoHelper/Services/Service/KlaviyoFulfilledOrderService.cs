﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.ERPConnector.KlaviyoHelper.Models;
using Znode.Libraries.Framework.Business;
using Newtonsoft.Json;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;

namespace Znode.Engine.ERPConnector.KlaviyoHelper.Services.Service
{
    public class KlaviyoFulfilledOrderService
    {
        private readonly KlaviyoFulfilledOrderGetSeoData _KlaviyoFulfilledOrderGetSeoDataService;

        public KlaviyoFulfilledOrderService()
        {
            _KlaviyoFulfilledOrderGetSeoDataService = new KlaviyoFulfilledOrderGetSeoData();
        }
        public void KlaviyoFulfilledOrder(OrderModel orderModel)
        {
            try
            {
                KlaviyoFulfilledOrderModel _KlaviyoFulfilledOrderModel = MapKlaviyoFulfilledOrderModelData(orderModel);

                string URL = "https://a.klaviyo.com/api/track";
                string urlParameters = "?api_key=pk_15a256266172f3366b079c6f9126c66f0d";
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var jsonBody = Newtonsoft.Json.JsonConvert.SerializeObject(_KlaviyoFulfilledOrderModel);
                var postRequest = new HttpRequestMessage(HttpMethod.Post, urlParameters)
                {
                    Content = new StringContent(jsonBody, Encoding.UTF8, "application/json")
                };
                HttpResponseMessage response = client.SendAsync(postRequest).Result;
                var result = JsonConvert.SerializeObject(response);
                if (response.IsSuccessStatusCode)
                {
                    string content = response.Content.ReadAsStringAsync().Result;
                    ZnodeLogging.LogMessage("KlaviyoFulfilledOrderService Post Method Status Success :- " + JsonConvert.SerializeObject(content), ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                }
                else
                {
                    var l = (int)response.StatusCode;
                    var p = response.ReasonPhrase;
                    ZnodeLogging.LogMessage("KlaviyoFulfilledOrderService Post Method Status Fail :- " + l + " --- " + p, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                }
            }
            catch (Exception Ex)
            {
                ZnodeLogging.LogMessage("KlaviyoFulfilledOrderService Post Method Error Exception :- " + Ex.Message, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            }
        }
        private KlaviyoFulfilledOrderModel MapKlaviyoFulfilledOrderModelData(OrderModel orderModel)
        {
            KlaviyoFulfilledOrderModel klaviyoFulfilledOrderModel = new KlaviyoFulfilledOrderModel();
            List<KlaviyoOrderLineItemsModel> _klaviyoOrderLineItemsModel = new List<KlaviyoOrderLineItemsModel>();
            //var ab = orderModel.ShoppingCartModel.ShoppingCartItems.Select(x=>x.SKU)
            
            try
            {
                foreach (var item in orderModel.OrderLineItems)
                {
                    ShoppingCartItemModel _ShoppingCartItemModel = orderModel.ShoppingCartModel.ShoppingCartItems.Where(y => y.ConfigurableProductSKUs == item.Sku).FirstOrDefault();
                    var SeoData = _KlaviyoFulfilledOrderGetSeoDataService.GetSEOData(_ShoppingCartItemModel, orderModel.PortalId).FirstOrDefault();
                    KlaviyoOrderLineItemsModel model = new KlaviyoOrderLineItemsModel()
                    {
                        ProductName = item.ProductName,
                        SKU = item.Sku,
                        ProductUrl = "https://www.drewshoe.com/" + SeoData?.SEOUrl,
                        ImageUrl = _ShoppingCartItemModel.ImagePath,
                        ItemDescription = item.Description,
                        ItemQuantity = Convert.ToString(Convert.ToInt32(item.Quantity)),
                        ItemPrice = "$" + Convert.ToString(item.Price.ToString("0.00")),
                        ItemRowTotal = "$" + Convert.ToString((item.Quantity * item.Price).ToString("0.00"))
                    };
                    _klaviyoOrderLineItemsModel.Add(model);
                }

                klaviyoFulfilledOrderModel.Token = klaviyoFulfilledOrderModel.Token;
                klaviyoFulfilledOrderModel.Event = klaviyoFulfilledOrderModel.Event;
                klaviyoFulfilledOrderModel.CustomerProperties = new KlaviyoCustomerPropertiesModel()
                {
                    Email = orderModel.ShippingAddress.EmailAddress
                };
                klaviyoFulfilledOrderModel.Properties = new KlaviyoProperties()
                {
                    ShippingAddress = new KlaviyoShippingAddressModel()
                    {
                        FirstName = orderModel.ShippingAddress.FirstName,
                        LastName = orderModel.ShippingAddress.LastName,
                        CompanyName = orderModel.ShippingAddress.CompanyName,
                        Address1 = orderModel.ShippingAddress.Address1,
                        Address2 = orderModel.ShippingAddress.Address2,
                        City = orderModel.ShippingAddress.CityName,
                        Region = orderModel.ShippingAddress.StateName,
                        RegionCode = orderModel.ShippingAddress.StateName,
                        Country = orderModel.ShippingAddress.CountryName,
                        CountryCode = orderModel.ShippingAddress.CountryName,
                        Zip = orderModel.ShippingAddress.PostalCode,
                        Phone = orderModel.ShippingAddress.PhoneNumber,
                    },

                    Items = _klaviyoOrderLineItemsModel,
                    BillingAddress = new KlaviyoBillingAddressModel()
                    {
                        FirstName = orderModel.BillingAddress.FirstName,
                        LastName = orderModel.BillingAddress.LastName,
                        CompanyName = orderModel.BillingAddress.CompanyName,
                        Address1 = orderModel.BillingAddress.Address1,
                        Address2 = orderModel.BillingAddress.Address2,
                        City = orderModel.BillingAddress.CityName,
                        Region = orderModel.BillingAddress.StateCode,
                        RegionCode = orderModel.BillingAddress.StateCode,
                        Country = orderModel.BillingAddress.CountryName,
                        CountryCode = orderModel.BillingAddress.CountryName,
                        Zip = orderModel.BillingAddress.PostalCode,
                        Phone = orderModel.BillingAddress.PhoneNumber,
                    },
                    Value = orderModel.Total
                };
            }
            catch (Exception Ex)
            {
                ZnodeLogging.LogMessage("KlaviyoFulfilledOrderService MapModel Error :- " + Ex.Message, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            }
            return klaviyoFulfilledOrderModel;
        }
    }
}
