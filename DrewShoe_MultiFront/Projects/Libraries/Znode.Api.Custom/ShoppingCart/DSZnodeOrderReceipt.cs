﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Api.Custom.ShoppingCart
{
   public class DSZnodeOrderReceipt : ZnodeOrderReceipt
    {

        private readonly IZnodeRepository<ZnodeOmsOrderDiscount> _orderDiscountRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderDetail> _orderDetailRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderLineItem> _omsOrderLineItemRepository;
        private readonly IZnodeRepository<ZnodeUser> _userRepository;
        private readonly IZnodeRepository<ZnodeAccount> _AccountRepository;
        private string _cultureCode = string.Empty;
        private string _currencyCode = string.Empty;
        public DSZnodeOrderReceipt(ZnodeOrderFulfillment order, ZnodeShoppingCart shoppingCart) : base(order, shoppingCart)
        {
            _orderDiscountRepository = new ZnodeRepository<ZnodeOmsOrderDiscount>();
            _orderDetailRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
            _userRepository = new ZnodeRepository<ZnodeUser>();
            _AccountRepository = new ZnodeRepository<ZnodeAccount>();
            _omsOrderLineItemRepository = new ZnodeRepository<ZnodeOmsOrderLineItem>();
        }
        // Builds the order line item table.
        public override void BuildOrderLineItem(DataTable multipleAddressTable, DataTable orderLineItemTable, DataTable multipleTaxAddressTable)
        {
            List<OrderLineItemModel> OrderLineItemList = Order?.OrderLineItems.GroupBy(p => new { p.OmsOrderShipmentId }).Select(g => g.First()).ToList();
            IEnumerable<OrderShipmentModel> orderShipments = OrderLineItemList.Select(s => s.ZnodeOmsOrderShipment);

            int shipmentCounter = 1;

            foreach (var orderShipment in orderShipments)
            {
                var addressRow = multipleAddressTable.NewRow();

                // If multiple shipping addresses then display the address for each group
                if (orderShipments.Count() > 1)
                {
                    addressRow["ShipmentNo"] = $"Shipment #{shipmentCounter++}{orderShipment.ShipName}";
                    addressRow["ShipTo"] = GetOrderShipmentAddress(orderShipment);
                }

                addressRow["OmsOrderShipmentID"] = orderShipment.OmsOrderShipmentId;
                var counter = 0;

                var readedShoppingCartItems = new List<ZnodeShoppingCartItem>();

                foreach (OrderLineItemModel lineitem in Order.OrderLineItems.Where(x => x.OmsOrderShipmentId == orderShipment.OmsOrderShipmentId))
                {
                    var shoppingCartItems = ((ZnodePortalCart)ShoppingCart).AddressCarts.Where(x => x.OrderShipmentID == orderShipment.OmsOrderShipmentId).SelectMany(x => x.ShoppingCartItems.Cast<ZnodeShoppingCartItem>());

                    lineitem.OrderLineItemCollection.RemoveAll(x => x.OrderLineItemRelationshipTypeId == Convert.ToInt16(ZnodeCartItemRelationshipTypeEnum.AddOns));
                    if (lineitem.OrderLineItemCollection?.Any(x => x.OrderLineItemRelationshipTypeId != Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Bundles)) ?? false)
                    {
                        foreach (OrderLineItemModel childLineItem in lineitem.OrderLineItemCollection)
                        {
                            ZnodeShoppingCartItem shoppingCartItem = null;
                            if (Order.Order.IsQuoteOrder)
                            {
                                //TODO : Right now we are adding these conditions for handling same products with different addons. We will refactor this later.
                                if (readedShoppingCartItems.Where(s => s.SKU.Equals(childLineItem.Sku)).Count() > 0)
                                {
                                    shoppingCartItem = shoppingCartItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && (!string.IsNullOrEmpty(s.SKU) ? childLineItem.Sku.Contains(s.SKU) : false) && s.OrderLineItemRelationshipTypeId.HasValue && !readedShoppingCartItems.Any(n => n.SKU == s.SKU && n.Sequence == s.Sequence && n.Description == s.Description));
                                }
                                else
                                {
                                    shoppingCartItem = shoppingCartItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && (!string.IsNullOrEmpty(s.SKU) ? childLineItem.Sku.Contains(s.SKU) : false) && s.OrderLineItemRelationshipTypeId.HasValue);
                                }
                                if (IsNull(shoppingCartItem))
                                {
                                    if (readedShoppingCartItems.Where(s => s.SKU.Equals(childLineItem.Sku)).Count() > 0)
                                    {
                                        shoppingCartItem = shoppingCartItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && childLineItem.Sku == s.ParentProductSKU && s.OrderLineItemRelationshipTypeId.HasValue && !readedShoppingCartItems.Any(n => n.SKU == s.SKU && n.Sequence == s.Sequence && n.Description == s.Description));
                                    }
                                    else
                                    {
                                        shoppingCartItem = shoppingCartItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && childLineItem.Sku == s.ParentProductSKU && s.OrderLineItemRelationshipTypeId.HasValue);
                                    }

                                }
                            }
                            else
                            {
                                if (readedShoppingCartItems.Where(s => s.SKU.Equals(childLineItem.Sku)).Count() > 0)
                                {
                                    shoppingCartItem = shoppingCartItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && s.SKU == childLineItem.Sku && s.OrderLineItemRelationshipTypeId.HasValue && !readedShoppingCartItems.Any(n => n.SKU == s.SKU && n.Sequence == s.Sequence && n.Description == s.Description && childLineItem?.PersonaliseValuesDetail?.Count <= 0));
                                }
                                else
                                {
                                    shoppingCartItem = shoppingCartItems.FirstOrDefault(s => s.GroupId == lineitem.GroupId && s.SKU == childLineItem.Sku && s.OrderLineItemRelationshipTypeId.HasValue);
                                }
                            }

                            //Get ShoppingCartItem when GroupId is Null
                            if (IsNull(shoppingCartItem))
                            {
                                if (readedShoppingCartItems.Where(s => s.SKU.Equals(childLineItem.Sku)).Count() > 0)
                                {
                                    shoppingCartItem = shoppingCartItems.FirstOrDefault(m => m.ParentProductSKU.Equals(childLineItem.Sku) && !readedShoppingCartItems.Any(n => n.SKU == m.SKU && n.Sequence == m.Sequence && n.Description == m.Description));
                                }
                                else
                                {
                                    shoppingCartItem = shoppingCartItems.FirstOrDefault(m => m.ParentProductSKU.Equals(childLineItem.Sku));
                                }
                            }
                            setGroupProductDetails(lineitem, childLineItem);

                            StringBuilder sb = new StringBuilder();
                            sb.Append(lineitem.ProductName + "<br />");

                            //For binding personalise attribute to Name
                            if (IsNotNull(lineitem.PersonaliseValueList))
                            {
                                sb.Append(GetPersonaliseAttributes(lineitem.PersonaliseValueList));
                            }
                            else
                            {
                                sb.Append(GetPersonaliseAttributesDetail(lineitem.PersonaliseValuesDetail));
                            }

                            if (!String.IsNullOrEmpty(shoppingCartItem?.Product?.DownloadLink?.Trim()))
                            {
                                sb.Append("<a href='" + shoppingCartItem.Product.DownloadLink + "' target='_blank'>Download</a><br />");
                            }

                            if (orderLineItemTable != null)
                            {
                                DataRow orderlineItemDbRow = orderLineItemTable.NewRow();
                                orderlineItemDbRow["ProductImage"] = lineitem.ProductImagePath;
                                orderlineItemDbRow["Name"] = sb.ToString();
                                orderlineItemDbRow["SKU"] = childLineItem.Sku;
                                orderlineItemDbRow["Description"] = lineitem.Description;
                                orderlineItemDbRow["UOMDescription"] = string.Empty;
                                orderlineItemDbRow["Custom1"] ="Product Code:" + childLineItem.Custom1;//Nivi Code For Add Product Code
                                orderlineItemDbRow["Quantity"] = childLineItem.Quantity;
                                if (shoppingCartItem != null)
                                {
                                    orderlineItemDbRow["Price"] = GetFormatPriceWithCurrency(shoppingCartItem.UnitPrice, shoppingCartItem.UOM);
                                    orderlineItemDbRow["ExtendedPrice"] = GetFormatPriceWithCurrency(shoppingCartItem.ExtendedPrice);
                                    orderlineItemDbRow["ShortDescription"] = shoppingCartItem.Product.ShortDescription;
                                }

                                orderlineItemDbRow["OmsOrderShipmentID"] = childLineItem.OmsOrderShipmentId;
                                orderlineItemDbRow["GroupId"] = string.IsNullOrEmpty(lineitem.GroupId) ? Guid.NewGuid().ToString() : lineitem.GroupId;

                                orderLineItemTable.Rows.Add(orderlineItemDbRow);
                            }
                            counter++;
                            if (shoppingCartItem != null)
                                readedShoppingCartItems.Add(shoppingCartItem);
                        }
                    }
                    else
                    {
                        var shoppingCartItem = shoppingCartItems.ElementAt(counter++);

                        foreach (OrderLineItemModel orderLineItem in lineitem.OrderLineItemCollection)
                            setGroupProductDetails(lineitem, orderLineItem);

                        StringBuilder sb = new StringBuilder();
                        sb.Append(lineitem.ProductName + "<br />");

                        if (!String.IsNullOrEmpty(shoppingCartItem.Product.DownloadLink.Trim()))
                        {
                            sb.Append("<a href='" + shoppingCartItem.Product.DownloadLink + "' target='_blank'>Download</a><br />");
                        }

                        if (orderLineItemTable != null)
                        {
                            DataRow orderlineItemDbRow = orderLineItemTable.NewRow();
                            orderlineItemDbRow["ProductImage"] = lineitem.ProductImagePath;
                            orderlineItemDbRow["Name"] = sb.ToString();
                            orderlineItemDbRow["SKU"] = lineitem.Sku;
                            orderlineItemDbRow["Description"] = lineitem.Description;
                            orderlineItemDbRow["UOMDescription"] = string.Empty;
                            orderlineItemDbRow["Quantity"] = lineitem.OrderLineItemRelationshipTypeId.Equals((int)ZnodeCartItemRelationshipTypeEnum.Group) ? lineitem.GroupProductQuantity : Convert.ToString(double.Parse(Convert.ToString(lineitem.Quantity)));
                            orderlineItemDbRow["Price"] = GetFormatPriceWithCurrency(shoppingCartItem.UnitPrice, shoppingCartItem.UOM);
                            orderlineItemDbRow["ExtendedPrice"] = GetFormatPriceWithCurrency(shoppingCartItem.ExtendedPrice);
                            orderlineItemDbRow["OmsOrderShipmentID"] = lineitem.OmsOrderShipmentId;
                            orderlineItemDbRow["ShortDescription"] = shoppingCartItem.Product.ShortDescription;
                            orderlineItemDbRow["GroupId"] = string.IsNullOrEmpty(lineitem.GroupId) ? Guid.NewGuid().ToString() : lineitem.GroupId;
                            orderLineItemTable.Rows.Add(orderlineItemDbRow);
                        }
                    }
                }

                var addressCart = ((ZnodePortalCart)ShoppingCart).AddressCarts.FirstOrDefault(y => y.OrderShipmentID == orderShipment.OmsOrderShipmentId);

                if (addressCart != null && orderShipments.Count() > 1)
                {
                    var globalResourceObject = HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnTitleShipmentSubTotal");
                    if (globalResourceObject != null)
                        BuildOrderShipmentTotalLineItem(globalResourceObject.ToString(), addressCart.SubTotal, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem($"Shipping Cost({addressCart.Shipping.ShippingName})", addressCart.ShippingCost, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnSalesTax")) ? Admin_Resources.LabelSalesTax : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnSalesTax").ToString(), addressCart.SalesTax, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT")) ? Admin_Resources.LabelVAT : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnVAT").ToString(), addressCart.VAT, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST")) ? Admin_Resources.LabelGST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnGST").ToString(), addressCart.GST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST")) ? Admin_Resources.LabelHST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnHST").ToString(), addressCart.HST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);

                    BuildOrderShipmentTotalLineItem(IsNull(HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST")) ? Admin_Resources.LabelPST : HttpContext.GetGlobalResourceObject("CommonCaption", "ColumnPST").ToString(), addressCart.PST, orderShipment.OmsOrderShipmentId, multipleTaxAddressTable);
                }
                multipleAddressTable.Rows.Add(addressRow);
            }
        }

        public override string CreateOrderReceipt(string template)
        {
            if (string.IsNullOrEmpty(template))
                return template;

            //order to bind order details in data tabel
            System.Data.DataTable orderTable = SetOrderData();

            //create order line Item
            DataTable orderlineItemTable = CreateOrderLineItemTable();

            //order to bind order amount details in data tabel
            DataTable orderAmountTable = SetOrderAmountData();

            //create multiple Address
            DataTable multipleAddressTable = CreateOrderAddressTable();

            //create multiple tax address
            DataTable multipleTaxAddressTable = CreateOrderTaxAddressTable();

            //bind line item data
            BuildOrderLineItem(multipleAddressTable, orderlineItemTable, multipleTaxAddressTable);

            ZnodeReceiptHelper receiptHelper = new ZnodeReceiptHelper(template);

            // Parse order table
            receiptHelper.Parse(orderTable.CreateDataReader());

            // Parse order line items table
            receiptHelper.Parse("AddressItems", multipleAddressTable.CreateDataReader());
            foreach (DataRow address in multipleAddressTable.Rows)
            {
                // Parse OrderLineItem
                var filterData = orderlineItemTable.DefaultView;

                List<DataTable> group = filterData.ToTable().AsEnumerable()
                .GroupBy(r => new { Col1 = r["GroupId"] })
                .Select(g => g.CopyToDataTable()).ToList();

                filterData.RowFilter = $"OmsOrderShipmentID={address["OmsOrderShipmentID"]}";
                receiptHelper.ParseWithGroup("LineItems" + address["OmsOrderShipmentID"], group);

                //Parse Tax based on order shipment
                var amountFilterData = multipleTaxAddressTable.DefaultView;
                amountFilterData.RowFilter = $"OmsOrderShipmentID={address["OmsOrderShipmentID"]}";
                receiptHelper.Parse($"AmountLineItems{address["OmsOrderShipmentID"]}", amountFilterData.ToTable().CreateDataReader());
            }
            // Parse order amount table
            receiptHelper.Parse("GrandAmountLineItems", orderAmountTable.CreateDataReader());
            //Replace the Email Template Keys, based on the passed email template parameters.

            // Return the HTML output
            return receiptHelper.Output;
        }
    }
}
