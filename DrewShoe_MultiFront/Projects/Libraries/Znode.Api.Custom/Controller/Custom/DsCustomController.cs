﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Znode.Api.Custom.Cache;
using Znode.Api.Custom.Service;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Helper;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.Responses;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Client.Expands;
using Znode.Api.Custom.Cache.Cache;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Znode.Api.Custom.Controller.Custom
{
    public class DsCustomController : BaseController
    {
        private readonly IDSCustomCache _cache;
        private readonly IDSCustomService _DSCustomService;

        public DsCustomController(IDSCustomService DSCustomService)
        {
            _DSCustomService = DSCustomService;
            _cache = new DSCustomCache(_DSCustomService);
        }

        /// <summary>
        /// Get Portal List.
        /// </summary>
        /// <returns>HttpResponseMessage</returns>
        [HttpGet]
        public HttpResponseMessage GetInventoyGrid(int PublishProductId, string Color)
        //public HttpResponseMessage GetInventoyGrid()
        {
            HttpResponseMessage response;
            try
            {
                if (PublishProductId == 0)
                {
                    ZnodeLogging.LogMessage("PAYMENT-TS-ERROR:- " + Color, ZnodeLogging.Components.Payment.ToString(), TraceLevel.Error);
                    ZnodeLogging.LogMessage("PAYMENT-TS-ERRORS:- " + Color, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                    return CreateNoContentResponse();
                }
                else
                {
                    string data = _cache.GetInventoyGrid(PublishProductId, Color, RouteUri, RouteTemplate);
                    response = string.IsNullOrEmpty(data) ? CreateNoContentResponse() : CreateOKResponse<string>(data);
                    //response = string.IsNullOrEmpty(data) ? CreateNoContentResponse() : CreateOKResponse<OrderListResponse>(data);
                }
            }
            catch (Exception ex)
            {
                //CustomPortalDetailListResponse data = new CustomPortalDetailListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse("error");
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.Portal.ToString());
            }
            return response;
        }

        [HttpGet]
        public string LogMessagesForOrderLoss(string data)
        {
            ZnodeLogging.LogMessage("PAYMENT-TS-ERROR:- " + data, ZnodeLogging.Components.Payment.ToString(),TraceLevel.Error);
            ZnodeLogging.LogMessage("PAYMENT-TS-ERRORS:- " + data, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            return string.Empty;
        }

        [HttpGet]
        public HttpResponseMessage UpdatePatientName(string patientName, string sku, string email, string userId)
        {
            HttpResponseMessage response;
            try
            {
                using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ZnodeECommerceDB"].ConnectionString))
                {
                    try
                    {
                        SqlCommand command = new SqlCommand("Nivi_UpdatePatientNameInCart", connection);
                        connection.Open();
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@PatientName", patientName);
                        command.Parameters.AddWithValue("@Sku", sku);
                        command.Parameters.AddWithValue("@Email", email);
                        command.Parameters.AddWithValue("@UserId", Convert.ToInt32(userId));
                        SqlDataReader reader = command.ExecuteReader();
                        
                        connection.Close();
                        response = CreateOKResponse<string>("Database Error");
                    }
                    catch (Exception ex)
                    {
                        response = CreateOKResponse<string>("Database Internal Exception Error " + ex.Message);
                    }
                }
                response = CreateOKResponse<string>("Success");
            }
            catch (Exception ex)
            {
                //CustomPortalDetailListResponse data = new CustomPortalDetailListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse("error");
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.Portal.ToString());
            }
            return response;
        }
    }
}
