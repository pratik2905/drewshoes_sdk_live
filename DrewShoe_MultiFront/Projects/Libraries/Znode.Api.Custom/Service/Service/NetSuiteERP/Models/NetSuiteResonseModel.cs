﻿using System.Runtime.Serialization;

namespace Znode.Api.Custom.Service.Service.NetSuiteERP.Models
{
    [DataContract]
    public class NetSuiteSuccessResonseModel
    {
        [DataMember(Name = "status")]
        public string Status { get; set; }
        [DataMember(Name = "customer")]
        public CustomerResponseModel Customer { get; set; }
        [DataMember(Name = "order")]
        public OrderResponseModel Order { get; set; }
    }

    [DataContract]
    public class CustomerResponseModel
    {
        [DataMember(Name = "erpId")]
        public string ErpId { get; set; }
    }

    [DataContract]
    public class OrderResponseModel
    {
        [DataMember(Name = "erpOrderId")]
        public string ErpOrderId { get; set; }
    }

    [DataContract]
    public class NetSuiteErrorResonseModel
    {
        [DataMember(Name = "status")]
        public string Status { get; set; }
        [DataMember(Name = "error")]
        public string Error { get; set; }
        [DataMember(Name = "requestData")]
        public object RequestData { get; set; }
    }

    [DataContract]
    public class NetSuiteAuthenticationErrorResonseModel
    {
        [DataMember(Name = "error")]
        public NetSuiteAuthenticationErrorResonseSubModel Error { get; set; }
    }

    [DataContract]
    public class NetSuiteAuthenticationErrorResonseSubModel
    {
        [DataMember(Name = "code")]
        public string Code { get; set; }
        [DataMember(Name = "message")]
        public string Message { get; set; }
    }
}