﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Znode.Api.Custom.Service.Service.NetSuiteERP.Models
{
    [DataContract]
    public class OrderDetailsModel
    {
        [DataMember(Name = "zenodeOrderId")]
        public string ZnodeOrderId { get; set; }
        [DataMember(Name = "storeName")]
        public string StoreName { get; set; }
        [DataMember(Name = "salesRep")]
        public string SalesRep { get; set; }
        [DataMember(Name = "paymentMethod")]
        public string PaymentMethod { get; set; }
        [DataMember(Name = "paymentId")]
        public string PaymentId { get; set; }
        [DataMember(Name = "shipMethod")]
        public string ShipMethod { get; set; }
        [DataMember(Name = "shipCost")]
        public decimal ShipCost { get; set; }
        [DataMember(Name = "couponCode")]
        public string CouponCode { get; set; }
        [DataMember(Name = "discount")]
        public string Discount { get; set; }
        [DataMember(Name = "discountRate")]
        public string DiscountRate { get; set; }
        [DataMember(Name = "tax")]
        public string Tax { get; set; }
        [DataMember(Name = "POnumber")]
        public string PoNumber { get; set; }
        [DataMember(Name = "shipDate")]
        public string ShipDate { get; set; }
        [DataMember(Name = "shippingAddress")]
        public OrderShippingAddress ShippingAddress { get; set; }
        [DataMember(Name = "billingAddress")]
        public OrderBillingAddress BillingAddress { get; set; }
        [DataMember(Name = "items")]
        public List<OrderLineItems> Items { get; set; } = new List<OrderLineItems>();
    }
}