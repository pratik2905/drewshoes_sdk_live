﻿using System.Runtime.Serialization;

namespace Znode.Api.Custom.Service.Service.NetSuiteERP.Models
{
    [DataContract]
    public class NetSuiteCreateOrderModel
    {
        [DataMember(Name = "customer")]
        public CustomerDetailsModel Customer { get; set; }
        [DataMember(Name = "order")]
        public OrderDetailsModel Order { get; set; }
    }
}