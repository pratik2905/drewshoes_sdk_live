﻿using System.Runtime.Serialization;

namespace Znode.Api.Custom.Service.Service.NetSuiteERP.Models
{
    [DataContract]
    public class OrderLineItems
    {
        [DataMember(Name = "item")]
        public string Item { get; set; }
        [DataMember(Name = "quantity")]
        public decimal Quantity { get; set; }
        [DataMember(Name = "uom")]
        public string Uom { get; set; }
        [DataMember(Name = "amount")]
        public decimal Amount { get; set; }
        [DataMember(Name = "patientName")]
        public string PatientName { get; set; }
        [DataMember(Name = "linediscountcode")]
        public string LineDiscountCode { get; set; }
        [DataMember(Name = "linediscountrate")]
        public string LineDiscountRate { get; set; }
    }
}