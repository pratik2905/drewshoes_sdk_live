﻿using System.Runtime.Serialization;

namespace Znode.Api.Custom.Service.Service.NetSuiteERP.Models
{
    [DataContract]
    public class CustomerDetailsModel
    {
        [DataMember(Name = "dealerid")]
        public string DealerId { get; set; }
        [DataMember(Name = "externalid")]
        public string ExternalId { get; set; }
        [DataMember(Name = "name")]
        public string Name { get; set; }
        [DataMember(Name = "companyname")]
        public string CompanyName { get; set; }
        [DataMember(Name = "email")]
        public string Email { get; set; }
        [DataMember(Name = "subsidiary")]
        public string Subsidiary { get; set; }
        [DataMember(Name = "primaryPhone")]
        public string PrimaryPhone { get; set; }
        [DataMember(Name = "shippingAddress")]
        public CustomerShippingAddress ShippingAddress { get; set; }
        [DataMember(Name = "billingAddress")]
        public CustomerBillingAddress BillingAddress { get; set; }
    }
}