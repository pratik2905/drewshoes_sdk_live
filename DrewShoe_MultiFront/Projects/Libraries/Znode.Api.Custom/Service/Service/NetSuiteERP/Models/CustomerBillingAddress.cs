﻿using System.Runtime.Serialization;

namespace Znode.Api.Custom.Service.Service.NetSuiteERP.Models
{
    [DataContract]
    public class CustomerBillingAddress
    {
        [DataMember(Name = "attention")]
        public string Attention { get; set; }
        [DataMember(Name = "addressee")]
        public string Addressee { get; set; }
        [DataMember(Name = "address1")]
        public string Address1 { get; set; }
        [DataMember(Name = "address2")]
        public string Address2 { get; set; }
        [DataMember(Name = "city")]
        public string City { get; set; }
        [DataMember(Name = "state")]
        public string State { get; set; }
        [DataMember(Name = "zip")]
        public string Zip { get; set; }
        [DataMember(Name = "country")]
        public string Country { get; set; }
        [DataMember(Name = "phone")]
        public string Phone { get; set; }
    }
}
