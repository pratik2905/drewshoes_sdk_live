﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Api.Custom.Service.Service.NetSuiteERP.StaticERPSettings
{
    public static class NiviStaticNetSuiteERPSettings
    {
        public static string OauthConsumerKey = Convert.ToString(ConfigurationManager.AppSettings["NetSuiteOauthConsumerKey"]);
        public static string OauthConsumerSecret = Convert.ToString(ConfigurationManager.AppSettings["NetSuiteOauthConsumerSecret"]);
        public static string OauthToken = Convert.ToString(ConfigurationManager.AppSettings["NetSuiteOauthToken"]);
        public static string OauthTokenSecret = Convert.ToString(ConfigurationManager.AppSettings["NetSuiteOauthTokenSecret"]);
        public static string OauthRealm = Convert.ToString(ConfigurationManager.AppSettings["NetSuiteOauthRealm"]);
        public static string OauthUrl = Convert.ToString(ConfigurationManager.AppSettings["NetSuiteOauthUrl"]);
        public static string OauthQueryParamScriptKey = Convert.ToString(ConfigurationManager.AppSettings["NetSuiteOauthQueryParamScriptKey"]);
        public static int OauthQueryParamScriptValue = Convert.ToInt32(ConfigurationManager.AppSettings["NetSuiteOauthQueryParamScriptValue"]);
        public static string OauthQueryParamDeployKey = Convert.ToString(ConfigurationManager.AppSettings["NetSuiteOauthQueryParamDeployKey"]);
        public static int OauthQueryParamDeployValue = Convert.ToInt32(ConfigurationManager.AppSettings["NetSuiteOauthQueryParamDeployValue"]);

        public static string DrewCustomerCode = Convert.ToString(ConfigurationManager.AppSettings["NetSuiteDrewCustomerCode"]);
        public static string DrewContactCode = Convert.ToString(ConfigurationManager.AppSettings["NetSuiteDrewContactCode"]);
        public static int DrewPortalId = Convert.ToInt32(ConfigurationManager.AppSettings["NetSuitePortalIdDrew"]);

        public static string RosCustomerCode = Convert.ToString(ConfigurationManager.AppSettings["NetSuiteRosCustomerCode"]);
        public static string RosContactCode = Convert.ToString(ConfigurationManager.AppSettings["NetSuiteRosContactCode"]);
        public static int RosPortalId = Convert.ToInt32(ConfigurationManager.AppSettings["NetSuitePortalIdRos"]);

        public static string BelliCustomerCode = Convert.ToString(ConfigurationManager.AppSettings["NetSuiteBelliCustomerCode"]);
        public static string BelliContactCode = Convert.ToString(ConfigurationManager.AppSettings["NetSuiteBelliContactCode"]);
        public static int BelliPortalId = Convert.ToInt32(ConfigurationManager.AppSettings["NetSuitePortalIdBellini"]);

        public static int DealerDrewPortalId = Convert.ToInt32(ConfigurationManager.AppSettings["NetSuitePortalIdDealer"]);
    }
}
