﻿using System;
using System.Collections.Generic;
using Znode.Api.Custom.Service.Service.NetSuiteERP.Models;
using Znode.Engine.Api.Models;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using System.Data;
using System.Linq;
using Znode.Api.Custom.Service.Service.NetSuiteERP.StaticERPSettings;
using Znode.Libraries.Framework.Business;
using System.Diagnostics;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Service.Service.NetSuiteERP.Services
{
    public class ModelMapNetSuiteERPService
    {
        private readonly ZnodeRepository<ZnodePortal> _znodePortal;

        public ModelMapNetSuiteERPService()
        {
            _znodePortal = new ZnodeRepository<ZnodePortal>();
        }
        public NetSuiteCreateOrderModel MapModelCreateSalesOrderData(OrderModel orderModel)
        {
            NetSuiteCreateOrderModel netSuiteCreateOrderModel = new Znode.Api.Custom.Service.Service.NetSuiteERP.Models.NetSuiteCreateOrderModel();
            List<OrderLineItems> listOrderLineItems = new List<OrderLineItems>();
            try
            {
                var PortalDetails = _znodePortal.Table.Where(x => x.PortalId == orderModel.PortalId).FirstOrDefault();
                string DealerId = string.Empty;
                string CustomerExtId = string.Empty;
                string PaymentTypes = string.Empty;

                if (orderModel.PortalId == NiviStaticNetSuiteERPSettings.DealerDrewPortalId)
                {
                    DealerId = orderModel.Custom4;
                    CustomerExtId = orderModel.Custom3;
                    PaymentTypes = orderModel.PaymentDisplayName.Contains("Invoice My Account") ? "Web COD" : "Web COD";
                }
                else
                {
                    DealerId = "";
                    CustomerExtId = "";
                    PaymentTypes = orderModel.PaymentDisplayName.Contains("Credit Card") ? "Web Credit Card" :
                        orderModel.PaymentDisplayName.Contains("Paypal") ? "Paypal Express" : "Web COD";
                }
                string couponCode = string.Empty;
                string discountName = string.Empty;
                if (!string.IsNullOrEmpty(orderModel.CouponCode))
                {
                    couponCode = orderModel.CouponCode;
                    ZnodePromotion _znodePromotion = new ZnodeRepository<ZnodePromotion>().Table.Where(x => x.PromoCode == orderModel.CouponCode).FirstOrDefault();
                    ZnodePromotionType _znodePromotionType = new ZnodeRepository<ZnodePromotionType>().Table.Where(x => x.PromotionTypeId == _znodePromotion.PromotionTypeId).FirstOrDefault();
                    discountName = !HelperUtility.IsNull(_znodePromotionType) ? _znodePromotionType?.Name : string.Empty;
                }
                ZnodeOmsOrderDetail _znodeOmsOrderDetail = new ZnodeRepository<ZnodeOmsOrderDetail>().Table.Where(x => x.OmsOrderDetailsId == orderModel.OmsOrderDetailsId).FirstOrDefault();
                netSuiteCreateOrderModel.Customer = new Znode.Api.Custom.Service.Service.NetSuiteERP.Models.CustomerDetailsModel()
                {
                    DealerId = CustomerExtId,
                    ExternalId = "",
                    Name = orderModel.ShippingAddress.FirstName + " " + orderModel.ShippingAddress.LastName,
                    CompanyName = string.IsNullOrEmpty(orderModel.ShippingAddress.CompanyName) ? orderModel.ShippingAddress.FirstName + " " + orderModel.ShippingAddress.LastName : orderModel.ShippingAddress.CompanyName,
                    Email = orderModel.ShippingAddress.EmailAddress,
                    Subsidiary = "Drew", //PortalDetails.StoreName,
                    PrimaryPhone = orderModel.ShippingAddress.PhoneNumber,
                    ShippingAddress = new Znode.Api.Custom.Service.Service.NetSuiteERP.Models.CustomerShippingAddress()
                    {
                        Attention = orderModel.ShippingAddress.FirstName + " " + orderModel.ShippingAddress.LastName,
                        Addressee = string.IsNullOrEmpty(orderModel.ShippingAddress.CompanyName) ? "" : orderModel.ShippingAddress.CompanyName,
                        Address1 = orderModel.ShippingAddress.Address1,
                        Address2 = orderModel.ShippingAddress.Address2,
                        City = orderModel.ShippingAddress.CityName,
                        State = orderModel.ShippingAddress.StateCode,
                        Zip = orderModel.ShippingAddress.PostalCode,
                        Country = orderModel.ShippingAddress.CountryCode,
                        Phone = orderModel.ShippingAddress.PhoneNumber
                    },
                    BillingAddress = new Znode.Api.Custom.Service.Service.NetSuiteERP.Models.CustomerBillingAddress()
                    {
                        Attention = orderModel.BillingAddress.FirstName + " " + orderModel.BillingAddress.LastName,
                        Addressee = string.IsNullOrEmpty(orderModel.BillingAddress.CompanyName) ? "" : orderModel.BillingAddress.CompanyName,
                        Address1 = orderModel.BillingAddress.Address1,
                        Address2 = orderModel.BillingAddress.Address2,
                        City = orderModel.BillingAddress.CityName,
                        State = orderModel.BillingAddress.StateCode,
                        Zip = orderModel.BillingAddress.PostalCode,
                        Country = orderModel.BillingAddress.CountryCode,
                        Phone = orderModel.BillingAddress.PhoneNumber
                    }
                };
                orderModel.OrderLineItems.ForEach(x =>
                {
                    listOrderLineItems.Add(new OrderLineItems()
                    {
                        Item = x.Custom1,
                        Quantity = x.Quantity,
                        Uom = "PR",
                        Amount = x.Price,
                        PatientName = !string.IsNullOrEmpty(x.Custom5) ? x.Custom5 : "",
                        LineDiscountCode = (!HelperUtility.IsNull(x.DiscountAmount) ? x.DiscountAmount : 0) == 0 ? "" : discountName, //"Web Discount"
                        LineDiscountRate = (!HelperUtility.IsNull(x.DiscountAmount) ? x.DiscountAmount : 0) == 0 ? "" : Convert.ToString(x.DiscountAmount)
                    });
                });
                netSuiteCreateOrderModel.Order = new Znode.Api.Custom.Service.Service.NetSuiteERP.Models.OrderDetailsModel()
                {
                    ZnodeOrderId = orderModel.OrderNumber,
                    StoreName = PortalDetails.StoreName,
                    SalesRep = PortalDetails.AdminEmail,  //"testemail@testemail.com",
                    PaymentMethod = PaymentTypes,
                    PaymentId = _znodeOmsOrderDetail.PaymentTransactionToken,
                    ShipMethod = orderModel.ShippingCode, //orderModel.ShippingTypeName,
                    ShipCost = orderModel.ShippingCost,
                    CouponCode = couponCode,
                    Discount = discountName, //"Web Discount"
                    DiscountRate = discountName == "Percent Off Product" ? "" : orderModel.DiscountAmount == 0 ? "" : Convert.ToString(orderModel.DiscountAmount),
                    Tax = Convert.ToString(orderModel.TaxCost),
                    PoNumber = orderModel.PurchaseOrderNumber,
                    ShipDate = !string.IsNullOrEmpty(orderModel.Custom5) ? orderModel.Custom5 : DateTime.Now.ToString("MM/dd/yyyy"),
                    ShippingAddress = new Znode.Api.Custom.Service.Service.NetSuiteERP.Models.OrderShippingAddress()
                    {
                        Attention = orderModel.ShippingAddress.FirstName + " " + orderModel.ShippingAddress.LastName,
                        Addressee = string.IsNullOrEmpty(orderModel.ShippingAddress.CompanyName) ? "" : orderModel.ShippingAddress.CompanyName,
                        Address1 = orderModel.ShippingAddress.Address1,
                        Address2 = orderModel.ShippingAddress.Address2,
                        City = orderModel.ShippingAddress.CityName,
                        State = orderModel.ShippingAddress.StateCode,
                        Zip = orderModel.ShippingAddress.PostalCode,
                        Country = orderModel.ShippingAddress.CountryCode
                    },
                    BillingAddress = new Znode.Api.Custom.Service.Service.NetSuiteERP.Models.OrderBillingAddress()
                    {
                        Attention = orderModel.BillingAddress.FirstName + " " + orderModel.BillingAddress.LastName,
                        Addressee = string.IsNullOrEmpty(orderModel.BillingAddress.CompanyName) ? "" : orderModel.BillingAddress.CompanyName,
                        Address1 = orderModel.BillingAddress.Address1,
                        Address2 = orderModel.BillingAddress.Address2,
                        City = orderModel.BillingAddress.CityName,
                        State = orderModel.BillingAddress.StateCode,
                        Zip = orderModel.BillingAddress.PostalCode,
                        Country = orderModel.BillingAddress.CountryCode
                    },
                    Items = listOrderLineItems
                };
            }
            catch (Exception Ex)
            {
                ZnodeLogging.LogMessage("Net Suite Create ModelMap Error :- " + Ex.Message, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            }
            return netSuiteCreateOrderModel;
        }
    }
}
