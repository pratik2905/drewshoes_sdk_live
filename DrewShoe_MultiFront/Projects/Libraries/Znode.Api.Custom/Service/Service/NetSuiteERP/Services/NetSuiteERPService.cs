﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Authenticators.OAuth;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Service.Service.NetSuiteERP.Models;
using Znode.Api.Custom.Service.Service.NetSuiteERP.StaticERPSettings;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Service.Service.NetSuiteERP.Services
{
    public class NetSuiteERPService
    {
        private readonly ModelMapNetSuiteERPService _modelMapNetSuiteERPService;
        private readonly ZnodeRepository<ZnodeOmsOrderDetail> _omsOrderDetailRepository;
        private readonly string oauthconsumerKey;
        private readonly string oauthconsumerSecret;
        private readonly string oauthToken;
        private readonly string oauthTokenSecret;
        private readonly string oauthRealm;
        private readonly string oauthUrl;
        private readonly string oauthQueryParamScriptKey;
        private readonly int oauthQueryParamScriptValue;
        private readonly string oauthQueryParamDeployKey;
        private readonly int oauthQueryParamDeployValue;

        public NetSuiteERPService()
        {
            _modelMapNetSuiteERPService = new ModelMapNetSuiteERPService();
            _omsOrderDetailRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
            oauthconsumerKey = NiviStaticNetSuiteERPSettings.OauthConsumerKey;
            oauthconsumerSecret = NiviStaticNetSuiteERPSettings.OauthConsumerSecret;
            oauthToken = NiviStaticNetSuiteERPSettings.OauthToken;
            oauthTokenSecret = NiviStaticNetSuiteERPSettings.OauthTokenSecret;
            oauthRealm = NiviStaticNetSuiteERPSettings.OauthRealm;
            oauthUrl = NiviStaticNetSuiteERPSettings.OauthUrl;
            oauthQueryParamScriptKey = NiviStaticNetSuiteERPSettings.OauthQueryParamScriptKey;
            oauthQueryParamScriptValue = NiviStaticNetSuiteERPSettings.OauthQueryParamScriptValue;
            oauthQueryParamDeployKey = NiviStaticNetSuiteERPSettings.OauthQueryParamDeployKey;
            oauthQueryParamDeployValue = NiviStaticNetSuiteERPSettings.OauthQueryParamDeployValue;
        }
        public NetSuiteSuccessResonseModel NetSuiteCreateSalesOrder(Znode.Api.Custom.Service.Service.NetSuiteERP.Models.NetSuiteCreateOrderModel netSuiteCreateOrderModel, Engine.Api.Models.OrderModel orderModel)
        {
            Znode.Api.Custom.Service.Service.NetSuiteERP.Models.NetSuiteSuccessResonseModel netSuiteSuccessResonseModel = new Znode.Api.Custom.Service.Service.NetSuiteERP.Models.NetSuiteSuccessResonseModel();
            try
            {
                ZnodeLogging.LogMessage("Net Suite Create SalesOrder OrderModelMain:- " + JsonConvert.SerializeObject(orderModel), ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                var client = new RestClient(oauthUrl);
                client.AddDefaultParameter(oauthQueryParamScriptKey, oauthQueryParamScriptValue, ParameterType.QueryString);
                client.AddDefaultParameter(oauthQueryParamDeployKey, oauthQueryParamDeployValue, ParameterType.QueryString);
                OAuth1Authenticator _OAuth1Authenticator = OAuth1Authenticator.ForAccessToken(
                            oauthconsumerKey, oauthconsumerSecret, oauthToken, oauthTokenSecret,
                            OAuthSignatureMethod.HmacSha256);
                _OAuth1Authenticator.Realm = oauthRealm;
                _OAuth1Authenticator.ParameterHandling = OAuthParameterHandling.HttpAuthorizationHeader;
                client.Authenticator = _OAuth1Authenticator;
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                var JsonBody = Newtonsoft.Json.JsonConvert.SerializeObject(netSuiteCreateOrderModel);
                ZnodeLogging.LogMessage("Net Suite Create SalesOrder JsonBody:- " + JsonBody, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                request.AddParameter("application/json", JsonBody, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                ZnodeOmsOrderDetail createdOrder = new ZnodeRepository<ZnodeOmsOrderDetail>().Table.Where(x => x.OmsOrderId == orderModel.OmsOrderId).FirstOrDefault();
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JObject jobject = JObject.Parse(response.Content);
                    string ResponseStatus = jobject.Value<string>("status");
                    if (ResponseStatus == "Success")
                    {
                        NetSuiteSuccessResonseModel _netSuiteSuccessResonseModel = JsonConvert.DeserializeObject<NetSuiteSuccessResonseModel>(response.Content);
                        createdOrder.ExternalId = Convert.ToString(_netSuiteSuccessResonseModel.Order.ErpOrderId);
                        createdOrder.Custom2 = _netSuiteSuccessResonseModel.Customer.ErpId;
                        _omsOrderDetailRepository.Update(createdOrder);
                        ZnodeLogging.LogMessage("Net Suite Create SalesOrder Success :- erpId:- " + _netSuiteSuccessResonseModel.Customer.ErpId + " erpOrderId:- " + _netSuiteSuccessResonseModel.Order.ErpOrderId + " status:- " + _netSuiteSuccessResonseModel.Status, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                    }
                    else if (ResponseStatus == "Error")
                    {
                        NetSuiteErrorResonseModel _netSuiteErrorResonseModel = JsonConvert.DeserializeObject<NetSuiteErrorResonseModel>(response.Content);
                        int strLength = Convert.ToInt32(_netSuiteErrorResonseModel?.Error.Length);
                        int needStrLength = strLength < 200 ? strLength : 200;
                        string resultResponse = Convert.ToString(_netSuiteErrorResonseModel?.Error).Substring(0, needStrLength);
                        createdOrder.ExternalId = resultResponse;
                        createdOrder.OmsOrderStateId = 50;//PENDING APPROVAL    
                        _omsOrderDetailRepository.Update(createdOrder);
                        ZnodeLogging.LogMessage("Net Suite Create SalesOrder Error :- status:- " + _netSuiteErrorResonseModel.Status + " ErrorMessage:- " + _netSuiteErrorResonseModel.Error + " RequestBody:- " + JsonConvert.SerializeObject(_netSuiteErrorResonseModel.RequestData), ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                    }
                    else
                    {
                        NetSuiteAuthenticationErrorResonseSubModel _netSuiteAuthenticationErrorResonseSubModel = JsonConvert.DeserializeObject<NetSuiteAuthenticationErrorResonseSubModel>(response.Content);
                        createdOrder.ExternalId = Convert.ToString(_netSuiteAuthenticationErrorResonseSubModel?.Code);
                        createdOrder.OmsOrderStateId = 50;//PENDING APPROVAL    
                        _omsOrderDetailRepository.Update(createdOrder);
                        ZnodeLogging.LogMessage("Net Suite Create SalesOrder Authentication Success Error :- code:- " + _netSuiteAuthenticationErrorResonseSubModel.Code + " ErrorMessage:- " + _netSuiteAuthenticationErrorResonseSubModel.Message, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                    }
                }
                else
                {
                    NetSuiteAuthenticationErrorResonseSubModel _netSuiteAuthenticationErrorResonseSubModel = JsonConvert.DeserializeObject<NetSuiteAuthenticationErrorResonseSubModel>(response.Content);
                    createdOrder.ExternalId = Convert.ToString(_netSuiteAuthenticationErrorResonseSubModel?.Code);
                    createdOrder.OmsOrderStateId = 50;//PENDING APPROVAL              
                    _omsOrderDetailRepository.Update(createdOrder);
                    ZnodeLogging.LogMessage("Net Suite Create SalesOrder Authentication Error :- code:- " + _netSuiteAuthenticationErrorResonseSubModel.Code + " ErrorMessage:- " + _netSuiteAuthenticationErrorResonseSubModel.Message, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                }
            }
            catch (Exception Ex)
            {
                ZnodeOmsOrderDetail _createdOrder = new ZnodeRepository<ZnodeOmsOrderDetail>().Table.Where(x => x.OmsOrderId == orderModel.OmsOrderId).FirstOrDefault();
                _createdOrder.OmsOrderStateId = 50;//PENDING APPROVAL              
                _omsOrderDetailRepository.Update(_createdOrder);
                ZnodeLogging.LogMessage("Net Suite Create SalesOrder Error :- " + Ex.Message, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            }
            return netSuiteSuccessResonseModel;
        }
    }
}
