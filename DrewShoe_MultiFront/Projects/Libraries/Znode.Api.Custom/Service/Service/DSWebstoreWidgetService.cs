﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.Helper;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;


namespace Znode.Api.Custom.Service.Service
{
    public class DSWebstoreWidgetService : WebStoreWidgetService
    {
        
        string PortalIdDrew = ConfigurationManager.AppSettings["PortalIDDrew"];
        string PortalIdRos = ConfigurationManager.AppSettings["PortalIDRos"];
        string PortalIdBellini = ConfigurationManager.AppSettings["PortalIDBellini"];
        public DSWebstoreWidgetService() : base()
        {          
        }

        #region Base Methods Override 9.6.2.1

        public override WebStoreLinkProductListModel GetLinkProductList(WebStoreWidgetParameterModel parameter, NameValueCollection expands)
        {
            WebStoreLinkProductListModel webStoreLinkProductListModel = new WebStoreLinkProductListModel();
            webStoreLinkProductListModel = base.GetLinkProductList(parameter, expands);
            webStoreLinkProductListModel = GetSwatchImagesForLinkProductsOnPDP(webStoreLinkProductListModel, parameter.PortalId);
            return webStoreLinkProductListModel;
        }

        private WebStoreLinkProductListModel GetSwatchImagesForLinkProductsOnPDP(WebStoreLinkProductListModel webStoreLinkProductListModel, int portalId)
        {

            DSAttributeSwatchHelper dsAttributeSwatchHelper = new Helper.DSAttributeSwatchHelper();
            int? _linkproductcount = webStoreLinkProductListModel.LinkProductList?.Count;
            for (int i = 0; i < _linkproductcount; i++)
            {
                if (portalId == Convert.ToInt32(PortalIdDrew))
                {
                    dsAttributeSwatchHelper.GetpublishedConfigurableProducts(webStoreLinkProductListModel.LinkProductList[i].PublishProduct, portalId, "drewcolor");
                }
                else if (portalId == Convert.ToInt32(PortalIdRos))
                {
                    dsAttributeSwatchHelper.GetpublishedConfigurableProducts(webStoreLinkProductListModel.LinkProductList[i].PublishProduct, portalId, "roscolor");
                }
                else if (portalId == Convert.ToInt32(PortalIdBellini))
                {
                    dsAttributeSwatchHelper.GetpublishedConfigurableProducts(webStoreLinkProductListModel.LinkProductList[i].PublishProduct, portalId, "roscolor");
                }
                else
                {
                    dsAttributeSwatchHelper.GetpublishedConfigurableProducts(webStoreLinkProductListModel.LinkProductList[i].PublishProduct, portalId, "drewcolor");
                }
            }
            return webStoreLinkProductListModel;

        }

        private List<PublishProductModel> AssignSwatchImages(List<PublishProductModel> publishProducts, int portalId)
        {
            DSAttributeSwatchHelper dsAttributeSwatchHelper = new Helper.DSAttributeSwatchHelper();
            if (portalId == Convert.ToInt32(PortalIdDrew))
            {
                dsAttributeSwatchHelper.GetpublishedConfigurableProducts(publishProducts, portalId, "drewcolor");
            }
            else if (portalId == Convert.ToInt32(PortalIdRos))
            {
                dsAttributeSwatchHelper.GetpublishedConfigurableProducts(publishProducts, portalId, "roscolor");
            }
            else if (portalId == Convert.ToInt32(PortalIdBellini))
            {
                dsAttributeSwatchHelper.GetpublishedConfigurableProducts(publishProducts, portalId, "roscolor");
            }
            else
            {
                dsAttributeSwatchHelper.GetpublishedConfigurableProducts(publishProducts, portalId, "drewcolor");
            }

            return publishProducts;
        }

        private WebStoreWidgetProductListModel ToWebstoreWidgetProductListModel(List<PublishProductModel> model)
        {
            WebStoreWidgetProductListModel webStoreWidgetProductListModel = new WebStoreWidgetProductListModel();
            try
            {
                bool modelIsNull = model == null;
                ZnodeLogging.LogMessage("DsWebstoreWidgetService-ToWebstoreWidgetProductListModel-Inner-start model:- " + modelIsNull, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                ZnodeLogging.LogMessage("DsWebstoreWidgetService-ToWebstoreWidgetProductListModel-webStoreWidgetProductListModel.Products-objectInitialization-start", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                webStoreWidgetProductListModel.Products = new List<WebStoreWidgetProductModel>();
                ZnodeLogging.LogMessage("DsWebstoreWidgetService-ToWebstoreWidgetProductListModel-webStoreWidgetProductListModel.Products-objectInitialization-end", ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                bool HelperUtilityIsModelNull1 = HelperUtility.IsNotNull(model);
                bool HelperUtilityIsModelNull2 = model?.Count > 0;
                if (HelperUtility.IsNotNull(model) && model?.Count > 0)
                {
                    foreach (PublishProductModel data in model)
                    {
                        ZnodeLogging.LogMessage("DsWebstoreWidgetService-ToWebstoreWidgetProductListModel-foreach-start " + data, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                        WebStoreWidgetProductModel webStoreWidgetProductModel = new WebStoreWidgetProductModel();
                        webStoreWidgetProductModel.WebStoreProductModel = MapData(data);
                        webStoreWidgetProductListModel.Products.Add(webStoreWidgetProductModel);
                    }
                }
                ZnodeLogging.LogMessage("DsWebstoreWidgetService-ToWebstoreWidgetProductListModel-Inner-end model:- " + modelIsNull + " HelperUtilityIsModelNull1:- " + HelperUtilityIsModelNull1 + " HelperUtilityIsModelNull2:- " + HelperUtilityIsModelNull2, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("DsWebstoreWidgetService-ToWebstoreWidgetProductListModel-Inner-start ex:- " + ex, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            }
            return webStoreWidgetProductListModel;
        }

        private WebStoreProductModel MapData(PublishProductModel model)
        {
            WebStoreProductModel webStoreProductModel = new WebStoreProductModel();
            try
            {
                ZnodeLogging.LogMessage("DsWebstoreWidgetService-MapData-start model:- " + model, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                webStoreProductModel.Attributes = model.Attributes;
                webStoreProductModel.LocaleId = model.LocaleId;
                webStoreProductModel.CatalogId = model.PublishedCatalogId;
                webStoreProductModel.PublishProductId = model.PublishProductId;
                webStoreProductModel.Name = model.Name;
                webStoreProductModel.RetailPrice = model.RetailPrice;
                webStoreProductModel.SalesPrice = model.SalesPrice;
                webStoreProductModel.PromotionalPrice = model.PromotionalPrice;
                webStoreProductModel.SKU = model.SKU;
                webStoreProductModel.ProductReviews = model.ProductReviews;
                webStoreProductModel.CurrencyCode = model.CurrencyCode;
                webStoreProductModel.ImageMediumPath = model.ImageMediumPath;
                webStoreProductModel.ImageSmallPath = model.ImageSmallPath;
                webStoreProductModel.ImageSmallThumbnailPath = model.ImageSmallThumbnailPath;
                webStoreProductModel.ImageThumbNailPath = model.ImageThumbNailPath;
                webStoreProductModel.ImageLargePath = model.ImageLargePath;
                webStoreProductModel.SEODescription = model.SEODescription;
                webStoreProductModel.SEOTitle = model.SEOTitle;
                webStoreProductModel.Rating = model.Rating;
                webStoreProductModel.TotalReviews = model.TotalReviews;
                webStoreProductModel.SEOUrl = model.SEOUrl;
                webStoreProductModel.SEOKeywords = model.SEOKeywords;
                webStoreProductModel.GroupProductPriceMessage = model.GroupProductPriceMessage;
                webStoreProductModel.Promotions = model.Promotions;
                webStoreProductModel.AlternateImages = model.AlternateImages;
                ZnodeLogging.LogMessage("DsWebstoreWidgetService-MapData-end model:- " + model, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                //return new WebStoreProductModel
                //{
                //    Attributes = model.Attributes,
                //    LocaleId = model.LocaleId,
                //    CatalogId = model.PublishedCatalogId,
                //    PublishProductId = model.PublishProductId,
                //    Name = model.Name,
                //    RetailPrice = model.RetailPrice,
                //    SalesPrice = model.SalesPrice,
                //    PromotionalPrice = model.PromotionalPrice,
                //    SKU = model.SKU,
                //    ProductReviews = model.ProductReviews,
                //    CurrencyCode = model.CurrencyCode,
                //    ImageMediumPath = model.ImageMediumPath,
                //    ImageSmallPath = model.ImageSmallPath,
                //    ImageSmallThumbnailPath = model.ImageSmallThumbnailPath,
                //    ImageThumbNailPath = model.ImageThumbNailPath,
                //    ImageLargePath = model.ImageLargePath,
                //    SEODescription = model.SEODescription,
                //    SEOTitle = model.SEOTitle,
                //    Rating = model.Rating,
                //    TotalReviews = model.TotalReviews,
                //    SEOUrl = model.SEOUrl,
                //    SEOKeywords = model.SEOKeywords,
                //    GroupProductPriceMessage = model.GroupProductPriceMessage,
                //    Promotions = model.Promotions,
                //    AlternateImages = model.AlternateImages
                //};
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("DsWebstoreWidgetService-MapData-end ex:- " + ex, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            }
            return webStoreProductModel;
        }

        private FilterCollection CreateFilterCollectionForProducts(WebStoreWidgetParameterModel parameter)
      => new FilterCollection() {
                new FilterTuple("MappingId", FilterOperators.Equals, parameter.CMSMappingId.ToString()),
                new FilterTuple(ZnodeCMSWidgetSliderBannerEnum.WidgetsKey.ToString(), FilterOperators.Like, parameter.WidgetKey),
                new FilterTuple(ZnodeCMSWidgetSliderBannerEnum.TypeOFMapping.ToString(), FilterOperators.Like, parameter.TypeOfMapping),
        };
        #endregion

        #region Base Methods Override 9.6.2.1

        //Get Product list widget with product details.
        public override WebStoreWidgetProductListModel GetProducts(WebStoreWidgetParameterModel parameter, NameValueCollection expands)
        {
            WebStoreWidgetProductListModel listModel = new WebStoreWidgetProductListModel();
            try
            {
                ZnodeLogging.LogMessage("DsWebstoreWidgetService-GetProducts-start parameter:- " + parameter + " expands:- " + expands, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);

                PublishProductListModel model = new PublishProductListModel();

                List<ZnodePublishWidgetProductEntity> productListWidgetEntity = publishedPortalDataService.GetProductWidget(GetFilters(parameter));

                int? versionId = GetCatalogVersionId();

                List<ZnodePublishCategoryEntity> categoryListWidgetEntity = ZnodeDependencyResolver.GetService<IPublishedCategoryDataService>().GetCategoryListByCatalogId(parameter.PublishCatalogId, parameter.LocaleId).Where(x => x.VersionId == versionId && x.IsActive).ToList();

                List<string> SKUs = productListWidgetEntity?.OrderBy(a => a.DisplayOrder).Select(x => x.SKU)?.ToList();
                foreach (ZnodePublishCategoryEntity item in categoryListWidgetEntity)
                {
                    if ((item.ActivationDate == null || item.ActivationDate.GetValueOrDefault().Date <= HelperUtility.GetDate()) && (item.ExpirationDate == null || item.ExpirationDate.GetValueOrDefault().Date >= HelperUtility.GetDate()))
                        parameter.CategoryIds += item.ZnodeCategoryId.ToString() + ",";
                }

                //if (SKUs?.Count > 0)
                //    model = GetPublishProducts(expands, ProductListFilters(parameter, SKUs), null, null);

                //ZnodeLogging.LogMessage("Count of PublishProducts list in PublishProductListModel returned from method GetPublishProducts: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, model?.PublishProducts?.Count);

                //WebStoreWidgetProductListModel listModel = ToWebstoreWidgetProductListModel(model?.PublishProducts, productListWidgetEntity);

                //listModel.DisplayName = parameter.DisplayName;
                //ZnodeLogging.LogMessage("DisplayName property of WebStoreWidgetProductListModel", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, listModel?.DisplayName);
                //ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
                //return listModel;

                //nivicode start
                expands = new NameValueCollection();
                if (SKUs?.Count > 0)
                    // model = GetPublishProducts(expands, ProductListFilters(parameter, string.Join(",", SKUs)), null, null);
                    model = GetPublishProducts(expands, ProductListFilters(parameter, SKUs), null, null);
                if (model.PublishProducts != null && model.PublishProducts.Count > 0)
                {
                    model.PublishProducts = AssignSwatchImages(model?.PublishProducts, parameter.PortalId);
                }
                ZnodeLogging.LogMessage("DsWebstoreWidgetService-ToWebstoreWidgetProductListModel-outer-start parameter:- " + model?.PublishProducts, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                listModel = ToWebstoreWidgetProductListModel(model?.PublishProducts);
                ZnodeLogging.LogMessage("DsWebstoreWidgetService-ToWebstoreWidgetProductListModel-outer-end parameter:- " + model?.PublishProducts, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                listModel.DisplayName = parameter.DisplayName;
                ZnodeLogging.LogMessage("DsWebstoreWidgetService-GetProducts-end parameter:- " + parameter + " expands:- " + expands + " parameterDisplayName:- " + parameter.DisplayName, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
                //return listModel;
                //nivicode End
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("DsWebstoreWidgetService-GetProducts-ex " + ex, ZnodeLogging.Components.WebstoreApplicationError.ToString(), TraceLevel.Error);
            }
            return listModel;
        }
        private FilterCollection GetFilters(WebStoreWidgetParameterModel parameter)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add("MappingId", FilterOperators.Equals, parameter.CMSMappingId.ToString());
            filters.Add("WidgetsKey", FilterOperators.Is, parameter.WidgetKey);
            filters.Add("TypeOFMapping", FilterOperators.Like, parameter.TypeOfMapping);
            filters.Add("VersionId", FilterOperators.Equals, WebstoreVersionId.ToString());
            return filters;
        }
        //Generate filters to render product list.
        private FilterCollection ProductListFilters(WebStoreWidgetParameterModel parameter, List<string> SKUs)
        {
            FilterCollection filter = new FilterCollection();
            if (SKUs?.Count > 0)
                filter.Add(new FilterTuple(WebStoreEnum.SKU.ToString(), FilterOperators.In, string.Join(",", SKUs.Select(x => $"\"{x}\""))));
            filter.Add(new FilterTuple(FilterKeys.LocaleId, FilterOperators.Equals, parameter.LocaleId.ToString()));
            filter.Add(new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, parameter.PortalId.ToString()));
            filter.Add(new FilterTuple(ZnodeLocaleEnum.IsActive.ToString(), FilterOperators.Equals, ZnodeConstant.TrueValue));
            filter.Add(FilterKeys.ProductIndex, FilterOperators.Equals, ZnodeConstant.DefaultPublishProductIndex.ToString());
            filter.Add(new FilterTuple(WebStoreEnum.ZnodeCatalogId.ToString(), FilterOperators.Equals, parameter.PublishCatalogId.ToString()));
            if (!string.IsNullOrEmpty(parameter.CategoryIds))
                filter.Add(new FilterTuple(FilterKeys.CategoryIds, FilterOperators.In, parameter.CategoryIds?.TrimEnd(',')));
            return filter;
        }

        #endregion
    }
}
