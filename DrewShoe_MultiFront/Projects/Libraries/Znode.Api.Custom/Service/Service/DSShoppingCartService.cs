﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Engine.Promotions;
using Znode.Engine.Shipping;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using Znode.Libraries.Admin;
using System.Configuration;

namespace Znode.Api.Custom.Service.Service
{
    public class DSShoppingCartService : ShoppingCartService
    {

        #region Private Variables
        private readonly IZnodeRepository<ZnodeOmsCookieMapping> _cookieMappingRepository;
        private readonly IZnodeRepository<ZnodeOmsSavedCart> _omsSavedRepository;
        private readonly IZnodeRepository<ZnodeOmsSavedCartLineItem> _savedCartLineItemService;
        private readonly IZnodeRepository<ZnodeOmsOrderDetail> _orderDetailRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderLineItem> _orderLineItemRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderLineItemRelationshipType> _lineItemRelationshipType;
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
        private readonly IZnodeRepository<ZnodeUser> _userRepository;
        private readonly IZnodeRepository<ZnodeAccount> _accountRepository;
        private IPublishedProductDataService publishedProductData;

        private readonly IZnodeRepository<ZnodeState> _stateRepository;
        private readonly IPublishProductHelper publishProductHelper;
        private readonly IZnodeOrderHelper orderHelper;
        public static string SKU { get; } = "sku";
        public static string Width { get; } = "width";
        public static string Height { get; } = "height";
        private readonly IShoppingCartMap _shoppingCartMap;
        private readonly IShoppingCartItemMap _shoppingCartItemMap;
        public readonly string[] _upsLTLCode = { "308", "309", "310" };
        #endregion
        string PortalIDDealer = ConfigurationManager.AppSettings["PortalIDDealer"];
        #region Constructor
        public DSShoppingCartService()
        {
            _cookieMappingRepository = new ZnodeRepository<ZnodeOmsCookieMapping>();
            _omsSavedRepository = new ZnodeRepository<ZnodeOmsSavedCart>();
            _savedCartLineItemService = new ZnodeRepository<ZnodeOmsSavedCartLineItem>();
            _orderDetailRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
            _lineItemRelationshipType = new ZnodeRepository<ZnodeOmsOrderLineItemRelationshipType>();
            _stateRepository = new ZnodeRepository<ZnodeState>();
            publishProductHelper = GetService<IPublishProductHelper>();
            orderHelper = GetService<IZnodeOrderHelper>();
            _orderLineItemRepository = new ZnodeRepository<ZnodeOmsOrderLineItem>();
            _shoppingCartMap = GetService<IShoppingCartMap>();
            _shoppingCartItemMap = GetService<IShoppingCartItemMap>();
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            _userRepository = new ZnodeRepository<ZnodeUser>();
            _accountRepository = new ZnodeRepository<ZnodeAccount>();
            publishedProductData = GetService<IPublishedProductDataService>();

        }
        #endregion


        public override ShippingListModel GetShippingEstimates(string zipCode, ShoppingCartModel model)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Base - GetShippingEstimates", "Custom", TraceLevel.Info);
            if (HelperUtility.IsNull(model))
                //throw new ZnodeException(ErrorCodes.NullModel, Admin_Resources.ErrorShoppingCartModelNull);

                if (string.IsNullOrEmpty(zipCode))
                    return null;

            List<ShippingModel> listwithRates = new List<ShippingModel>();
            try
            {
                List<ShippingModel> list = GetShippingList(model);

                ZnodeLogging.LogMessage("Shipping list:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, list?.Count);
                if (list?.Count > 0)
                {
                    if (Equals(model.ShippingAddress, null))
                    {
                        IZnodeRepository<ZnodeAddress> _addressRepository = new ZnodeRepository<ZnodeAddress>();
                        IZnodeRepository<ZnodeUserAddress> _addressUserRepository = new ZnodeRepository<ZnodeUserAddress>();
                        var shippingAddress = (from p in _addressRepository.Table
                                               join q in _addressUserRepository.Table
                                               on p.AddressId equals q.AddressId
                                               where (q.UserId == model.UserId) && (p.IsDefaultShipping)
                                               select new AddressModel
                                               {
                                                   StateName = p.StateName,
                                                   CountryName = p.CountryName,
                                                   PostalCode = p.PostalCode
                                               }).FirstOrDefault();
                        model.ShippingAddress = (shippingAddress != null) ? shippingAddress : new AddressModel();
                    }
                    string countryCode = model?.ShippingAddress?.CountryName;
                    string stateCode = GetStateCode(model?.ShippingAddress?.StateName);
                    ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { countryCode = countryCode, stateCode = stateCode });

                    if (!string.IsNullOrEmpty(countryCode) && !string.IsNullOrEmpty(stateCode))
                        list = GetShippingByCountryAndStateCode(countryCode, stateCode, list);

                    if (!string.IsNullOrEmpty(model.ShippingAddress?.PostalCode))
                        list = GetShippingByZipCode(model.ShippingAddress.PostalCode, list);

                    //check shipping type and call that service to get the rates. Add the rates in the list.
                    if (Equals(model.ShippingAddress, null))
                        model.ShippingAddress = new AddressModel();

                    /*NIVI CODE for Alaska,Hawaii & Puerto Rico State*/
                    if (model.PortalId != 16)
                    {
                        if (stateCode == "AK" || stateCode == "HI" || stateCode == "PR")
                        {
                            // list.RemoveRange(list.Select(x => x.ShippingName != "USPS").ToList());	                       
                            //list?.RemoveAll(r => (r.ShippingTypeName?.ToLower() != "Custom" && r.ShippingCode != "AK"));
                            list?.RemoveAll(r => (r.StateCode != stateCode));
                            ZnodeLogging.LogMessage("stateCode=" + stateCode + "ShippingListCount=" + Convert.ToString(list.Count), "DSShoppingCartService", TraceLevel.Info);
                        }
                    }
                    // Nivi Code End
                    model.ShippingAddress.PostalCode = zipCode;
                    model.ShippingAddress.StateCode = stateCode;
                    model.ShippingAddress.CountryName = countryCode;
                    model.ShippingAddress.Address1 = string.IsNullOrEmpty(model.ShippingAddress.Address1) ? string.Empty : model.ShippingAddress.Address1;
                    model.ShippingAddress.CityName = string.IsNullOrEmpty(model.ShippingAddress.CityName) ? string.Empty : model.ShippingAddress.CityName;


                    ShoppingCartMap SCM = new ShoppingCartMap();
                    ZnodeShoppingCart znodeShoppingCart = SCM.ToZnodeShoppingCart(model);



                    //ZnodeShoppingCart znodeShoppingCart = _shoppingCartMap.ToZnodeShoppingCart(model);
                    List<ShippingModel> upslist = list.Where(w => w.ShippingTypeName.ToLower() == ZnodeConstant.UPS.ToLower() /*|| w.ShippingTypeName.ToLower() == ZnodeConstant.FedEx.ToLower()*/).ToList();
                    list?.RemoveAll(r => (r.ShippingTypeName?.ToLower() == ZnodeConstant.UPS.ToLower() /*|| r.ShippingTypeName?.ToLower() == ZnodeConstant.FedEx.ToLower() */) && !_upsLTLCode.Contains(r.ShippingCode));
                    bool isCalculatePromotionForShippingEstimates = ZnodeWebstoreSettings.IsCalculatePromotionForShippingEstimate;

                    //Call the respective shipping classes to get the shipping rates.
                    foreach (ShippingModel item in list)
                    {
                        model.Shipping.ShippingId = item.ShippingId;
                        model.Shipping.ShippingName = item.ShippingCode;
                        model.Shipping.ShippingCountryCode = string.IsNullOrEmpty(countryCode) ? item.DestinationCountryCode : countryCode;

                        znodeShoppingCart.Shipping = ShippingMap.ToZnodeShipping(model.Shipping);
                        ZnodeShippingManager shippingManager = new ZnodeShippingManager(znodeShoppingCart);
                        shippingManager.Calculate();

                        // Calculate shipping type promotion if isCalculatePromotionForShippingEstimates is true.
                        if (isCalculatePromotionForShippingEstimates && znodeShoppingCart.IsCalculatePromotionAndCoupon)
                            CalculatePromotionForShippingEstimate(znodeShoppingCart);

                        item.ShippingRate = znodeShoppingCart.ShippingCost;
                        if (item.ShippingRate >= 0 && znodeShoppingCart?.Shipping?.ShippingDiscount > 0)
                            item.ShippingRateWithoutDiscount = znodeShoppingCart.ShippingCost + znodeShoppingCart.Shipping.ShippingDiscount;

                        item.ApproximateArrival = znodeShoppingCart.ApproximateArrival;
                        if (Equals(znodeShoppingCart?.Shipping?.ResponseCode, "0"))
                            listwithRates.Add(item);
                    }

                    //UPS shipping type is exclude from above loop execution to avoid ups api call in loop.
                    //Below ups execution consolidated calculated shipping rates and 'EstimateDate' of ups shipping type on page load.
                    //It also removes not applicable ups shipping types which above loop shows with high shipping values.
                    //As below execution call ups api, it also calculate shipping value for ups and also discounted shipping value .
                    //Need to implemented same for FedEx and USPS shipping types for which API are already present.
                    if (upslist.Count > 0)
                    {
                        ZnodeShippingManager manager = null;
                        ZnodeGenericCollection<IZnodeShippingsType> shippingTypes = new ZnodeGenericCollection<IZnodeShippingsType>();
                        List<ZnodeShippingBag> shippingbagList = new List<ZnodeShippingBag>();
                        foreach (ShippingModel item in upslist)
                        {
                            model.Shipping.ShippingId = item.ShippingId;
                            model.Shipping.ShippingName = item.ShippingCode;
                            model.Shipping.ShippingCountryCode = string.IsNullOrEmpty(countryCode) ? item.DestinationCountryCode : countryCode;
                            znodeShoppingCart.Shipping = ShippingMap.ToZnodeShipping(model.Shipping);
                            manager = new ZnodeShippingManager(znodeShoppingCart, true, shippingTypes, shippingbagList);
                        }

                        List<ShippingModel> ratelist = manager.GetShippingEstimateRate(znodeShoppingCart, model, countryCode, shippingbagList);
                        ZnodeLogging.LogMessage("Rate list:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, ratelist?.Count());
                        upslist = upslist.Join(ratelist, r => r.ShippingCode, p => p.ShippingCode, (ulist, rlist) => ulist).ToList();

                        upslist?.ForEach(f =>
                        {
                            ShippingModel shippingModel = ratelist?.Where(w => w.ShippingCode == f.ShippingCode && !Equals(w.ShippingRate, 0))?.FirstOrDefault();

                            if (HelperUtility.IsNotNull(shippingModel) && shippingModel?.ShippingRate >= 0)
                            {
                                f.EstimateDate = shippingModel.EstimateDate;
                                f.ShippingRate = znodeShoppingCart.CustomShippingCost ?? shippingModel.ShippingRate;
                                f.ShippingRateWithoutDiscount = shippingModel?.ShippingRateWithoutDiscount > 0 ? shippingModel?.ShippingRateWithoutDiscount : 0;
                            }

                        });

                        listwithRates.AddRange(upslist);
                    }
                    return new ShippingListModel { ShippingList = listwithRates.OrderBy(o => o.DisplayOrder).ToList() };
                }
                else
                    return new ShippingListModel { ShippingList = new List<ShippingModel>() };
            }
            catch (Exception ex)
            {
                //ZnodeLogging.LogMessage(Admin_Resources.ErrorShippingOptionGet, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error, ex);
                return new ShippingListModel { ShippingList = listwithRates?.Count() > 0 ? listwithRates.OrderBy(o => o.DisplayOrder).ToList() : new List<ShippingModel>() };
            }
        }

        // Calculate shipping type promotion if isCalculatePromotionForShippingEstimates is true.
        private void CalculatePromotionForShippingEstimate(ZnodeShoppingCart znodeShoppingCart)
        {
            ZnodeLogging.LogMessage("Execution Started:", ZnodeLogging.Components.Shipping.ToString(), TraceLevel.Info);
            ZnodeCartPromotionManager cartPromoManager = new ZnodeCartPromotionManager(znodeShoppingCart, znodeShoppingCart.ProfileId);

            if (cartPromoManager.CartPromotionCache.Any(x => x.PromotionType.ClassName == ZnodeConstant.PercentOffShipping || x.PromotionType.ClassName == ZnodeConstant.PercentOffShippingWithCarrier || x.PromotionType.ClassName == ZnodeConstant.AmountOffShipping || x.PromotionType.ClassName == ZnodeConstant.AmountOffShippingWithCarrier))
                cartPromoManager.Calculate();
            ZnodeLogging.LogMessage("Execution Ended:", ZnodeLogging.Components.Shipping.ToString(), TraceLevel.Info);

        }

        //if any item from cart is out of stock so it return total no of item which is "out stock item".
        public override int IsItemOutOfStock(ShoppingCartItemModel shoppingCartItem, List<string> skus, decimal selectedQuantity, List<InventorySKUModel> inventoryList, int insufficientQuantity, PublishedProductEntityModel products)
        {
            try
            {
                List<PublishedSelectValuesEntityModel> inventorySettingList = null;



                ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                selectedQuantity = shoppingCartItem.Quantity;

                //Get inventory setting for product.
                //List<PublishedSelectValuesEntityModel> inventorySettingList = products.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.OutOfStockOptions)?.SelectValues;
                //Nivi code changes for inventory
                var inventorysku = inventoryList.FirstOrDefault().SKU;
                ZnodeInventory Inventory = new ZnodeRepository<ZnodeInventory>().Table.Where(x => x.SKU == inventorysku).FirstOrDefault();
                inventoryList.FirstOrDefault().BackOrderExpectedDate = Inventory.BackOrderExpectedDate;
                if (inventoryList.FirstOrDefault().PortalId != Convert.ToInt32(PortalIDDealer))
                {
                    inventorySettingList = products.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.OutOfStockOptions)?.SelectValues;
                    if (inventorySettingList.FirstOrDefault().Code == "AllowBackOrdering")
                    {
                        inventorySettingList.FirstOrDefault().Code = "DisablePurchasing";
                    }
                }
                else
                {
                    inventorySettingList = products.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.OutOfStockOptions)?.SelectValues;
                    var todaysDate = DateTime.Today;
                    if (inventoryList.FirstOrDefault().BackOrderExpectedDate != null && todaysDate <= inventoryList.FirstOrDefault().BackOrderExpectedDate)
                    {
                        inventorySettingList.FirstOrDefault().Code = "AllowBackOrdering";
                    }

                }

                string minimumQuantity = products.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.MinimumQuantity)?.AttributeValues;
                string maximumQuantity = products.Attributes?.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.MaximumQuantity)?.AttributeValues;
                List<string> sku = skus.Where(m => m == products.SKU).ToList();

                string inventorySetting = inventorySettingList?.Count > 0 ? inventorySettingList.FirstOrDefault().Code : ZnodeConstant.DontTrackInventory;
                ZnodeLogging.LogMessage("Parameter :", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { minimumQuantity = minimumQuantity, maximumQuantity = maximumQuantity, InventorySetting = inventorySetting });

                switch (inventorySetting)
                {
                    case ZnodeConstant.DisablePurchasing:
                        shoppingCartItem.InsufficientQuantity = IsInsufficientQuantity(sku, selectedQuantity, inventoryList);
                        if (shoppingCartItem.InsufficientQuantity)
                            insufficientQuantity++;
                        break;

                    case ZnodeConstant.AllowBackOrdering:
                        shoppingCartItem.InsufficientQuantity = false;
                        //Nivi Changes for back Ordering
                        //shoppingCartItem.InsufficientQuantity = IsInsufficientQuantityCheck(sku, selectedQuantity, inventoryList);
                        //if (shoppingCartItem.InsufficientQuantity)
                        //    insufficientQuantity++;
                        break;

                    case ZnodeConstant.DontTrackInventory:
                        shoppingCartItem.InsufficientQuantity = false;
                        break;

                    default:
                        //Between true if want to include min and max number in comparison.
                        shoppingCartItem.InsufficientQuantity = string.IsNullOrEmpty(minimumQuantity) ? false : !HelperUtility.Between(Convert.ToDecimal(shoppingCartItem.Quantity), Convert.ToDecimal(minimumQuantity), Convert.ToDecimal(maximumQuantity), true);
                        if (shoppingCartItem.InsufficientQuantity)
                            insufficientQuantity++;
                        break;
                }
                ZnodeLogging.LogMessage("Insufficient Quantity:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, insufficientQuantity);
                ZnodeLogging.LogMessage("Execution Done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);

                return insufficientQuantity;
            }
            catch (Exception)
            {
                if (IsNotNull(shoppingCartItem.OmsSavedcartLineItemId))
                {
                    base.RemoveSavedCartLineItem(Convert.ToInt32(shoppingCartItem.OmsSavedcartLineItemId));
                }
                return insufficientQuantity;

            }

        }


        //Check item has Insufficient Quantity or not.
        protected bool IsInsufficientQuantityCheck(List<string> skus, decimal selectedQuantity, List<InventorySKUModel> inventoryList)
        {
            if (HelperUtility.IsNotNull(skus) && HelperUtility.IsNotNull(inventoryList))
            {
                foreach (InventorySKUModel inventoryItem in inventoryList)
                {
                    var InventoryQuantity = inventoryItem.Quantity + inventoryItem.BackOrderQuantity;
                    foreach (var item in skus)
                    {
                        if (string.Equals(item, inventoryItem.SKU, StringComparison.OrdinalIgnoreCase) && InventoryQuantity < selectedQuantity)
                            return true;
                    }
                }
                return false;
            }
            return true;
        }

    }
}