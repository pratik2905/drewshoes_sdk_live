﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;


namespace Znode.Api.Custom.Service.Service
{
    public class DSPublishProductService : PublishProductService, IDSPublishProductService
    {      
        public DSPublishProductService() : base()
        {
          
        }


        #region Base Methods Override 9.6.2.1

        public override PublishProductModel GetPublishProduct(int publishProductId, FilterCollection filters, NameValueCollection expands)
        {
            ZnodeLogging.LogMessage("Input Parameters publishProductId:", string.Empty, TraceLevel.Info, publishProductId);

            bool isChildPersonalizableAttribute = false;
            List<PublishAttributeModel> parentPersonalizableAttributes = null;
            int portalId, localeId;
            int? catalogVersionId;
            PublishProductModel publishProduct = null;
            List<PublishProductModel> products;
            string parentProductImageName = null;

            //Get publish product 
            products = GetPublishProductFromPublishedData(publishProductId, filters, out portalId, out localeId, out catalogVersionId, publishProduct, out products);

            List<int> associatedCategoryIds = new List<int>();

            if (HelperUtility.IsNotNull(products) && products.Count > 0)
            {
                List<int> categoryIds = products.Select(x => x.ZnodeCategoryIds)?.ToList();

                FilterCollection filter = new FilterCollection();
                if (categoryIds?.Count > 0)
                    filter.Add("ZnodeCategoryId", FilterOperators.In, string.Join(",", categoryIds));
                filter.Add("IsActive", FilterOperators.Equals, ZnodeConstant.TrueValue);
                filter.Add("VersionId", FilterOperators.Equals, catalogVersionId.ToString());

                List<PublishedCategoryEntityModel> categoryEntities = ZnodeDependencyResolver.GetService<IPublishedCategoryDataService>().GetPublishedCategoryList(new PageListModel(filter, null, null))?.ToModel<PublishedCategoryEntityModel>()?.ToList();

                associatedCategoryIds.AddRange(categoryEntities.Select(x => x.ZnodeCategoryId));

                //If no category associated to product then perform else part
                if (associatedCategoryIds?.Count > 0)
                    publishProduct = products.FirstOrDefault(x => associatedCategoryIds.Contains(x.ZnodeCategoryIds));
                else
                    publishProduct = products?.FirstOrDefault();

                parentProductImageName = publishProduct?.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues;
                // parentProductImageName = publishProduct?.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductImage)?.DisplayOrder.ToString();
            }

            if (HelperUtility.IsNotNull(publishProduct))
            {
                PublishedConfigurableProductEntityModel configEntiy = GetConfigurableProductEntity(publishProductId, catalogVersionId);

                if (HelperUtility.IsNotNull(configEntiy))
                {
                    //Selecting child SKU

                    IEnumerable<string> childSKU = publishProduct.Attributes.Where(x => x.IsConfigurable).SelectMany(x => x.SelectValues.OrderBy(z => z.VariantDisplayOrder).Select(y => y.VariantSKU)).Distinct();


                    //Get Associated product by child SKUs
                    List<PublishProductModel> associatedProducts = GetAssociatedConfigurableProducts(localeId, catalogVersionId, childSKU);

                    //Nivi Code start for Selecting parent SKU
                    IEnumerable<string> parentSKU = publishProduct.Attributes.Where(x => x.AttributeCode == "SKU").Select(y => y.AttributeValues);

                    //Get Associated product by parent SKUs
                    List<PublishProductModel> parentassociatedProducts = GetAssociatedConfigurableProducts(localeId, catalogVersionId, parentSKU);
                    List<PublishAttributeModel> ConfigData = new List<PublishAttributeModel>();

                    ConfigData.AddRange(parentassociatedProducts?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "LongDescription").ToList());
                    ConfigData.AddRange(parentassociatedProducts?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "ShortDescription").ToList());
                    //ConfigData.AddRange(parentassociatedProducts?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "fallundercategory").ToList());
                    ConfigData.AddRange(parentassociatedProducts?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "ProductSpecification").ToList());
                    ConfigData.AddRange(parentassociatedProducts?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "Highlights").ToList());
                    //ConfigData.AddRange(parentassociatedProducts?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "videourl360").ToList());
                    ConfigData.AddRange(parentassociatedProducts?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "videourllifestyle").ToList());

                    /*removing existing data from list*/
                    publishProduct.Attributes.RemoveAll(x => ConfigData.Select(y => y.AttributeCode).Contains(x.AttributeCode));

                    parentassociatedProducts.FirstOrDefault().Attributes.RemoveAll(x => ConfigData.Select(y => y.AttributeCode).Contains(x.AttributeCode));

                    /*inserting Config product data to simple products*/
                    associatedProducts.ForEach(x => x.Attributes.AddRange(ConfigData?.ToList()));
                    //Nivi Code end for Selecting parent SKU


                    parentPersonalizableAttributes = publishProduct.Attributes?.Where(x => x.IsPersonalizable).ToList();

                    publishProduct = GetDefaultConfigurableProduct(expands, portalId, localeId, publishProduct, associatedProducts, configEntiy.ConfigurableAttributeCodes, catalogVersionId.GetValueOrDefault());
                }
                else
                    //Get expands associated to Product
                    publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId.GetValueOrDefault(), WebstoreVersionId, GetProfileId());

                isChildPersonalizableAttribute = publishProduct.Attributes.Where(x => x.IsPersonalizable).Count() > 0;

                GetProductImagePath(portalId, publishProduct, true, parentProductImageName);

                //set stored based In Stock, Out Of Stock, Back Order Message.
                SetPortalBasedDetails(portalId, publishProduct);

                publishProduct.ZnodeProductCategoryIds = associatedCategoryIds;
            }
            return AddPersonalizeAttributeInChildProduct(publishProduct, parentPersonalizableAttributes, isChildPersonalizableAttribute);
        }

        private List<PublishProductModel> GetPublishProductFromPublishedData(int publishProductId, FilterCollection filters, out int portalId, out int localeId, out int? catalogVersionId, PublishProductModel publishProduct, out List<PublishProductModel> products)
        {
            //Get parameter values from filters.
            int catalogId;
            GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

            //Remove portal id filter.
            filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);

            //Replace filter keys.
            ReplaceFilterKeys(ref filters);

            //get catalog current version id by catalog id.
            catalogVersionId = GetCatalogVersionId(catalogId);

            filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, Convert.ToString(publishProductId));

            if (catalogVersionId > 0)
                filters.Add(FilterKeys.VersionId, FilterOperators.Equals, catalogVersionId.HasValue ? catalogVersionId.Value.ToString() : "0");

            publishProduct = null;

            //Get publish product
            products = ZnodeDependencyResolver.GetService<IPublishedProductDataService>().GetPublishProducts(new PageListModel(filters, null, null)).ToModel<PublishProductModel>().ToList();

            return products;
        }

        private PublishProductModel AddPersonalizeAttributeInChildProduct(PublishProductModel publishProduct, List<PublishAttributeModel> parentPersonalizableAttributes, bool isChildPersonalizableAttribute)
        {
            if (!isChildPersonalizableAttribute)
            {
                if (parentPersonalizableAttributes?.Count > 0)
                    publishProduct.Attributes.AddRange(parentPersonalizableAttributes);
            }
            return publishProduct;
        }

        //Get Configurable Product

        public override PublishProductModel GetConfigurableProduct(ParameterProductModel productAttributes, NameValueCollection expands)
        {
            PublishProductModel product = null;
            //new implementaion
            int? versionId = GetCatalogVersionId(productAttributes.PublishCatalogId);

            FilterCollection filters = new FilterCollection();
            filters.Add("ZnodeProductId", FilterOperators.Equals, productAttributes.ParentProductId.ToString());
            filters.Add("IsActive", FilterOperators.Equals, ZnodeConstant.TrueValue);
            filters.Add("VersionId", FilterOperators.Equals, versionId.ToString());

            PublishProductModel parentProduct = ZnodeDependencyResolver.GetService<IPublishedProductDataService>().GetPublishProductByFilters(filters)?.ToModel<PublishProductModel>();

            //Selecting child SKU
            IEnumerable<string> childSKU = parentProduct.Attributes.Where(x => x.IsConfigurable).SelectMany(x => x.SelectValues.OrderBy(z => z.VariantDisplayOrder).Select(y => y.VariantSKU)).Distinct();

            //Creating new query
            List<PublishProductModel> productList = GetAssociatedConfigurableProducts(productAttributes.LocaleId, versionId, childSKU);

            if (productAttributes.PortalId == 8)
            {
                int count = 1;
                foreach (var item in productAttributes.SelectedAttributes)
                {
                    if ((productAttributes.SelectedAttributes).Count > 3)
                    {
                        if (count == 4)
                        {
                            break;
                        }
                        else
                        {
                            productList = (from productentity in productList
                                           from attribute in productentity.Attributes
                                           where attribute.AttributeCode == item.Key && attribute.SelectValues.FirstOrDefault()?.Value == item.Value
                                           select productentity).ToList();
                        }
                        count++;
                    }
                    else
                    {
                        productList = (from productentity in productList
                                       from attribute in productentity.Attributes
                                       where attribute.AttributeCode == item.Key && attribute.SelectValues.FirstOrDefault()?.Value == item.Value
                                       select productentity).ToList();
                    }
                }
            }
            else
            {
                foreach (var item in productAttributes.SelectedAttributes)
                {
                    productList = (from productentity in productList
                                   from attribute in productentity.Attributes
                                   where attribute.AttributeCode == item.Key && attribute.SelectValues.FirstOrDefault()?.Value == item.Value
                                   select productentity).ToList();
                }
            }


            PublishProductModel productEntity = productList.FirstOrDefault();

            //Nivi Code Start//

            IEnumerable<string> parentSKU = parentProduct.Attributes.Where(x => x.AttributeCode == "SKU").Select(y => y.AttributeValues);

            List<PublishProductModel> newproductList = GetAssociatedConfigurableProducts(productAttributes.LocaleId, versionId, parentSKU);


            List<PublishAttributeModel> ConfigData = new List<PublishAttributeModel>();

            ConfigData.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "LongDescription").ToList());
            ConfigData.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "ShortDescription").ToList());
            //ConfigData.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "fallundercategory").ToList());
            ConfigData.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "ProductSpecification").ToList());
            ConfigData.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "Highlights").ToList());
            //ConfigData.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "videourl360").ToList());
            ConfigData.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "videourllifestyle").ToList());

            //Nivi code End//

            //If Combination does not exist.
            if (HelperUtility.IsNull(productEntity))
            {
                //Creating new query
                //List<PublishProductModel> newproductList = GetAssociatedConfigurableProducts(productAttributes.LocaleId, versionId, childSKU);
                //product = newproductList?.FirstOrDefault();
                //if (HelperUtility.IsNotNull(product)) { product.IsDefaultConfigurableProduct = true; }

                //Nivi Code Start//
                List<PublishProductModel> ChildnewproductList = GetAssociatedConfigurableProducts(productAttributes.LocaleId, versionId, childSKU);
                ChildnewproductList.FirstOrDefault().Attributes.RemoveAll(x => ConfigData.Select(y => y.AttributeCode).Contains(x.AttributeCode));
                ChildnewproductList.ForEach(x => x.Attributes.AddRange(ConfigData?.ToList()));
                product = ChildnewproductList?.FirstOrDefault();
                //product = ChildnewproductList?.FirstOrDefault().Attributes.Where(x=>x.SelectedAttributeValue==);
                if (HelperUtility.IsNotNull(product)) { product.IsDefaultConfigurableProduct = true; }

                //Nivi code end//
            }
            else
            {
                /* Nivi Code:start */

                productEntity.Attributes.RemoveAll(x => ConfigData.Select(y => y.AttributeCode).Contains(x.AttributeCode));
                //productEntity.Attributes.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "LongDescription").ToList());

                productEntity.Attributes.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "LongDescription").ToList());
                productEntity.Attributes.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "ShortDescription").ToList());
                //productEntity.Attributes.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "fallundercategory").ToList());
                productEntity.Attributes.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "ProductSpecification").ToList());
                productEntity.Attributes.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "Highlights").ToList());
                //productEntity.Attributes.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "videourl360").ToList());
                productEntity.Attributes.AddRange(newproductList?.FirstOrDefault()?.Attributes.Where(x => x.AttributeCode == "videourllifestyle").ToList());

                //newproductList.FirstOrDefault().Attributes.RemoveAll(x => ConfigData.Select(y => y.AttributeCode).Contains(x.AttributeCode));

                /*Nivi Code:End*/

                product = productEntity;
            }

            if (HelperUtility.IsNotNull(product))
            {
                bool isChildPersonalizableAttribute = product.Attributes.Where(x => x.IsPersonalizable).Count() > 0;

                var parentPersonalizableAttributes = parentProduct.Attributes?.Where(x => x.IsPersonalizable);
                product.AssociatedGroupProducts = MapParametersForProduct(productList);

                product.ConfigurableProductId = productAttributes.ParentProductId;
                product.IsConfigurableProduct = true;

                product.ProductType = ZnodeConstant.ConfigurableProduct;
                product.ConfigurableProductSKU = product.SKU;
                product.SKU = productAttributes.ParentProductSKU;

                publishProductHelper.GetDataFromExpands(productAttributes.PortalId, GetExpands(expands), product, productAttributes.LocaleId, string.Empty, GetLoginUserId(), null, null, GetProfileId());

                GetProductImagePath(productAttributes.PortalId, product);

                //set stored based In Stock, Out Of Stock, Back Order Message.
                SetPortalBasedDetails(productAttributes.PortalId, product);

                if (!isChildPersonalizableAttribute && parentPersonalizableAttributes?.Count() > 0)
                    product.Attributes.AddRange(parentPersonalizableAttributes);
            }
            return product;

        }

        #endregion

        public DataTable GetStoreLocations(string state, string sku)
        {
            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
            objStoredProc.GetParameter("@State", state, ParameterDirection.Input, SqlDbType.VarChar);
            objStoredProc.GetParameter("@SKU", sku, ParameterDirection.Input, SqlDbType.VarChar);

            DataSet data = objStoredProc.GetSPResultInDataSet("Nivi_DS_GetStoreAddressDetails");

            DataTable storeLocationTable = data.Tables[0];

            return storeLocationTable;
        }
        public DataTable GetStoreLocationDetails(string state, string sku)
        {
            DataTable storeData = this.GetStoreLocations(state, sku);
            return storeData;
        }

    }
}
