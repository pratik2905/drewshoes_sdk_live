﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;


namespace Znode.Api.Custom.Service.Service
{
    public class DSWebStoreCaseRequestService : WebStoreCaseRequestService
    {
        #region Private Variables
        private readonly IZnodeRepository<ZnodeCaseRequest> _caseRequestRepository;
        private readonly IZnodeRepository<ZnodeCaseType> _caseTypeRepository;
        private readonly IZnodeRepository<ZnodeCasePriority> _casePriorityRepository;
        private readonly IZnodeRepository<ZnodeCaseStatu> _caseStatusRepository;
        private readonly IZnodeRepository<ZnodePortal> _portalRepository;
        private readonly IZnodeRepository<ZnodeCaseRequestHistory> _caseRequestHistoryRepository;
        #endregion

        #region Constructor
        public DSWebStoreCaseRequestService()
        {
            _caseRequestRepository = new ZnodeRepository<ZnodeCaseRequest>();
            _caseTypeRepository = new ZnodeRepository<ZnodeCaseType>();
            _casePriorityRepository = new ZnodeRepository<ZnodeCasePriority>();
            _caseStatusRepository = new ZnodeRepository<ZnodeCaseStatu>();
            _portalRepository = new ZnodeRepository<ZnodePortal>();
            _caseRequestHistoryRepository = new ZnodeRepository<ZnodeCaseRequestHistory>();
        }
        #endregion


        public override WebStoreCaseRequestModel CreateContactUs(WebStoreCaseRequestModel caseRequestModel)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);

            if (IsNull(caseRequestModel))
                throw new ZnodeException(ErrorCodes.NullModel, "ErrorCaseRequestModelNull");

            //Create new CaseRequest and return it.
            ZnodeCaseRequest caseRequest = _caseRequestRepository.Insert(caseRequestModel.ToEntity<ZnodeCaseRequest>());//insert log
            if (Equals(caseRequestModel.CaseOrigin, ZnodeConstant.ContactUsForm))
                SendContactUsEmail(GetStoreName(caseRequestModel), caseRequestModel.FirstName, caseRequestModel.LastName, caseRequestModel.CompanyName, caseRequestModel.PhoneNumber, caseRequestModel.Description, caseRequestModel.EmailId, caseRequestModel.PortalId, caseRequestModel.LocaleId);
            else if (Equals(caseRequestModel.CaseOrigin, ZnodeConstant.CustomerFeedbackForm))
            {
                //Split message data.
                string[] message = caseRequestModel.Description.Split(new String[] { ":", "<br/>" }, StringSplitOptions.None);

                SendCustomerFeedbackEmail(GetStoreName(caseRequestModel), message, caseRequestModel.FirstName, caseRequestModel.LastName, caseRequestModel.EmailId, caseRequestModel.PortalId, caseRequestModel.LocaleId);
            }

            if (caseRequest?.CaseRequestId > 0)
                return caseRequest.ToModel<WebStoreCaseRequestModel>();

            ZnodeLogging.LogMessage("CaseRequestId, PortalId and LocaleId properties of caseRequestModel: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new { CaseRequestId = caseRequestModel?.CaseRequestId, PortalId = caseRequestModel?.PortalId, LocaleId = caseRequestModel?.LocaleId });
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Info);
            return caseRequestModel;
        }
        public static bool IsNull(object value)
           => Equals(value, null);
        private bool SendContactUsEmail(string storeName, string firstName, string lastName, string companyName, string phoneNumber, string comments, string email, int portalId, int localeId)
        {
            ZnodeLogging.LogMessage("Input parameters storeName, firstName, lastName, companyName, phoneNumber, comments, email, portalId, localeId: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new object[] { storeName, firstName, lastName, companyName, phoneNumber, comments, email, portalId, localeId });

            //Get Email template by passing code as 'ContactUs'
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode(ZnodeConstant.ContactUs, (portalId > 0) ? portalId : PortalId, localeId);
            ZnodeLogging.LogMessage("EmailTemplateMapperId and EmailTemplateId of emailTemplateMapperModel returned from method GetEmailTemplateByCode", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new { EmailTemplateMapperId = emailTemplateMapperModel?.EmailTemplateMapperId, EmailTemplateId = emailTemplateMapperModel?.EmailTemplateId });

            if (IsNull(emailTemplateMapperModel))
                return false;

            if (IsNull(storeName))
                storeName = ZnodeConfigManager.SiteConfig.StoreName;

            string messageText = emailTemplateMapperModel.Descriptions;
            messageText = ReplaceTokenWithMessageText("#FirstName#", firstName, messageText);
            messageText = ReplaceTokenWithMessageText("#LastName#", lastName, messageText);
            messageText = ReplaceTokenWithMessageText("#CompanyName#", IsNull(companyName) ? string.Empty : companyName, messageText);
            messageText = ReplaceTokenWithMessageText("#PhoneNumber#", IsNull(phoneNumber) ? string.Empty : phoneNumber, messageText);
            messageText = ReplaceTokenWithMessageText("#Comments#", IsNull(comments) ? string.Empty : comments, messageText);
            //nivi code for add email//
            messageText = ReplaceTokenWithMessageText("#Email#", IsNull(comments) ? string.Empty : email, messageText);

            messageText = HelperUtility.ReplaceTokenWithMessageText("#StoreLogo#", GetCustomPortalDetails((portalId > 0) ? portalId : PortalId)?.StoreLogo, messageText);

            //These method is used to set null to unwanted keys.
            messageText = EmailTemplateHelper.ReplaceTemplateTokens(messageText);
            ZnodeLogging.LogMessage("messageText generated by EmailTemplateHelper.ReplaceTemplateTokens", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, messageText);
            try
            {
                ZnodeEmail.SendEmail(ZnodeConfigManager.SiteConfig.AdminEmail, email, ZnodeEmail.GetBccEmail(emailTemplateMapperModel.IsEnableBcc, portalId, string.Empty), emailTemplateMapperModel.Subject, messageText, true, null);
                return true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Error);
                ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginCreateSuccess, firstName, string.Empty, null, ex.Message, null);
                return false;
            }
        }
        private string GetStoreName(WebStoreCaseRequestModel webStoreCaseRequestModel)
      => _portalRepository.GetById(webStoreCaseRequestModel.PortalId)?.StoreName;
        private bool SendCustomerFeedbackEmail(string storeName, string[] message, string firstName, string lastName, string email, int portalId, int localeId)
        {
            ZnodeLogging.LogMessage("Input parameters storeName, message, firstName, lastName, email, portalId, localeId: ", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new object[] { storeName, message, firstName, lastName, email, portalId, localeId });

            //Get Email template by passing code as 'ContactUs'
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode(ZnodeConstant.CustomerFeedbackNotification, (portalId > 0) ? portalId : PortalId, localeId);
            ZnodeLogging.LogMessage("EmailTemplateMapperId and EmailTemplateId of emailTemplateMapperModel returned from method GetEmailTemplateByCode", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, new { EmailTemplateMapperId = emailTemplateMapperModel?.EmailTemplateMapperId, EmailTemplateId = emailTemplateMapperModel?.EmailTemplateId });

            if (IsNull(emailTemplateMapperModel))
                return false;

            if (IsNull(storeName))
                storeName = ZnodeConfigManager.SiteConfig.StoreName;

            string messageText = emailTemplateMapperModel.Descriptions;
            messageText = ReplaceTokenWithMessageText("#Message#", message[1], messageText);
            messageText = ReplaceTokenWithMessageText("#FirstName#", IsNull(firstName) ? string.Empty : firstName, messageText);
            messageText = ReplaceTokenWithMessageText("#LastName#", IsNull(lastName) ? string.Empty : lastName, messageText);
            messageText = ReplaceTokenWithMessageText("#City#", message[3], messageText);
            messageText = ReplaceTokenWithMessageText("#State#", message[5], messageText);
            messageText = ReplaceTokenWithMessageText("#EmailAddress#", email, messageText);
            messageText = ReplaceTokenWithMessageText("#ShareFeedback#", message[6], messageText);
            messageText = HelperUtility.ReplaceTokenWithMessageText("#StoreLogo#", GetCustomPortalDetails((portalId > 0) ? portalId : PortalId)?.StoreLogo, messageText);

            //These method is used to set null to unwanted keys.
            messageText = EmailTemplateHelper.ReplaceTemplateTokens(messageText);
            ZnodeLogging.LogMessage("messageText generated by EmailTemplateHelper.ReplaceTemplateTokens", ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Verbose, messageText);

            try
            {
                ZnodeEmail.SendEmail(ZnodeConfigManager.SiteConfig.AdminEmail, email, ZnodeEmail.GetBccEmail(emailTemplateMapperModel.IsEnableBcc, portalId, string.Empty), emailTemplateMapperModel.Subject, messageText, true, null);
                return true;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Webstore.ToString(), TraceLevel.Error);
                ZnodeLogging.LogActivity((int)ZnodeLogging.ErrorNum.LoginCreateSuccess, firstName, string.Empty, null, ex.Message, null);
                return false;
            }
        }
        public static string ReplaceTokenWithMessageText(string key, string replaceValue, string resourceText)
        {
            Regex rgx = new Regex(key, RegexOptions.IgnoreCase);
            return rgx.Replace(resourceText, string.IsNullOrEmpty(replaceValue) ? string.Empty : replaceValue);
        }
    }

}
