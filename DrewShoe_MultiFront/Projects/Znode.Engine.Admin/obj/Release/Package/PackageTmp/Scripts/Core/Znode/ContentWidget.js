var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var isChangedWidget = false;
var attributeData = "";
var ContentWidget = /** @class */ (function (_super) {
    __extends(ContentWidget, _super);
    function ContentWidget() {
        return _super.call(this) || this;
    }
    ContentWidget.prototype.GetVariants = function () {
        var contentWidgetId = $("#ContentWidgetId").val();
        ZnodeBase.prototype.BrowseAsidePoupPanel('/ContentWidget/GetVariants?contentWidgetId=' + contentWidgetId, 'VariantPanel');
    };
    ContentWidget.prototype.GetUnassociatedProfiles = function () {
        var widgetKey = $("#WidgetKey").val();
        var localeId = $('#ddlLocale option:selected').val();
        Endpoint.prototype.GetUnassociatedProfileList(widgetKey, localeId, function (response) {
            var profileList = response.ProfileList;
            $('#profileList').empty();
            $.each(profileList, function (index, element) {
                $('#profileList').append($("<option value=\"" + element.Value + "\"><text>" + element.Text + "</text></option>"));
            });
        });
    };
    ContentWidget.prototype.BindVariantDropdown = function (data) {
        if (data != undefined && data != null) {
            var variantList = data.VariantList;
            $('#ddlWidgetVariants').empty();
            $('#ddlWidgetVariants').removeAttr("disabled");
            $.each(variantList, function (index, element) {
                element.WidgetTemplateId = (element.WidgetTemplateId == null) ? "" : element.WidgetTemplateId;
                $('#ddlWidgetVariants').append($("<option value=\"" + element.WidgetProfileVariantId + "\" data-template = \"" + element.WidgetTemplateId + "\" ><text>Variant For " + element.ProfileName + " And " + element.LocaleName + "</text></option>"));
            });
            ZnodeBase.prototype.HideLoader();
            ContentWidget.prototype.GetAssociatedAttributes();
        }
        ZnodeBase.prototype.CancelUpload('VariantPanel');
    };
    ContentWidget.prototype.BindAssociatedAttributeValue = function (data) {
        if (data != undefined && data != null) {
            $("#AssociatedGroups").html("");
            $("#AssociatedGroups").html(data);
            $.getScript("/Scripts/References/DynamicValidation.js");
            reInitializationMce();
            $("#DynamicHeading").text("Edit - " + $('#ddlWidgetVariants option:selected').text());
            ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SuccessChangesSaved"), "success", isFadeOut, fadeOutTime);
        }
    };
    ContentWidget.prototype.GetAssociatedAttributes = function () {
        var entityType = "Widget";
        var entityId = $('#ddlWidgetVariants option:selected').val();
        $('#drpwidgetTemplate').val($('#ddlWidgetVariants option:selected').attr("data-template"));
        $("#EntityId").val($('#ddlWidgetVariants option:selected').val());
        $("#EntityType").val("Widget");
        if (entityId > 0) {
            Endpoint.prototype.GetAssociatedVariants(entityId, entityType, function (response) {
                $("#AssociatedGroups").html("");
                $("#AssociatedGroups").html(response);
                reInitializationMce();
                $("#DynamicHeading").text("Edit - " + $('#ddlWidgetVariants option:selected').text());
                attributeData = $("#frmGlobalAttribute").serialize();
                $("#variantWidgetTemplate").show();
                $("#variantAttributeData").show();
                ZnodeBase.prototype.HideLoader();
            });
        }
        ZnodeBase.prototype.HideLoader();
    };
    ContentWidget.prototype.DeleteContentWidget = function (control) {
        var contentWidgetId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (contentWidgetId.length > 0) {
            Endpoint.prototype.DeleteContentWidget(contentWidgetId, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    };
    ContentWidget.prototype.DeleteVariant = function () {
        var variantId = $('#ddlWidgetVariants option:selected').val();
        var widgetKey = $('#WidgetKey').val();
        if (variantId > 0) {
            Endpoint.prototype.DeleteAssociatedVariant(variantId, widgetKey, function (response) {
                if (response.status) {
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "success", isFadeOut, fadeOutTime);
                    $("#ddlWidgetVariants option[value=" + variantId + "]").remove();
                    if ($("#ddlWidgetVariants option").length > 0) {
                        ContentWidget.prototype.GetAssociatedAttributes();
                    }
                    else {
                        $("#AssociatedGroups").html("");
                    }
                }
                else {
                    ZnodeBase.prototype.HideLoader();
                    ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "error", isFadeOut, fadeOutTime);
                }
            });
        }
    };
    ContentWidget.prototype.fnShowHide = function (control, divId) {
        control = $(divId).parent("div").find(".panel-heading").find("h4");
        if ($(control).hasClass("collapsed")) {
            $(control).removeClass("collapsed");
        }
        else {
            $(control).addClass("collapsed");
        }
        $("#" + divId).children(".panel-body").children(".widgetAtribute").show();
        $("#" + divId).slideToggle();
    };
    ContentWidget.prototype.SaveEntityAttribute = function (backURL) {
        if ($('#drpwidgetTemplate').val() == "") {
            $("#errorRequiredTemplate").text('').text(ZnodeBase.prototype.getResourceByKeyName("ErrorWidgetTemplate")).addClass("field-validation-error").show();
            $("#drpwidgetTemplate").addClass('input-validation-error');
            ContentWidget.prototype.ValidateForm();
        }
        else {
            if (isChangedWidget && ((attributeData) != $("#frmGlobalAttribute").serialize())) {
                var variantId = $('#ddlWidgetVariants option:selected').val();
                var widgetTemplateId = $('#drpwidgetTemplate').val();
                Endpoint.prototype.AssociateWidgetTemplate(variantId, widgetTemplateId, function (response) {
                    $('#ddlWidgetVariants option:selected').attr("data-template", $('#drpwidgetTemplate').val());
                    isChangedWidget = false;
                });
                $("#frmGlobalAttribute").submit();
                ContentWidget.prototype.ValidateForm();
                ZnodeBase.prototype.HideLoader();
            }
            else if (isChangedWidget && ((attributeData) == $("#frmGlobalAttribute").serialize())) {
                var variantId = $('#ddlWidgetVariants option:selected').val();
                var widgetTemplateId = $('#drpwidgetTemplate').val();
                Endpoint.prototype.AssociateWidgetTemplate(variantId, widgetTemplateId, function (response) {
                    $('#ddlWidgetVariants option:selected').attr("data-template", $('#drpwidgetTemplate').val());
                    isChangedWidget = false;
                });
                ZnodeBase.prototype.HideLoader();
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(ZnodeBase.prototype.getResourceByKeyName("SuccessChangesSaved"), "success", isFadeOut, fadeOutTime);
            }
            else if (!isChangedWidget && ((attributeData) != $("#frmGlobalAttribute").serialize())) {
                if (!$("#frmGlobalAttribute").valid()) {
                    $("#frmGlobalAttribute .input-validation-error").closest('.panel-collapse').attr("style", "display:block");
                }
                $("#frmGlobalAttribute").submit();
                ContentWidget.prototype.ValidateForm();
            }
            else {
                $("#frmGlobalAttribute").submit();
                ContentWidget.prototype.ValidateForm();
            }
        }
    };
    ContentWidget.prototype.ValidateForm = function () {
        if (!$("#frmGlobalAttribute").valid()) {
            $("#frmGlobalAttribute .input-validation-error").closest('.panel-collapse').attr("style", "display:block");
        }
    };
    ContentWidget.prototype.OnTemplateChange = function () {
        if ($('#drpwidgetTemplate').val() != "") {
            $("#drpwidgetTemplate").removeClass("input-validation-error");
            $("#errorRequiredTemplate").text('').text("").removeClass("field-validation-error").hide();
        }
        isChangedWidget = true;
    };
    ContentWidget.prototype.SetActiveGroup = function (group) {
        $(this).addClass('active-tab-validation');
    };
    ContentWidget.prototype.ValidateData = function () {
        var isValid = true;
        if ($("#txtPortalName").is(':visible') && ($(".portalsuggestion *").attr('readonly') == undefined)) {
            if ($("#txtPortalName").val() == "") {
                $("#errorRequiredStore").text('').text(ZnodeBase.prototype.getResourceByKeyName("ErrorSelectPortal")).addClass("field-validation-error").show();
                $("#txtPortalName").parent("div").addClass('input-validation-error');
                isValid = false;
            }
        }
        if ($('#ContentWidgetId').val() < 1 && $('#WidgetKey').val() != '') {
            Endpoint.prototype.IsWidgetExist($('#WidgetKey').val(), function (response) {
                if (response.data) {
                    $("#WidgetKey").addClass("input-validation-error");
                    $("#errorSpanWidgetKey").addClass("error-msg");
                    $("#errorSpanWidgetKey").text(ZnodeBase.prototype.getResourceByKeyName("AlreadyExistAttributeWidgetKey"));
                    $("#errorSpanWidgetKey").show();
                    isValid = false;
                }
            });
        }
        return isValid;
    };
    ContentWidget.prototype.IsGlobal = function () {
        if ($('#PortalId').val() == "0" && window.location.href.toLowerCase().indexOf("update") !== -1) {
            $("#IsGlobalContentWidget").attr("checked", "checked");
        }
        if ($("#IsGlobalContentWidget").is(":checked")) {
            $("#txtPortalName").val('');
            $("#IsGlobalContentWidget").val("true");
            $('#hdnPortalId').val(0);
            $(".portalsuggestion *").attr('readonly', true);
            $('.portalsuggestion').css({ pointerEvents: "none" });
            $(".fstElement").css({ "background-color": "#e7e7e7" });
            $(".fstElement").removeClass('input-validation-error');
            $("#errorRequiredStore").text('').text("").removeClass("field-validation-error").hide();
            $("#txtPortalName").removeClass('input-validation-error');
            $(".fstToggleBtn").html("Select Store");
            $(".fstResultItem").removeClass('fstSelected');
            $('#PortalId').val("0");
            $('#hdnPortalId').val("0");
            $('.fstToggleBtn').html("All Stores");
            $('#StoreName').val("All Stores");
        }
        else {
            $(".portalsuggestion *").attr('readonly', false);
            $('.portalsuggestion').css({ pointerEvents: "visible" });
            $("#IsGlobalContentWidget").val("false");
            $(".fstElement").css({ "background-color": "#fff" });
            $('.fstToggleBtn').html("Select Store");
        }
    };
    ContentWidget.prototype.DeleteWidgetVariant = function () {
        $("#DeleteWidgetVariant").modal("show");
    };
    ContentWidget.prototype.OnSelectPortalResult = function (item) {
        if (item != undefined) {
            var portalName = item.text;
            $('#StoreName').val(portalName);
            Store.prototype.OnSelectStoreAutocompleteDataBind(item);
        }
    };
    return ContentWidget;
}(ZnodeBase));
//# sourceMappingURL=ContentWidget.js.map