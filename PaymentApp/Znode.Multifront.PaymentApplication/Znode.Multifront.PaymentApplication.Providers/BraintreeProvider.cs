﻿using Braintree;
using System;
using System.Diagnostics;
using Znode.Multifront.PaymentApplication.Data;
using Znode.Multifront.PaymentApplication.Helpers;
using Znode.Multifront.PaymentApplication.Models;

namespace Znode.Multifront.PaymentApplication.Providers
{
    /// <summary>
    /// This is the class responsible for the payment related operations of braintree payment provider
    /// </summary>
    public class BraintreeProvider : BaseProvider, IPaymentProviders
    {
        #region Private Variable
        BraintreeGateway gateway = null;
        #endregion

        #region Public Methods

        /// <summary>
        /// This method is used to validate the credit cards.  Which will call the Capture call or create customer accordingly.
        /// </summary>
        /// <param name="paymentModel">Complete Payment Data from Multifront</param>
        /// <returns>Repsonse from the payment gateway</returns>
        public GatewayResponseModel ValidateCreditcard(PaymentModel paymentModel)
        {
            gateway = MapBrainTreeCredentials(paymentModel);
            Logging.LogMessage("DSODE-97-BraintreeProvider ValidateCreditcard gateway start =:" + gateway?.AccessToken + "orderno " + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            if (!string.IsNullOrEmpty(paymentModel.CustomerProfileId))
            {
                Logging.LogMessage("DSODE-97-BraintreeProvider ValidateCreditcard paymentModel CustomerProfileId =:" + paymentModel?.CustomerProfileId, Logging.Components.Payment.ToString(), TraceLevel.Error);
                return (paymentModel.IsCapture)
                    ? CaptureTransaction(paymentModel) : AuthorizeTransaction(paymentModel);
            }
            Logging.LogMessage("DSODE-97-BraintreeProvider ValidateCreditcard gateway end =:" + paymentModel?.AddressId + "orderno " + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            return CreateCustomer(paymentModel);
        }

        /// <summary>
        /// This method will create the subscription in the payment gateway.
        /// </summary>
        /// <param name="paymentModel">Complete Payment Data from Multifront</param>
        /// <returns>Repsonse from the payment gateway</returns>
        public GatewayResponseModel Subscription(PaymentModel paymentModel)
        {
            Logging.LogMessage("DSODE-97-BraintreeProvider Subscription end =:" + paymentModel?.AddressId + "orderno " + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            gateway = MapBrainTreeCredentials(paymentModel);
            GatewayResponseModel paymentGatewayResponse = new GatewayResponseModel();
            try
            {
                Logging.LogMessage("DSODE-97-BraintreeProvider Subscription try start =:" + paymentModel?.AccessToken + "orderno " + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
                if (!string.IsNullOrEmpty(paymentModel.TransactionId))
                {
                    SubscriptionRequest subscriptionRequest = new SubscriptionRequest
                    {
                        Price = decimal.Parse(paymentModel.Subscription.InitialAmount.ToString()),
                        FirstBillingDate = DateTime.Now,
                        NeverExpires = false,
                        HasTrialPeriod = false,
                        NumberOfBillingCycles = paymentModel.Subscription.TotalCycles,
                        PlanId = paymentModel.Subscription.ProfileName,
                    };

                    Result<Subscription> subscriptionResult = gateway.Subscription.Create(subscriptionRequest);

                    paymentGatewayResponse.IsSuccess = subscriptionResult.IsSuccess();
                    paymentGatewayResponse.ResponseText = subscriptionResult.Message;
                }
                else
                {
                    paymentGatewayResponse.IsSuccess = false;
                    paymentGatewayResponse.ResponseText = "Error occured while retrieving Transaction ID.";
                }
                Logging.LogMessage("DSODE-97-BraintreeProvider Subscription try end =:" + paymentModel?.AccessToken + "orderno " + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            }
            catch (Exception ex)
            {
                Logging.LogMessage("DSODE-97-BraintreeProvider Subscription catch =:" + ex, Logging.Components.Payment.ToString(), TraceLevel.Error);
                paymentGatewayResponse.IsSuccess = false;
                paymentGatewayResponse.ResponseText = ex.Message.ToString();
                Logging.LogMessage(ex, Logging.Components.Payment.ToString(), TraceLevel.Error);
                LoggingService.LogActivity(paymentModel.PaymentApplicationSettingId, ex.Message);
            }
            Logging.LogMessage("DSODE-97-BraintreeProvider Subscription end" + " orderno " + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            return paymentGatewayResponse;
        }

        /// <summary>
        /// This method will void the payment.
        /// </summary>
        /// <param name="paymentModel">Complete Payment Data from Multifront</param>
        /// <returns>Repsonse from the payment gateway</returns>
        public GatewayResponseModel Void(PaymentModel paymentModel)
        {
            Logging.LogMessage("DSODE-97-BraintreeProvider Void start" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            gateway = MapBrainTreeCredentials(paymentModel);
            GatewayResponseModel paymentGatewayResponse = new GatewayResponseModel();
            try
            {
                Logging.LogMessage("DSODE-97-BraintreeProvider Void try start" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
                if (!string.IsNullOrEmpty(paymentModel.TransactionId))
                {
                    //Call gateway void method
                    Result<Transaction> transactionResult = gateway.Transaction.Void(paymentModel.TransactionId);
                    paymentGatewayResponse.IsSuccess = transactionResult.IsSuccess();
                    paymentGatewayResponse.ResponseText = transactionResult.Message;
                    if (paymentGatewayResponse.IsSuccess)
                        paymentGatewayResponse.PaymentStatus = ZnodePaymentStatus.VOIDED;
                }
                else
                {
                    paymentGatewayResponse.IsSuccess = false;
                    paymentGatewayResponse.ResponseText = "Error occured while retrieving Transaction ID.";
                }
            }
            catch (Exception ex)
            {
                Logging.LogMessage("DSODE-97-BraintreeProvider Void" + ex + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
                paymentGatewayResponse.IsSuccess = false;
                paymentGatewayResponse.ResponseText = ex.Message.ToString();
                Logging.LogMessage(ex, Logging.Components.Payment.ToString(), TraceLevel.Error);
                LoggingService.LogActivity(null, ex.Message);
            }
            Logging.LogMessage("DSODE-97-BraintreeProvider Void end" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            return paymentGatewayResponse;
        }

        /// <summary>
        /// This method will refund the payment amount to the relevant customer.
        /// </summary>
        /// <param name="paymentModel">Complete Payment Data from Multifront</param>
        /// <returns>Repsonse from the payment gateway</returns>
        public GatewayResponseModel Refund(PaymentModel paymentModel)
        {
            gateway = MapBrainTreeCredentials(paymentModel);
            GatewayResponseModel paymentGatewayResponse = new GatewayResponseModel();
            try
            {
                if (!string.IsNullOrEmpty(paymentModel.TransactionId))
                {
                    //Refund Transaction
                    Result<Transaction> transactionResult = gateway.Transaction.Refund(paymentModel.TransactionId, decimal.Parse(paymentModel.Total));
                    paymentGatewayResponse.IsSuccess = transactionResult.IsSuccess();
                    paymentGatewayResponse.ResponseText = transactionResult.Message;
                    if (paymentGatewayResponse.IsSuccess)
                        paymentGatewayResponse.PaymentStatus = ZnodePaymentStatus.REFUNDED;
                }
                else
                    paymentGatewayResponse.ResponseText = "Error occured while retrieving Transaction ID.";
            }
            catch (Exception ex)
            {
                paymentGatewayResponse.IsSuccess = false;
                paymentGatewayResponse.ResponseText = ex.Message.ToString();
                Logging.LogMessage(ex, Logging.Components.Payment.ToString(), TraceLevel.Error);
                LoggingService.LogActivity(null, ex.Message);
            }

            return paymentGatewayResponse;
        }

        public TransactionDetailsModel GetTransactionDetails(PaymentModel paymentModel)
        {
            Logging.LogMessage("DSODE-97-BraintreeProvider GetTransactionDetails start orderno" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            return new TransactionDetailsModel();
        }

        #endregion

        #region Privare Methods

        /// <summary>
        /// This method will create the customer in the payment gateway.
        /// </summary>
        /// <param name="paymentModel">Complete Payment Data from Multifront</param>
        /// <returns>Repsonse from the payment gateway</returns>
        private GatewayResponseModel CreateCustomer(PaymentModel paymentModel)
        {
            Logging.LogMessage("DSODE-97-BraintreeProvider CreateCustomer start =:" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            GatewayResponseModel response = new GatewayResponseModel();
            GatewayConnector gatewayConnector = new GatewayConnector();
            bool isSuccess = false;
            if (!paymentModel.IsAnonymousUser)
            {
                if (string.IsNullOrEmpty(paymentModel.CustomerGUID))
                {
                    // Create Customer and save Credit card info
                    if (paymentModel.IsSaveCreditCard)
                    {
                        response = CreatePaymentGatewayCustomer(paymentModel);
                        if (response.IsSuccess)
                            isSuccess = gatewayConnector.SaveCustomerDetails(paymentModel);
                        response.CustomerGUID = paymentModel.CustomerGUID;
                        response.PaymentToken = paymentModel.PaymentToken;
                        response.CustomerProfileId = paymentModel.CustomerProfileId;
                        response.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
                        response.IsSuccess = isSuccess;
                        Logging.LogMessage("DSODE-97-BraintreeProvider Create Customerresponse CustomerGUID =:" + response?.CustomerGUID, Logging.Components.Payment.ToString(), TraceLevel.Error);
                        Logging.LogMessage("DSODE-97-BraintreeProvider CreateCustomer end", Logging.Components.Payment.ToString(), TraceLevel.Error);
                        return response;
                    }
                    else
                        Logging.LogMessage("DSODE-97-BraintreeProvider CreateCustomer end", Logging.Components.Payment.ToString(), TraceLevel.Error);
                    return CreatePaymentGatewayCustomer(paymentModel);
                }
                else
                {
                    //Save credit card info for existing customer
                    if (paymentModel.IsSaveCreditCard)
                    {
                        response = CreatePaymentGatewayVault(paymentModel);
                        if (response.IsSuccess)
                            isSuccess = gatewayConnector.SaveCustomerDetails(paymentModel);
                        response.CustomerGUID = paymentModel.CustomerGUID;
                        response.PaymentToken = paymentModel.PaymentToken;
                        response.CustomerProfileId = paymentModel.CustomerProfileId;
                        response.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
                        Logging.LogMessage("DSODE-97-BraintreeProvider Create Customerresponse PaymentToken =:" + response?.PaymentToken, Logging.Components.Payment.ToString(), TraceLevel.Error);
                        response.IsSuccess = isSuccess;
                        Logging.LogMessage("DSODE-97-BraintreeProvider CreateCustomer end", Logging.Components.Payment.ToString(), TraceLevel.Error);
                        return response;
                    }
                    else if (!string.IsNullOrEmpty(paymentModel.PaymentToken))
                    {
                        isSuccess = gatewayConnector.SaveCustomerDetails(paymentModel);
                        response.CustomerGUID = paymentModel.CustomerGUID;
                        response.PaymentToken = paymentModel.PaymentToken;
                        response.CustomerProfileId = paymentModel.CustomerProfileId;
                        response.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
                        response.IsSuccess = isSuccess;
                        Logging.LogMessage("DSODE-97-BraintreeProvider Create Customerresponse CustomerPaymentProfileId =:" + response?.CustomerPaymentProfileId, Logging.Components.Payment.ToString(), TraceLevel.Error);
                        Logging.LogMessage("DSODE-97-BraintreeProvider CreateCustomer end", Logging.Components.Payment.ToString(), TraceLevel.Error);
                        return response;
                    }
                    else
                        return CreatePaymentGatewayCustomer(paymentModel);   //As it is which is the normal one without vault.
                }
            }
            else
                return CreatePaymentGatewayCustomer(paymentModel);   //As it is which is the normal one without vault.
        }

        /// <summary>
        /// This method will Authorize transaction.
        /// </summary>
        /// <param name="paymentModel">Complete Payment Data from Multifront</param>
        /// <returns>Repsonse from the payment gateway</returns>
        private GatewayResponseModel AuthorizeTransaction(PaymentModel paymentModel)
        {
            try
            {
                Logging.LogMessage("DSODE-97-BraintreeProvider AuthorizeTransaction =:" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
                TransactionRequest transRequest = new TransactionRequest
                {
                    Amount = decimal.Parse(paymentModel.Total),
                    CustomerId = paymentModel.CustomerProfileId,
                    PaymentMethodToken = paymentModel.CustomerPaymentProfileId,

                    //SubmitForSettlement= true will proceed with capture payment, SubmitForSettlement= false will Authorize payment 
                    Options = new TransactionOptionsRequest { SubmitForSettlement = false }
                };
                Result<Transaction> transactionResult = gateway.Transaction.Sale(transRequest);
                return MapGatewayResponseModel(transactionResult, paymentModel, false);
            }
            catch (Exception ex)
            {
                Logging.LogMessage("DSODE-97-BraintreeProvider AuthorizeTransaction ex =:" + paymentModel?.OrderNumber + ex, Logging.Components.Payment.ToString(), TraceLevel.Error);
                Logging.LogMessage(ex, Logging.Components.Payment.ToString(), TraceLevel.Error);
                LoggingService.LogActivity(null, ex.Message);
                return new GatewayResponseModel
                {
                    GatewayResponseData = ex.Message.ToString(),
                    IsSuccess = false
                };
            }
        }

        /// <summary>
        /// Capture the Authorised Transaction
        /// </summary>
        /// <param name="paymentModel">paymentModel</param>
        /// <returns>GatewayResponseModel</returns>
        private GatewayResponseModel CaptureTransaction(PaymentModel paymentModel)
        {
            try
            {
                Logging.LogMessage("DSODE-97-BraintreeProvider CaptureTransaction =:" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
                //Capture Transaction
                Result<Transaction> transactionResult = gateway.Transaction.SubmitForSettlement(paymentModel.TransactionId);
                return MapGatewayResponseModel(transactionResult, paymentModel, true);
            }
            catch (Exception ex)
            {
                Logging.LogMessage("DSODE-97-BraintreeProvider CaptureTransaction =:" + paymentModel?.OrderNumber + ex, Logging.Components.Payment.ToString(), TraceLevel.Error);
                Logging.LogMessage(ex, Logging.Components.Payment.ToString(), TraceLevel.Error);
                LoggingService.LogActivity(null, ex.Message);
                return new GatewayResponseModel
                {
                    GatewayResponseData = ex.Message.ToString(),
                    IsSuccess = false
                };

            }
        }

        /// <summary>
        /// Create a Customer using card holder data.
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        private GatewayResponseModel GetCustomer(PaymentModel paymentModel)
        {
            Logging.LogMessage("DSODE-97-BraintreeProvider GetCustomer start =:" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            GatewayResponseModel gatewayResponseModel = new GatewayResponseModel();
            try
            {
                CustomerRequest customerRequest = new CustomerRequest
                {
                    FirstName = paymentModel.BillingFirstName,
                    LastName = paymentModel.BillingLastName,
                    Company = string.Empty,
                    Email = paymentModel.BillingEmailId,
                    Fax = paymentModel.BillingPhoneNumber,
                    Phone = paymentModel.BillingPhoneNumber,
                    Website = string.Empty,
                };

                Result<Customer> resultCusomer = gateway.Customer.Create(customerRequest);
                resultCusomer.IsSuccess();
                string customerId = resultCusomer.Target.Id;
                gatewayResponseModel.IsSuccess = !string.IsNullOrEmpty(customerId);
                gatewayResponseModel.CustomerProfileId = customerId;
            }
            catch (Exception ex)
            {
                Logging.LogMessage("DSODE-97-BraintreeProvider GetCustomer =:" + paymentModel?.OrderNumber + ex, Logging.Components.Payment.ToString(), TraceLevel.Error);
                gatewayResponseModel.IsSuccess = false;
                gatewayResponseModel.GatewayResponseData = ex.Message.ToString();
                Logging.LogMessage(ex, Logging.Components.Payment.ToString(), TraceLevel.Error);
                LoggingService.LogActivity(null, ex.Message);
            }
            Logging.LogMessage("DSODE-97-BraintreeProvider GetCustomer end =:" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            return gatewayResponseModel;
        }

        /// <summary>
        /// Create the Customer Payment
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns>returns customer payment response</returns>
        private GatewayResponseModel CreateCustomerPayment(PaymentModel paymentModel)
        {
            Logging.LogMessage("DSODE-97-BraintreeProvider CreateCustomerPayment start =:" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            GatewayResponseModel response = new GatewayResponseModel();

            CreditCardRequest creditCardRequest = new CreditCardRequest
            {
                CustomerId = paymentModel.CustomerProfileId,
                CVV = paymentModel.CardSecurityCode,
                Number = paymentModel.CardNumber,
                ExpirationMonth = paymentModel.CardExpirationMonth,
                ExpirationYear = paymentModel.CardExpirationYear,
            };

            Result<CreditCard> ccResult = gateway.CreditCard.Create(creditCardRequest);
            response.IsSuccess = ccResult.IsSuccess();
            if (response.IsSuccess)
            {
                response.CustomerProfileId = paymentModel.CustomerProfileId;
                response.CustomerPaymentProfileId = ccResult.Target.Token;
                paymentModel.CreditCardImageUrl = ccResult.Target.ImageUrl;
            }
            else
            {
                response.CustomerProfileId = paymentModel.CustomerProfileId;
                response.ResponseText = ccResult.Message;
            }
            Logging.LogMessage("DSODE-97-BraintreeProvider CreateCustomerPayment end =:" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            return response;
        }

        /// <summary>
        /// To Customer new customer account and ADD voult
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns>returns gateway response</returns>
        private GatewayResponseModel CreatePaymentGatewayCustomer(PaymentModel paymentModel)
        {
            Logging.LogMessage("DSODE-97-BraintreeProvider CreatePaymentGatewayCustomer start =:" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            GatewayResponseModel response = GetCustomer(paymentModel);
            if (!string.IsNullOrEmpty(response.CustomerProfileId))
            {
                paymentModel.CustomerProfileId = response.CustomerProfileId;
                response = CreateCustomerPayment(paymentModel);
                paymentModel.CustomerPaymentProfileId = response.CustomerPaymentProfileId;
                Logging.LogMessage("DSODE-97-BraintreeProvider  CreatePaymentGatewayCustomer paymentModel CustomerPaymentProfileId =:" + paymentModel?.CustomerPaymentProfileId, Logging.Components.Payment.ToString(), TraceLevel.Error);

                if (response.IsSuccess && paymentModel.IsAnonymousUser)
                    response.IsSuccess = new GatewayConnector().SavePaymentDetails(paymentModel);
            }
            Logging.LogMessage("DSODE-97-BraintreeProvider CreatePaymentGatewayCustomer end =:" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            return Equals(response, null) ? new GatewayResponseModel() : response;
        }

        /// <summary>
        /// To vault using existing customer id
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns>returns gateway response</returns>
        private GatewayResponseModel CreatePaymentGatewayVault(PaymentModel paymentModel)
        {
            Logging.LogMessage("DSODE-97-BraintreeProvider CreatePaymentGatewayVault start =:" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            GatewayResponseModel gatewayResponseModel = new GatewayResponseModel();

            ZnodePaymentMethod payment = new PaymentMethodsService().GetPaymentMethod(paymentModel.PaymentApplicationSettingId, paymentModel.CustomerGUID);
            if (!Equals(payment, null) && !string.IsNullOrEmpty(payment.CustomerProfileId))
                paymentModel.CustomerProfileId = payment.CustomerProfileId;
            else
            {
                gatewayResponseModel = GetCustomer(paymentModel);
                paymentModel.CustomerProfileId = gatewayResponseModel.CustomerProfileId;
            }
            if (!string.IsNullOrEmpty(paymentModel.CustomerProfileId))
            {
                gatewayResponseModel = CreateCustomerPayment(paymentModel);
                paymentModel.CustomerPaymentProfileId = gatewayResponseModel.CustomerPaymentProfileId;
            }
            Logging.LogMessage("DSODE-97-BraintreeProvider CreatePaymentGatewayVault end =:" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            return Equals(gatewayResponseModel, null) ? new GatewayResponseModel() : gatewayResponseModel;
        }

        //Map GatewayResponseModel
        private GatewayResponseModel MapGatewayResponseModel(Result<Transaction> transactionResult, PaymentModel paymentModel, bool isCapture)
        {
            Logging.LogMessage("DSODE-97-BraintreeProvider MapGatewayResponseModel start =:" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            GatewayResponseModel gatewayResponseModel = new GatewayResponseModel();
            gatewayResponseModel.IsSuccess = transactionResult.IsSuccess();

            if (gatewayResponseModel.IsSuccess)
            {
                gatewayResponseModel.TransactionId = transactionResult.Target.Id;
                gatewayResponseModel.PaymentStatus = isCapture ? ZnodePaymentStatus.CAPTURED : ZnodePaymentStatus.AUTHORIZED;
                gatewayResponseModel.CustomerProfileId = paymentModel.CustomerProfileId;
                gatewayResponseModel.CustomerPaymentProfileId = paymentModel.CustomerPaymentProfileId;
                gatewayResponseModel.ResponseText = transactionResult.Message;
                gatewayResponseModel.IsGatewayPreAuthorize = paymentModel.GatewayPreAuthorize;
            }
            else
            {
                gatewayResponseModel.ResponseText = transactionResult.Transaction.ProcessorResponseText;
                gatewayResponseModel.ResponseCode = transactionResult.Transaction.ProcessorResponseCode;
            }
            Logging.LogMessage("DSODE-97-BraintreeProvider MapGatewayResponseModel end =:" + paymentModel?.OrderNumber, Logging.Components.Payment.ToString(), TraceLevel.Error);
            return gatewayResponseModel;
        }

        //Map Braintree Credentials
        private BraintreeGateway MapBrainTreeCredentials(PaymentModel paymentModel)
        => new BraintreeGateway
        {
            Environment = paymentModel.GatewayTestMode ? Braintree.Environment.SANDBOX : Braintree.Environment.PRODUCTION,
            MerchantId = paymentModel.GatewayLoginName,
            PublicKey = paymentModel.GatewayLoginPassword,
            PrivateKey = paymentModel.GatewayTransactionKey
        };
        #endregion
    }
}
