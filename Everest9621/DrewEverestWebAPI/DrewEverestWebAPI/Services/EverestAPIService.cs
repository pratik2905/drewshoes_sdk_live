﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Xml;
using EverestAPI.Net;
using EverestAPI.Net.ObjectModel;
using Znode.Engine.Api.Models;

namespace DrewEverestWebAPI.Services
{
    public class EverestAPIService
    {
        #region Private Variables
        private const string DrewCustomerCode = "199177";
        private string serverName = Convert.ToString(ConfigurationManager.AppSettings["EverestDBServer"]);
        private string companyCode = Convert.ToString(ConfigurationManager.AppSettings["EverestCompanyCode"]);
        private string userName = Convert.ToString(ConfigurationManager.AppSettings["EverestUsername"]);
        private string password = Convert.ToString(ConfigurationManager.AppSettings["EverestPassword"]);
        private string loggedInJurisdiction = Convert.ToString(ConfigurationManager.AppSettings["EverestLoggedInJurisdiction"]);
        private string loggedInDepartment = Convert.ToString(ConfigurationManager.AppSettings["EverestLoggedInDepartment"]);

        private string PortalIDDrew = Convert.ToString(ConfigurationManager.AppSettings["PortalIDDrew"]);
        private string PortalIDRos = Convert.ToString(ConfigurationManager.AppSettings["PortalIDRos"]);
        private string PortalIDBellini = Convert.ToString(ConfigurationManager.AppSettings["PortalIDBellini"]);
        private string PortalIDDealer = Convert.ToString(ConfigurationManager.AppSettings["PortalIDDealer"]);
        #endregion



        //public string GetSessionID()
        //{
        //    string sessionId = "";

        //        string serverName = Convert.ToString(ConfigurationManager.AppSettings["EverestDBServer"]);
        //        string companyCode = Convert.ToString(ConfigurationManager.AppSettings["EverestCompanyCode"]);
        //        string userName = Convert.ToString(ConfigurationManager.AppSettings["EverestUsername"]);
        //        string password = Convert.ToString(ConfigurationManager.AppSettings["EverestPassword"]);
        //        string loggedInJurisdiction = Convert.ToString(ConfigurationManager.AppSettings["EverestLoggedInJurisdiction"]);
        //        string loggedInDepartment = Convert.ToString(ConfigurationManager.AppSettings["EverestLoggedInDepartment"]);

        //    EverestAPI.eoSession session = new EverestAPI.eoSession();
        //    sessionId = session.Open(serverName, companyCode, userName, password, DateTime.Now, "", loggedInJurisdiction, loggedInDepartment);


        //    return sessionId;
        //}
        #region Address
        public string CreateAddress(AddressModel model)
        {
            string addressxml = CreateAddressXMLForEverest(model);
            EverestAPI.eoSession session = new EverestAPI.eoSession();
            string sessionId = session.Open(serverName, companyCode, userName, password, DateTime.Now, "", loggedInJurisdiction, loggedInDepartment);
            EverestAPI.eoAddress everAddress = new EverestAPI.eoAddress();
            string result = everAddress.Create(sessionId, addressxml, EverestConsts.enumAddressTypes.atCustomerShipping, true);
            if (!string.IsNullOrEmpty(sessionId))
            {
                session.Close(sessionId);
            }
            Marshal.ReleaseComObject(session);
            session = null;
            return result;
        }

        private string CreateAddressXMLForEverest(AddressModel address)
        {

            string new_string = address.Address1 + System.Environment.NewLine + address.Address2;
            string postalcode = address.PostalCode.Substring(0, 5);
            string xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n";
            xml += "<Address>\r\n";
            xml += "<General>\r\n";
            //CustomerCode for Consumer is fixed in Drew and its value is going to be 199177

            if (address.PortalId == Convert.ToInt64(PortalIDDrew))
                xml += "<CustomerCode>199177</CustomerCode>\r\n";
            else if (address.PortalId == Convert.ToInt64(PortalIDRos))
                xml += "<CustomerCode>274673</CustomerCode>\r\n";
            else if (address.PortalId == Convert.ToInt64(PortalIDBellini))
                xml += "<CustomerCode>275759</CustomerCode>\r\n";
            else if (address.PortalId == Convert.ToInt64(PortalIDDealer))
                //xml += "<CustomerCode>" + address.ExternalId + " </CustomerCode>\r\n";
                xml += "<CustomerCode>" + address.Custom1 + " </CustomerCode>\r\n";
            else
                // xml += "<CustomerCode>199177</CustomerCode>\r\n";
                xml += "<CustomerCode></CustomerCode>\r\n";

            xml += "<Company>" + address.CompanyName + "</Company>";
            xml += "<AddressInfo>\r\n";
            //Need to confirm from everest what is AddressType=5 Shipping or Billing.
            //5 is used here as it is given in Sample request xml of Everest
            xml += "<AddressType>5</AddressType>\r\n";
            xml += "<FirstName>" + address.FirstName + "</FirstName>\r\n";
            xml += "<LastName>" + address.LastName + "</LastName>\r\n";
            xml += "<Address>" + new_string + "</Address>\r\n";
            xml += "<City>" + address.CityName + "</City>\r\n";
            xml += "<State>" + address.StateCode + "</State>\r\n";
            //xml += "<ZipCode>" + address.PostalCode + "</ZipCode>\r\n";
            xml += "<ZipCode>" + postalcode + "</ZipCode>\r\n";
            xml += "<Countrycode>"+address.CountryName+"</Countrycode>\r\n";
            xml += "<Telephone1>" + address.PhoneNumber + "</Telephone1>";
            //xml += "<Telephone2>" + address.Mobilenumber + "</Telephone2>"; 
            xml += "<Mobile>" + address.Mobilenumber + "</Mobile >";
            xml += "<E-mail>" + address.EmailAddress + "</E-mail>";
            xml += "</AddressInfo>\r\n";
            xml += "<ApplyMasks/>\r\n";
            xml += "</General>\r\n";
            xml += "<Others>\r\n";
            xml += "<Miscellaneous/>\r\n";
            xml += "<Security/>\r\n";
            xml += "</Others>\r\n";
            xml += "</Address>\r\n";
            return xml;
        }
        #endregion

        #region SaleOrder
        public string CreateSaleorder(OrderModel model)
        {
            string Orderxml = CreateOrderXMLForEverest(model);
            EverestAPI.eoSession session = new EverestAPI.eoSession();
            string sessionId = session.Open(serverName, companyCode, userName, password, DateTime.Now, "", loggedInJurisdiction, loggedInDepartment);
            EverestAPI.eoSalesOrder so = new EverestAPI.eoSalesOrder();
            string result = so.Create(sessionId, Orderxml, true);
            if (!string.IsNullOrEmpty(sessionId))
            {
                session.Close(sessionId);
            }
            Marshal.ReleaseComObject(session);
            session = null;
            return result;

        }

        public string UpdateSaleorder(OrderModel model)
        {
            string Orderxml = CreateOrderXMLForEverest(model);
            EverestAPI.eoSession session = new EverestAPI.eoSession();
            string sessionId = session.Open(serverName, companyCode, userName, password, DateTime.Now, "", loggedInJurisdiction, loggedInDepartment);
            EverestAPI.eoSalesOrder so = new EverestAPI.eoSalesOrder();
            string result = so.Update(sessionId, Orderxml, true);
            if (!string.IsNullOrEmpty(sessionId))
            {
                session.Close(sessionId);
            }
            Marshal.ReleaseComObject(session);
            session = null;
            return result;

        }

        public string CancelSaleorder(OrderModel model)
        {
            string message = "";
            EverestAPI.eoSession session = new EverestAPI.eoSession();
            string sessionId = session.Open(serverName, companyCode, userName, password, DateTime.Now, "", loggedInJurisdiction, loggedInDepartment);

            EverestAPI.eoSalesOrder so = new EverestAPI.eoSalesOrder();
            bool result = so.Cancel(sessionId, model.Custom1);
            if (!string.IsNullOrEmpty(sessionId))
            {
                session.Close(sessionId);
            }
            Marshal.ReleaseComObject(session);
            session = null;
            if (result)
                message = "Order Cancelled Successfully";

            return message;

        }

        private string CreateOrderXMLForEverest(OrderModel orderModel)
        {
            //Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB"); //dd/MM/yyyy
            //string datestr = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            //DateTime date = DateTime.Parse(datestr);
            //string dtst = DateTime.Now.Year.ToString() + "-0" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
            //DateTime date = DateTime.ParseExact(dtst, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            /*This is Hardcoded XML to check if order is created or not
             In future need to replace hardcoded values with respective PimAttributes Values.
             Commented nodes are part of Request XML but while creating test order in Everest
             API Browser we removed them*/
            string xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\r\n";
            xml += "<SalesOrder>\r\n";
            //Update Order Case
            if (orderModel.Custom1 != "" && orderModel.Custom1 != null)
                xml += "<DocumentNo>" + orderModel.Custom1 + "</DocumentNo>\r\n";
            //CustomerCode for Consumer is fixed in Drew and its value is going to be 199177
            //xml += "<CustomerCode>199177</CustomerCode>\r\n";
            //xml += "<ContactCode>13724</ContactCode>\r\n";

            if (orderModel.PortalId == Convert.ToInt64(PortalIDDrew))
            {
                xml += "<CustomerCode>199177</CustomerCode>\r\n";
                xml += "<ContactCode>13724</ContactCode>\r\n";
            }
            else if (orderModel.PortalId == Convert.ToInt64(PortalIDRos))
            {
                xml += "<CustomerCode>274673</CustomerCode>\r\n";
                xml += "<ContactCode>320742</ContactCode>\r\n";
            }
            else if (orderModel.PortalId == Convert.ToInt64(PortalIDBellini))
            {
                xml += "<CustomerCode>275759</CustomerCode>\r\n";
                xml += "<ContactCode>964244</ContactCode>\r\n";
            }
            else if (orderModel.PortalId == Convert.ToInt64(PortalIDDealer))
            {
                //int customercode = Convert.ToInt32(orderModel.Custom3);
                //int contactcode = Convert.ToInt32(orderModel.Custom4);                  
                xml += "<CustomerCode>" + orderModel.Custom3 + "</CustomerCode>\r\n";
                xml += "<ContactCode>" + orderModel.Custom4 + "</ContactCode>\r\n";
            }
            else
            {
                xml += "<CustomerCode>199177</CustomerCode>\r\n";
                xml += "<ContactCode>13724</ContactCode>\r\n";
            }
            xml += "<ShipToCode>" + orderModel.Custom2 + "</ShipToCode>\r\n";
            // xml += "<ShipToCode>1186076</ShipToCode>\r\n";
            xml += "<SalesRepCode>HOUSE</SalesRepCode>\r\n";

            if (orderModel.PortalId == Convert.ToInt64(PortalIDDealer))
            {
                xml += "<PayTermsCode>"+ orderModel.Custom5 +"</PayTermsCode>\r\n";
            }
            else
            {
                xml += "<PayTermsCode>MAN-CC</PayTermsCode>\r\n";
            }
            xml += "<OverRideDueDate>F</OverRideDueDate>\r\n";
            xml += "<DueDate></DueDate>\r\n";

            if (orderModel.PortalId == Convert.ToInt64(PortalIDDealer))
            {
                if (orderModel.TaxCost != 0)
                {
                    if (orderModel?.ShippingAddress?.CountryCode == "AU")
                    {
                        xml += "<JurisdictionCode>AUS</JurisdictionCode>\r\n";
                    }
                    else
                    {
                        xml += "<JurisdictionCode>"+ orderModel.ShippingAddress.StateCode +"</JurisdictionCode>\r\n";
                    }
                }
                else
                {
                    if (orderModel?.ShippingAddress?.CountryCode == "AU")
                    {
                        xml += "<JurisdictionCode>AUS</JurisdictionCode>\r\n";
                    }
                    else
                    {
                        xml += "<JurisdictionCode>OHOUT</JurisdictionCode>\r\n";
                    }
                }
            }
            else
            {
                if (orderModel.ShippingAddress.StateCode=="OH")
                {
                    xml += "<JurisdictionCode>OH</JurisdictionCode>\r\n";
                }
                else if (orderModel.ShippingAddress.StateCode =="FL")
                {
                    xml += "<JurisdictionCode>FLOR</JurisdictionCode>\r\n";
                }
                else if (orderModel.ShippingAddress.StateCode =="WI")
                {
                    xml += "<JurisdictionCode>WIS</JurisdictionCode>\r\n";
                }
                else if (orderModel.ShippingAddress.StateCode =="MO")
                {
                    xml += "<JurisdictionCode>MO</JurisdictionCode>\r\n";
                }
                else
                {
                    xml += "<JurisdictionCode>OHOUT</JurisdictionCode>\r\n";
                }
            }
            // if(orderModel.ShippingId==6)
            //xml += "<ShipViaCode>DRUPSB</ShipViaCode>\r\n";
            // else
            // xml += "<ShipViaCode>DRUSPS-P</ShipViaCode>\r\n";
            // xml += "<ShipViaCode>DRUPSG</ShipViaCode>\r\n";

            xml += "<ShipViaCode>"+ orderModel.ShippingCode +"</ShipViaCode>\r\n";

            if (orderModel.PortalId == Convert.ToInt64(PortalIDDealer))
            {
                var InHandDate = orderModel.InHandDate.ToString();
                DateTime dt1 = Convert.ToDateTime(InHandDate);
                var DeliveryDate = dt1.ToString("yyyy-MM-dd");
                xml += "<DeliveryDate>" + DeliveryDate + "</DeliveryDate>\r\n";
            }
            else
            {
                //xml += "<DeliveryDate>"+ DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + "</DeliveryDate>\r\n";
                xml += "<DeliveryDate>" + DateTime.Today.ToString("yyyy-MM-dd") + "</DeliveryDate>\r\n";
            }

            //xml += "<PromotionCode>"+ orderModel.CouponCode+ "</PromotionCode>\r\n";
            xml += "<TaxCalculationType>1</TaxCalculationType>\r\n";//EvSalesDocumentInfo.TaxCalculation.Automatic

            if (orderModel.PurchaseOrderNumber != null || orderModel.PurchaseOrderNumber != "")
            {
                string PurchaseOrderNumber = orderModel.OrderNumber + ',' + orderModel.PurchaseOrderNumber;
                xml += "<CustomerPONo> " + PurchaseOrderNumber + "</CustomerPONo>\r\n";
            }
            else
            {
                xml += "<CustomerPONo> " + orderModel.OrderNumber + "</CustomerPONo>\r\n";
            }

            string locationSubLocation = orderModel?.ShippingAddress?.CountryCode == "CA" ? "CAN" : orderModel?.ShippingAddress?.CountryCode == "AU" ? "AUS" : "MAIN";
            xml += "<LocationSubLocation> " + locationSubLocation + " </LocationSubLocation>\r\n";

            //xml += "<LocationSubLocation>MAIN</LocationSubLocation>\r\n";
            // xml += "<Processed></Processed>\r\n";
            // xml += "<FreightPaddingCode></FreightPaddingCode>\r\n";
            // xml += "<AllowBackOrderCreation></AllowBackOrderCreation>\r\n";
            xml += "<DocumentAlias>SALES ORDER</DocumentAlias>\r\n";
            xml += "<ShipmentTrackingNote>"+orderModel.AdditionalInstructions+"</ShipmentTrackingNote>\r\n";
            xml += "<CustomFields>\r\n";
            xml += "<CustomCharacterField Number='29'>" + orderModel.OrderNumber + "</CustomCharacterField>\r\n";
            xml += "<CustomCharacterField Number='30'>" + orderModel.CouponCode + "</CustomCharacterField>\r\n";
            xml += "</CustomFields>\r\n";
            xml += "<LineItems>\r\n";
            int i = 1;
            foreach (OrderLineItemModel ol in orderModel.OrderLineItems)
            {
                xml += "<LineItem>\r\n";
                xml += "<Sequence>" + i.ToString() + "</Sequence>\r\n";
                xml += "<ItemCode>"+ol.Custom1+"</ItemCode>\r\n";
                xml += "<Quantity>" + ol.Quantity + "</Quantity>\r\n";
                xml += "<ItemPrice>" + Math.Round(ol.Price, 2) + "</ItemPrice>\r\n";
                xml += "<UOMCode>PR</UOMCode>\r\n";
                //xml += "<Cost>" + ol + "</Cost>\r\n";
                xml += "<VendorCode>SINO</VendorCode>\r\n";
                xml += "<DiscountValue>"+ Math.Round(Convert.ToDouble(ol.DiscountAmount), 2)+"</DiscountValue>\r\n";
                //xml += "<TaxCode>" + ol + "</TaxCode>\r\n";
                // xml += "<DepartmentCode>" + ol + "</DepartmentCode>\r\n";
                //xml += "<Critical>" + ol + "</Critical>\r\n";
                xml += "</LineItem>\r\n";
                i++;
            }
            xml += "<LineItem>\r\n";
            xml += "<Sequence>" + i.ToString() + "</Sequence>\r\n";
            xml += "<ItemCode>SH</ItemCode>\r\n";
            xml += "<Quantity>1</Quantity>\r\n";
            if (orderModel.PortalId == Convert.ToInt64(PortalIDDealer))
            {
                orderModel.ShippingCost = 0;
                xml += "<ItemPrice>" + Math.Round(orderModel.ShippingCost, 2) + "</ItemPrice>\r\n";
            }
            else
            {
                xml += "<ItemPrice>" + Math.Round(orderModel.ShippingCost, 2) + "</ItemPrice>\r\n";
            }
            xml += "<UOMCode>EA</UOMCode>\r\n";
            xml += "<VendorCode>SINO</VendorCode>\r\n";
            xml += "<DiscountValue>.000000</DiscountValue>\r\n";
            xml += "</LineItem>\r\n";
            xml += "</LineItems>\r\n";
            xml += "</SalesOrder>\r\n";

            return xml;
        }

        #endregion

        public string CreateAddressAndOrder(AddressModel addrmodel, OrderModel omodel)
        {
            // string everestOrderNumber = "";
            string addressxml = CreateAddressXMLForEverest(addrmodel);
            EverestAPI.eoSession session = new EverestAPI.eoSession();
            string sessionId = session.Open(serverName, companyCode, userName, password, DateTime.Now, "", loggedInJurisdiction, loggedInDepartment);
            EverestAPI.eoAddress everAddress = new EverestAPI.eoAddress();
            string result = everAddress.Create(sessionId, addressxml, EverestConsts.enumAddressTypes.atCustomerShipping, true);

            if (result.Contains("Error"))
            {
                return result;
            }
            else
            {
                string firstString = "<AddressCode>";
                string lastString = "</AddressCode>";
                int pos1 = result.IndexOf(firstString) + firstString.Length;
                int pos2 = result.Substring(pos1).IndexOf(lastString);
                omodel.Custom2 = result.Substring(pos1, pos2);
            }
            string Orderxml = CreateOrderXMLForEverest(omodel);
            EverestAPI.eoSalesOrder so = new EverestAPI.eoSalesOrder();
            string finalresult = so.Create(sessionId, Orderxml, true);
            //if (finalresult.Contains("Error"))
            //{
            //    return finalresult;
            //}
            //else
            //{
            //    string firstString = "<DocumentNo>";
            //    string lastString = "</DocumentNo>";
            //    int pos1 = finalresult.IndexOf(firstString) + firstString.Length;
            //    int pos2 = finalresult.Substring(pos1).IndexOf(lastString);
            //    everestOrderNumber = finalresult.Substring(pos1, pos2);

            //}


            if (!string.IsNullOrEmpty(sessionId))
            {
                session.Close(sessionId);
            }
            Marshal.ReleaseComObject(session);
            session = null;
            return finalresult;
        }

        public string Log(string logMessage)
        {
            string logfilepath = ConfigurationManager.AppSettings["FIlelogPath"];
            try
            {
                using (StreamWriter writer = File.AppendText(logfilepath + "\\" + "log.txt"))
                {
                    // Log(logMessage, w);

                    writer.Write("\r\nLog Entry : ");
                    writer.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                    writer.WriteLine("  :");
                    writer.WriteLine("  :{0}", logMessage);
                    writer.WriteLine("-------------------------------");
                }
            }
            catch (Exception ex)
            {
                return "Error : " + ex.Message + " StackTrace: " + ex.StackTrace;
            }
            return string.Empty;
        }

    }
}